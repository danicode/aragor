<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth',[
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'authError' => 'No posee los privilegios necesarios.',
            'authorize' => 'Controller',
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'storage' => 'Session'
        ]);

        $this->loadResources();

        // selecciona el layout, parte como base layoutMain página comercial, los demás layoutAdmin parte administrativa
        $this->viewBuilder()->layout('layoutAdmin');
    }

    private function loadResources()
    {
        $realod = true;

        $session = $this->request->session();

        if ($realod || !$session->check('paraments')) {
            $file = new File(WWW_ROOT . 'paraments.json', false, 0644);
            $json = $file->read(true, 'r');
            $parament =  json_decode($json);
            $session->write('paraments', $parament);
        }

        if ($realod || !$session->check('titles')) {
            $file = new File(WWW_ROOT . 'titles.json', false, 0644);
            $json = $file->read(true, 'r');
            $titles =  json_decode($json, true);
            $session->write('titles', $titles);
        }

        if ($realod || !$session->check('menu')) {
            $file = new File(WWW_ROOT . 'menu.json', false, 0644);
            $json = $file->read(true, 'r');
            $menu =  json_decode($json, true);
            $session->write('menu', $menu);
        }
    }

    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

        $this->set('current_controller', $this->request->controller);
        $this->set('current_action', $this->request->action);
        $this->set('username', $this->request->session()->read('Auth.User')['username']);
        $this->set('version', Configure::Read('project.version'));

        $this->set('userAdmin', 0);
        if ($this->request->session()->read('Auth.User')) {
            if (array_key_exists('admin', $this->request->session()->read('Auth.User'))) {

                $val = $this->request->session()->read('Auth.User')['admin'] ? 1 : 0;
                $this->set('userAdmin', $val);
            }
            $this->set('loggedIn', 1);

        } else {
            $this->set('loggedIn', 0);
        }

        //check login
        if ($this->request->session()->read('Auth.User')) {
            $this->set('loggedIn', true); 
        } else {
            $this->set('loggedIn',false);
            $this->set('loggedIn', 0);
        }
    }

    public function isAuthorized($user_id)
    {
        return true;
    }

    /**
     * Para registra un log de acciones en el sistema
    */
    public function activityRecord($action, $from) 
    {}

    public function beforeFilter(Event $event)
    {}
}
