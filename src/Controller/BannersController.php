<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\File;
use Cake\Event\Event;
use Cake\I18n\Time;

/**
 * Banners Controller
 *
 * @property \App\Model\Table\BannersTable $Banners
 */
class BannersController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('layoutAdmin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $banners = $this->Banners->find()->where(['lateral' => FALSE]);

        $this->set(compact('banners'));
        $this->set('_serialize', ['banners']);
    }

    public function lateral()
    {
        $banners = $this->Banners->find()->where(['lateral' => TRUE]);

        $this->set(compact('banners'));
        $this->set('_serialize', ['banners']);
    }

    /**
     * View method
     *
     * @param string|null $id Sections Asset id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $banner = $this->Banners->get($id, [
            'contain' => []
        ]);

        $this->set('banner', $banner);
        $this->set('_serialize', ['asset']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {

            foreach ($this->request->data['files'] as $file) {

                if ($file['tmp_name']) {

                    $banner = $this->Banners->newEntity();

                    $ext = explode('.', $file['name']);

                    $now = Time::now();
                    $newName = $now->toUnixString() . '-' . $ext[0] . '.' .  end($ext);
                    $from = $file['tmp_name'];
                    $to = WWW_ROOT . 'images/'. $newName ;

                    if (move_uploaded_file($from, $to)) {
                        $this->request->data['url'] = $newName;
                    }
                }
                $this->request->data['visible'] = false;
                $banner = $this->Banners->patchEntity($banner, $this->request->data);
                if ($this->Banners->save($banner)) {
                    $this->Flash->success(__('Se ha guardado correctamente.'));
                } else {
                    $this->Flash->error(__('No se ha guardado el Banner. Por favor, intente nuevamente.'));
                }
            }
        }
        $action = 'index';
        if ($this->request->data['lateral']) {
            $action = 'lateral';
        }
        return $this->redirect(['action' => $action]);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sections Asset id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $banner = $this->Banners->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $banner = $this->Banners->patchEntity($banner, $this->request->data);
            if ($this->Banners->save($banner)) {
                $this->Flash->success(__('The sections asset has been saved.'));

                $action = 'index';
                if ($this->request->data['lateral']) {
                    $action = 'lateral';
                }
                return $this->redirect(['action' => $action]);
            }
            $this->Flash->error(__('The sections asset could not be saved. Please, try again.'));
        }
        $banner = $this->Banners->find('list', ['limit' => 200]);
        $this->set(compact('banner'));
        $this->set('_serialize', ['banner']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sections Asset id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $banner = $this->Banners->get($_POST['id']);

        if (unlink(WWW_ROOT . 'images/' . $banner->url)) {

            if ($this->Banners->delete($banner)) {
                $this->Flash->success(__('Se ha eliminado correctamente el Banner.'));
            } else {
                $this->Flash->error(__('No se ha eliminado el Banner. Por favor, intente nuevamente.'));
            }
        }
        $action = 'index';
        if ($this->request->data['lateral']) {
            $action = 'lateral';
        }
        return $this->redirect(['action' => $action]);
    }

    /**
     * Visible method
     *
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function visible()
    {
        $banner = $this->Banners->get($_POST['id']);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!$this->request->data['lateral']) {
                $banners = $this->Banners->find()->where(['visible' => true, 'lateral' => FALSE]);
                foreach ($banners as $ba) {
                    $ba->visible = false;
                    $this->Banners->save($ba);
                }
            }
            $banner = $this->Banners->patchEntity($banner, $this->request->data);
            $banner->visible = !$banner->visible;

            if ($this->Banners->save($banner)) {
                $this->Flash->success(__('Se ha modificado la visibilidad correctamente.'));
            } else {
                $this->Flash->error(__('No se ha modificado la visibilidad. por favor, intente nuevamente.'));
            }
        }
        $action = 'index';
        if ($this->request->data['lateral']) {
            $action = 'lateral';
        }
        return $this->redirect(['action' => $action]);
    }

    public function changeLink()
    {
        if ($this->request->is('ajax')) {
            $link = $this->request->input('json_decode')->link;
            $id = $this->request->input('json_decode')->id;

            $banner = $this->Banners->get(['id' => $id]);
            $banner->link = $link;
            if ($this->Banners->save($banner)) {
                $data['message'] = "Se agrego correctamente el link: " . $link;
            } else {
                $data['message'] = "No se agrego el link. Por favor intente, nuevamente";
            }
            $this->set('data', $data);
        }
    }
}
