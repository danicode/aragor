<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Filesystem\File;
use FPDF;
use Cake\Routing\Router;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 *
 * @method \App\Model\Entity\Post[] paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('layoutAdmin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $posts = $this->Posts->find()->contain(['Images'])->where(['deleted' => false]);

        $this->set(compact('posts'));
        $this->set('_serialize', ['posts']);
    }

    /**
     * View method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => ['Favorites', 'Images']
        ]);

        $this->set('post', $post);
        $this->set('_serialize', ['post']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $post = $this->Posts->newEntity();
        $result = $this->Posts->find('all', ['fields' => 'id'])->last();
        $number_property = 1;
        if (!empty($result)) {
            $number_property = ++$result->id;
        }
        if ($this->request->is('post')) {

            $this->request->data['number_property'] = $number_property;
            $this->request->data['deleted'] = false;
            $this->request->data['enabled'] = false;
            if ($this->request->data['status'] == '8') {
                $this->request->data['publication_date'] = Time::now();
            }
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            if ($this->Posts->find()->where(['code' => $post->code])->count() == 0) {
                if ($this->Posts->save($post)) {
                    $this->Flash->success(__('La Publicación se ha guardado correctamente.'));
    
                    if ($this->request->data['btn-add'] == 'exit') {
                        return $this->redirect(['action' => 'index']);
                    }
    
                    return $this->redirect(['action' => 'add']);
                } else {
                    $this->Flash->error(__('La Publicación no se ha guardado. Por favor, intente nuevamente.'));
                }
            } else {
                $this->Flash->error(__('El Código ya existe. Por favor, intente nuevamente.'));
            }
        }
        $this->set(compact('post', 'number_property'));
        $this->set('_serialize', ['post']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => ['Images']
        ]);
        $number_property = $id;
        if ($this->request->is(['patch', 'post', 'put'])) {

            $post = $this->Posts->patchEntity($post, $this->request->getData());
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('Se han guardado correctamente los cambios.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se han guardado los cambios. Por favor, intente nuevamente.'));
        }
        $this->set(compact('post', 'number_property'));
        $this->set('_serialize', ['post']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $post = $this->Posts->get($id);
        $post->deleted = true;
        if ($this->Posts->save($post)) {
            $this->Flash->success(__('Se ha eliminado la Publicación correctamente.'));
        } else {
            $this->Flash->error(__('No se ha eliminado la Publicación. por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deleted()
    {
        $posts = $this->Posts->find()->contain(['Images'])->where(['deleted' => true]);

        $this->set(compact('posts'));
        $this->set('_serialize', ['posts']);
    }

    public function addImage()
    {
        if ($this->request->is('post')) {

            if (array_key_exists('files', $this->request->data)) {

                $this->loadModel('Images');
                $count_error = 0;
                $total = count($this->request->data['files']);

                foreach ($this->request->data['files'] as $file) {

                    if ($file['error'] == 0) {
                        if ($file['tmp_name']) {

                            $image = $this->Images->newEntity();

                            $ext = explode('.', $file['name']);
                            $now = Time::now();
                            $newName = $now->toUnixString() .'-' . $ext[0] . '.' .  end($ext);
                            $from = $file['tmp_name'];
                            $to = WWW_ROOT . 'images/'. $newName ;

                            if (move_uploaded_file($from, $to)) {
        
                                $this->request->data['url'] = $newName;
                            }
                        }

                        $image = $this->Images->patchEntity($image, $this->request->data);
                        if (!$this->Images->save($image)) {
                             $this->Flash->error(__('Las Imagenes no se han guardado. Por favor, intente nuevamente.'));
                        }
                    } else {
                        $count_error++;
                    }
                }
            } else {
                $this->Flash->warning('Error al intentar subir la imagen. Por favor, intente nuevamente.');
                return $this->redirect($this->referer());
            }

            if ($count_error == 0) {
                $this->Flash->success(__('Las Imagenes se han guardado correctamente.'));
            } else {
                $count_success = $total - $count_error;
                $this->Flash->warning(__('Total de imagenes: ' . $total . ' - ' . 'Imagenes no cargadas: ' . $count_error . ' - ' . 'Imagenes cargadas: ' . $count_success));
            }

            return $this->redirect(['controller' => 'Posts', 'action' => 'edit', $image->post_id]);
        }
    }

    public function deleteImage()
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('Images');

        $image = $this->Images->get($this->request->data['id']);

        if (unlink(WWW_ROOT . 'images/'. $image->url)) {

            if ($this->Images->delete($image)) {
                $this->Flash->success(__('La Imagen se ha eliminado correctamente.'));
            } else {
                $this->Flash->error(__('La Imagen no se ha eliminado. Por favor, intente nuevamente.'));
            }
        }

        return $this->redirect(['controller' => 'Posts', 'action' => 'edit', $image->post_id]);
    }

    public function mainImage()
    {
        $this->loadModel('Images');
        $image = $this->Images->get($_POST['id']);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $image = $this->Images->patchEntity($image, $this->request->data);

            $before = $this->Images->find()->where(['main' => true, 'post_id' => $image->post_id])->first();

            if (!empty($before)) {
                $before->main = false;
                $this->Images->save($before);
            }

            $image->main = !$image->main;

            if ($this->Images->save($image)) {
                $this->Flash->success(__('Se ha modificado la Imagen principal correctamente.'));
            } else {
                $this->Flash->error(__('No se ha modificado la Imagen principal. por favor, intente nuevamente.'));
            }
        }
        return $this->redirect(['action' => 'edit', $image->post_id]);
    }

    public function restore($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => []
        ]);

        $this->request->data['deleted'] = false;

        $post = $this->Posts->patchEntity($post, $this->request->data);

        if ($this->Posts->save($post)) {
            $this->Flash->success(__('Publicación Restaurado.'));
        } else {
            $this->Flash->error(__('Error al restaurar la Publicación.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function changeStatus()
    {
        if ($this->request->is('post')) {
            $post = $this->Posts->get($this->request->data['post_id']);
            if (in_array($this->request->data['status'], [6, 7, 8])) {
                $this->request->data['publication_date'] = Time::now();
                $this->request->data['confirmation_date'] = null;
            } else if (in_array($this->request->data['status'], [1, 2, 3])){
                $this->request->data['confirmation_date'] = Time::now();
            } else if (in_array($this->request->data['status'], [4, 5])){
                $this->request->data['confirmation_date'] = null;
            } 
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('Se ha cambiado de Estado correctamente.'));
            } else {
                $this->Flash->error(__('No se ha cambiado de Estado. Por favor, intente nuevamente.'));
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function postExportPDF($id = null)
    {
        if (!empty($id)) {

            $post = $this->Posts->get($id, [
                'contain' => ['Images']
            ]);

            $this->response->charset('UTF-8');
            $this->response->type('application/pdf');

            $FONT = 'Helvetica';

            $pdf = new FPDF('P', 'mm', 'A4');
            $pdf->AliasNbPages();
            $pdf->AddPage();

            $pdf->SetTitle(utf8_decode("Publicación"));

            $pdf->Image('img/logo.png', 70, 15, 80);

            $fullLink =  Router::url([ 
                'controller' => 'inicio',
                'action' => 'publicaciones', $post->id,
            ], TRUE);

            //QR
            $pdf->Image("https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=$fullLink&choe=UTF-8", 0, 60, 215, 0, 'PNG');

            //Ver publicacion
            $pdf->SetFont($FONT, 'B', 30);
            $pdf->SetFontSize(30);
            $pdf->Text(57, 59, utf8_decode("VER PUBLICACIÓN"));

            //Codigo
            $pdf->SetFont($FONT, 'B', 30);
            $pdf->SetFontSize(30);
            $pdf->Text(67, 75, utf8_decode("CÓDIGO: #" . $post->code));

            $filename = $post->code . '.pdf';
            $pdf->Output('I', $filename);
        }
    }

    public function postTarjetaExportPDF($id = null)
    {
        if (!empty($id)) {

            $post = $this->Posts->get($id, [
                'contain' => ['Images']
            ]);

            $this->response->charset('UTF-8');
            $this->response->type('application/pdf');

            $FONT = 'Helvetica';

            $pdf = new FPDF('P','mm',array(90,50));
            $pdf->AliasNbPages();
            $pdf->AddPage();

            $pdf->SetTitle(utf8_decode("Publicación"));

            $pdf->Image('img/logo.png', 4, 8, 42);

            //Ver publicacion
            $pdf->SetFont($FONT,'B',12);
            $pdf->SetFontSize(12);
            $pdf->Text(6, 30, utf8_decode("VER PUBLICACIÓN"));
            
            //Codigo
            $pdf->SetFont($FONT,'B',12);
            $pdf->SetFontSize(12);
            $pdf->Text(12, 35, utf8_decode("CÓDIGO: #" . $post->code));

            $fullLink =  Router::url([ 
                'controller' => 'inicio',
                'action' => 'publicaciones', $post->id,
            ], TRUE);

            //QR
            $pdf->Image("https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=$fullLink&choe=UTF-8",0,35,51,0,'PNG');

            $filename = $post->code . '.pdf';
            $pdf->Output('I', $filename);
        }
    }

    public function remove($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $this->loadModel('Images');
        $images = $this->Images->find()->where(['post_id' => $id]);

        if (!empty($images)) {
            foreach ($images as $image) {
                if (unlink(WWW_ROOT . 'images/'. $image->url)) {
                    $this->Images->delete($image);
                }
            }
        }

        $this->loadModel('Favorites');
        $favorites = $this->Favorites->find()->where(['post_id' => $id]);

        if (!empty($favorites)) {
            foreach ($favorites as $favorite) {
                $this->Favorites->delete($favorite);
            }
        }

        $post = $this->Posts->get($id);
        if ($this->Posts->delete($post)) {
            $this->Flash->success(__('Se ha eliminado la Publicación correctamente.'));
        } else {
            $this->Flash->error(__('No se ha eliminado la Publicación. por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function emailRecords()
    {
        $posts = $this->Posts->find()->where(['deleted' => false, 'enabled' => true]);

        $this->set(compact('posts'));
        $this->set('_serialize', ['posts']);
    }
}
