<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * EmailRecords Controller
 *
 *
 * @method \App\Model\Entity\EmailRecord[] paginate($object = null, array $settings = [])
 */
class EmailRecordsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('layoutAdmin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $emailRecords = $this->EmailRecords->find();

        $this->set(compact('emailRecords'));
        $this->set('_serialize', ['emailRecords']);
    }

    /**
     * View method
     *
     * @param string|null $id Email Record id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $emailRecord = $this->EmailRecords->get($id, [
            'contain' => []
        ]);

        $this->set('emailRecord', $emailRecord);
        $this->set('_serialize', ['emailRecord']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $emailRecord = $this->EmailRecords->newEntity();
        if ($this->request->is('post')) {
            $emailRecord = $this->EmailRecords->patchEntity($emailRecord, $this->request->getData());
            if ($this->EmailRecords->save($emailRecord)) {
                $this->Flash->success(__('The email record has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The email record could not be saved. Please, try again.'));
        }
        $this->set(compact('emailRecord'));
        $this->set('_serialize', ['emailRecord']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Email Record id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $emailRecord = $this->EmailRecords->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $emailRecord = $this->EmailRecords->patchEntity($emailRecord, $this->request->getData());
            if ($this->EmailRecords->save($emailRecord)) {
                $this->Flash->success(__('The email record has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The email record could not be saved. Please, try again.'));
        }
        $this->set(compact('emailRecord'));
        $this->set('_serialize', ['emailRecord']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Email Record id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $emailRecord = $this->EmailRecords->get($id);
        if ($this->EmailRecords->delete($emailRecord)) {
            $this->Flash->success(__('Se ha eliminado el registro correctamente.'));
        } else {
            $this->Flash->error(__('No se ha eliminado el registro. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function sendEmail()
    {
        if ($this->request->is('post')) {
            $data = json_decode($this->request->data['data']);

            $email = new Email();
            $email->transport('godaddy');
            $email->to($data->customer_email);
            $email->from('contacto@aragorikerprop.com');
            $email->subject(__('Aragor Iker: Publicaciones'));
            $email->emailFormat('html');
            $email->template('news_letter');
            $email->viewVars(['data' => $data]);
            $email->send();
            $this->Flash->success(__("Se ha enviado correctamente el correo."));
            $emailRecord = $this->EmailRecords->get($data->id);
            $emailRecord->status = "Enviado";
            $this->EmailRecords->save($emailRecord);
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Presale id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editComment($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $email_record = $this->EmailRecords->get(intval($this->request->data['email_record_id']));
            $email_record->comments = $this->request->data['comments'];
            $email_record = $this->EmailRecords->patchEntity($email_record, $this->request->getData());
            if ($this->EmailRecords->save($email_record)) {

                $this->Flash->success(__('Se ha guardado correctamente el comentario.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se ha guardado el comentario. Por favor, intente nuevamente.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
