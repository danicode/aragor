<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Accounts Controller
 *
 * @property \App\Model\Table\AccountsTable $Accounts
 *
 * @method \App\Model\Entity\Account[] paginate($object = null, array $settings = [])
 */
class AccountsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Captcha.Captcha');
        $this->loadModel('Posts');
        $this->Auth->config('loginAction', [
            'controller' => 'Accounts',
            'action' => 'add'
        ]);
        $this->viewBuilder()->layout('layoutMain');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['login', 'add', 'edit', 'deleteFavorite', 'logout', 'recuperarClave', 'pedirBaja']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $accounts = $this->paginate($this->Accounts);

        $this->set(compact('accounts'));
        $this->set('_serialize', ['accounts']);
    }

    /**
     * View method
     *
     * @param string|null $id Account id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $account = $this->Accounts->get($id, [
            'contain' => ['Favorites']
        ]);

        $this->set('account', $account);
        $this->set('_serialize', ['account']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $account = $this->Accounts->newEntity();

        if ($this->request->is('post')) {
            $ip = getenv('REMOTE_ADDR');
            $gRecaptchaResponse = $this->request->data['g-recaptcha-response'];

            $captcha = $this->Captcha->check($ip, $gRecaptchaResponse);

            if ($captcha->errorCodes == null) {
                // Success
                $this->request->data['deleted'] = false;
                $this->request->data['request_delete'] = false;

                $account_validate = $this->Accounts->find()->where(['email' => $this->request->data['email'], 'deleted' => FALSE])->first();

                if ($account_validate) {
                    $this->Flash->error(__('El correo ya se encuentra registrado, por favor intente nuevamente'));
                } else {
                    $account = $this->Accounts->patchEntity($account, $this->request->getData());
                    if ($this->Accounts->save($account)) {
                        $this->Auth->setUser($account);
                        $this->request->session()->write('type_user', 'accounts');
                        $this->Flash->success(__('Se ha registrado corretamente.'));

                        return $this->redirect(['controller' => 'accounts', 'action' => 'edit', $account->id]);
                    }

                    $this->Flash->error(__('No se ha registrado. Por favor, intente nuevamente.'));
                }
            } else {
                $this->Flash->warning(__("Por favor, haga clic en el cuadro Validación de Captcha."));
            }
        }
        $this->set(compact('account'));
        $this->set('_serialize', ['account']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Account id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->request->session()->read('Auth.User') 
            && $this->request->session()->read('type_user') == 'accounts') {
           
        } else {
            $this->Flash->warning(__('No posee los privilegios de acceso.'));
            return $this->redirect(['controller' => 'inicio', 'action' => 'index']);
        }
        $account = $this->Accounts->get($this->Auth->user()->id, [
            'contain' => ['Favorites.Posts.Images']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $account = $this->Accounts->patchEntity($account, $this->request->getData());
            if ($this->Accounts->save($account)) {
                $this->Flash->success(__('Se ha editado correctamente su Perfl.'));

                return $this->redirect(['controller' => 'accounts', 'action' => 'edit']);
            }
            $this->Flash->error(__('No se ha podido editar su Perfil. por favor, intente nuevamente.'));
        }
        $this->set(compact('account'));
        $this->set('_serialize', ['account']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Account id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function eliminarFavorito($id = null)
    {
        if ($this->request->session()->read('Auth.User') 
            && $this->request->session()->read('type_user') == 'accounts') {
           
        } else {
            $this->Flash->warning(__('No posee los privilegios de acceso.'));
            return $this->redirect(['controller' => 'inicio', 'action' => 'index']);
        }
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('Favorites');
        $favorite = $this->Favorites->get($id);
        if ($this->Favorites->delete($favorite)) {
            $this->Flash->success(__('Se ha eliminado corretamente la Publicación de su lista de Favoritos.'));
        } else {
            $this->Flash->error(__('No se ha eliminado la Publicación de su lista de Favoritos. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['controller' => 'accounts', 'action' => 'edit']);
    }

    public function login() 
    {
        if ($this->request->is('post')) {

            $email = $this->request->data['email'];
            $password = $this->request->data['password'];

            $account = $this->Accounts->find()->where(['email' => $email, 'deleted' => false])->first();

            $this->set(compact('account'));
            $this->set('_serialize', ['account']);

            if ($account) {

                if ((new DefaultPasswordHasher)->check($password, $account->password)) {

                    $this->Auth->setUser($account);

                    $this->request->session()->write('type_user', 'accounts');

                    $this->Flash->success("Hola" . ' ' . __($account->name));
                    $this->set(compact('account'));
                    $this->set('_serialize', ['account']);
                    return $this->redirect(['controller' => 'accounts', 'action' => 'edit']);
                } else {
                    $this->Flash->warning("El Correo o la Clave son incorrectas.");
                    return $this->redirect(['controller' => 'accounts', 'action' => 'add']);
                }
            } else {
                $this->Flash->warning("No se encuentra registrado el Correo: " . ' ' . __($email));
            }
        }

        return $this->redirect(['controller' => 'accounts', 'action' => 'add']);
    }

    public function logout() 
    {
        if ($this->Auth->user()) {
            parent::activityRecord('Logout accounts.', 'Logout');
            $this->Auth->logout();
            $this->request->session()->destroy();
            $this->Flash->success("La sesión se cerro correctamente");
        }

        return $this->redirect(['controller' => 'inicio', 'action' => 'index']);
    }

    public function recuperarClave() 
    {
        if ($this->request->is('post')) {

            $email_passed = $_POST['email'];
            $controller = $_POST['controller'];
            $account = $this->Accounts->find()->where(['email' => $email_passed, 'deleted' => false])->first();

            if ($account) {
                $password = $this->_generateRandomString();
                $account->password = $password;

                if ($this->Accounts->save($account)) {
                    $data = [
                        'to'       => $account->email,
                        'password' => $password
                    ];
                    $this->sendMailResetPassword($data);
                    $this->Flash->success("Se ha enviado su nueva Clave a su correo");
                } else {
                    $this->Flash->warning("No se ha podido enviar su nueva Clave. Por favor intente nuevamente.");
                }
            } else {
                $this->Flash->warning('No se encuentra registrado el Correo: ' . $_POST['email']);
            }
        }
        return $this->redirect(['action' => 'add']);
    }

    private function _generateRandomString($length = 4) 
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function sendMailResetPassword($data)
    {
        $email = new Email();
        $email->transport('godaddy');
        $email->to($data['to']);
        $email->from('contacto@aragorikerprop.com');
        $email->subject(__('Aragor Iker - Nueva Clave'));
        $email->emailFormat('html');
        $email->template('reset_password');
        $email->viewVars(['data' => $data]);
        $email->send();
    }

    public function agregarFavorito($id)
    {
        if ($this->request->session()->read('Auth.User') 
            && $this->request->session()->read('type_user') == 'accounts') {
           
        } else {
            $this->Flash->warning(__('No posee los privilegios de acceso.'));
            return $this->redirect(['controller' => 'inicio', 'action' => 'index']);
        }
        if ($this->request->is('post')) {

            if ($id && $this->Auth->user()) {

                $action = $this->request->data['action'];
                $param = "";
                if ($action == "publicaciones") {
                    $param = $id;
                }
                $this->loadModel('Favorites');
                $exist = $this->Favorites->find()->where(['account_id' => $this->Auth->user()->id, 'post_id' => $id])->first();
                if ($exist) {
                    $this->Flash->success("La Publicación ya se enuentra en su lista de Favoritos");
                } else {
                    $favorite = $this->Favorites->newEntity();
                    $favorite->post_id = $id;
                    $favorite->account_id = $this->Auth->user()->id;
                    $this->Favorites->save($favorite);
                    $this->Flash->success("Se ha añadido la Publicación a su lista de Favoritos");
                }

                return $this->redirect(['controller' => 'inicio', 'action' => $action, $param]);
            }
        }

        return $this->redirect(['controller' => 'inicio', 'action' => 'index']);
    }

    public function pedirBaja($id = null)
    {
        if ($this->request->session()->read('Auth.User') 
            && $this->request->session()->read('type_user') == 'accounts') {
           
        } else {
            $this->Flash->warning(__('No posee los privilegios de acceso.'));
            return $this->redirect(['controller' => 'inicio', 'action' => 'index']);
        }
        $this->request->allowMethod(['post', 'delete']);
        $account = $this->Accounts->get($id);
        $account->request_delete = true;
        if ($this->Accounts->save($account)) {
            $this->Flash->success(__('Se ha realizado su pedido de Baja de su Cuenta correctamente.'));
        } else {
            $this->Flash->error(__('No se ha pedido realizar el pedido de Baja de su Cuenta. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['controller' => 'accounts', 'action' => 'edit']);
    }
}
