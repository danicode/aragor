<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use FPDF;
use Cake\Routing\Router;

/**
 * Inicio Controller
 *
 * @property \App\Model\Table\InicioTable $Home
 */
class InicioController extends AppController
{

    public $helpers = ['SocialShare.SocialShare'];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Captcha.Captcha');
        $this->viewBuilder()->layout('layoutMain');
        $this->loadModel('Accounts');
        $this->loadModel('Posts');
        $this->loadModel('Banners');
        $this->loadModel('Assets');
        $assets = $this->Assets->find()->where(['visible' => TRUE])->order(['main' => 'DESC']);
        $banner = $this->Banners->find()->where(['visible' => TRUE, 'lateral' => FALSE])->first();
        $banners_lateral = $this->Banners->find()->where(['visible' => TRUE, 'lateral' => TRUE]);
        $mobile = $this->request->is('mobile');
        $this->set(compact('assets', 'banner', 'banners_lateral', 'mobile'));
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'servicios', 'nosotros', 'contactos', 'publicaciones', 'postExportPDF', 'buscarPublicaciones']);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);

        $account = null;

        if ($this->request->session()->read('Auth.User') 
            && $this->request->session()->read('type_user') == 'accounts') {

            $account = $this->Accounts->get($this->Auth->user()->id, [
                'contain' => ['Favorites.Posts']
            ]);
        }

        $this->set(compact('account'));
        $this->set('_serialize', ['account']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $posts = $this->Posts->find()->contain(['Images'])->where(['Posts.cover' => true, 'Posts.deleted' => false, 'Posts.enabled' => true]);
        $this->set(compact('posts'));
        $this->set('_serialize', ['posts']);
    }

    public function servicios() 
    {
        //$this->set(compact('account'));
        //$this->set('_serialize', ['account']);
    }

    public function nosotros() 
    {
        //$this->set(compact('account'));
        //$this->set('_serialize', ['account']);
    }

    public function contactos() 
    {
        if ($this->request->is(['post'])) {

            $ip = getenv('REMOTE_ADDR');
            $gRecaptchaResponse = $this->request->data['g-recaptcha-response'];

            $captcha = $this->Captcha->check($ip, $gRecaptchaResponse);

            if ($captcha->errorCodes == null) {
                // Success
                $email = new Email();
                $email->transport('godaddy');
                $email->to('contacto@aragorikerprop.com');
                $email->from('contacto@aragorikerprop.com');
                $email->subject(__('Consulta desde Fomrulario Contacto'));
                $email->emailFormat('html');
                $email->template('contact');
                $email->viewVars(['data' => $this->request->data]);
                $email->send();
                $this->Flash->success(__("Se ha enviado correctamente su consulta. En breve estaremos respondiendo sus consultas. Muchas gracias."));

                $this->loadModel('EmailRecords');
                $emailRecord = $this->EmailRecords->newEntity();
                $data = [
                    "status"         => "Nuevo",
                    "message"        => $this->request->data['message'],
                    "send_from"      => "Formulario Contacto",
                    "customer_name"  => $this->request->data['name'],
                    "customer_email" => $this->request->data['email'],
                    "customer_phone" => $this->request->data['phone'],
                ];
                $emailRecord = $this->EmailRecords->patchEntity($emailRecord, $data);
                $this->EmailRecords->save($emailRecord);

            } else {
                $this->Flash->warning(__("Por favor, haga clic en el cuadro Validación de Captcha."));
            }
        }
        //$this->set(compact('account'));
        //$this->set('_serialize', ['account']);
    }

    public function publicaciones($id) 
    {
        $post = $this->Posts->get($id, [
            'contain' => ['Images']
        ]);
        $related_post = null;
        if (empty($post)) {
            $this->Flash->warning(__("No se ha encontrado la Publicación. Por favor intente nuevamente."));
            return $this->redirect(['action' => 'index']);
        } else {

            $related_post = $this->Posts->find()->contain(['Images'])->where([
                'status'    => 8, 
                'operation' => $post->operation, 
                'id !='     => $post->id, 
                'enabled'   => true, 
                'deleted'   => false
            ])->order(['RAND()'])->limit(4);
        }

        $this->loadModel('Users');
        $users = $this->Users->find()->where(['contact' => true, 'deleted' => false, 'enabled' => true]);
        $emails = [];
        if ($users->count() > 0) {
            foreach ($users as $user) {
                $emails[$user->email] = $user->name;
            }
        }
        $this->set(compact('post', 'emails', 'related_post'));
        $this->set('_serialize', ['post']);
    }

    public function postExportPDF($id = null)
    {
        if (!empty($id)) {

            $post = $this->Posts->get($id, [
                'contain' => ['Images']
            ]);

            $paraments = $this->request->session()->read('paraments');

            $this->response->charset('UTF-8');
            $this->response->type('application/pdf');

            $FONT = 'Helvetica';

            $pdf = new IMGPDF('P', 'mm', 'A4');
            $pdf->AliasNbPages();
            $pdf->AddPage();

            $pdf->SetTitle(utf8_decode("Publicación"));

            $pdf->SetFont($FONT, '', 14);

            $pdf->SetFontSize(8);
            $pdf->SetTextColor(105,105,105);
            $pdf->Text(55, 14, utf8_decode("Obligado 1401 - Rcia - Chaco"));

            $pdf->SetFontSize(8);
            $pdf->SetTextColor(105, 105, 105);
            $pdf->Text(55, 18, utf8_decode("Tel: +54 0362 154221616 | 4573000"));

            $pdf->SetFontSize(8);
            $pdf->SetTextColor(105, 105, 105);
            $pdf->Text(55, 22, utf8_decode("Correo: contacto@aragorikerprop.com"));

            $pdf->SetFontSize(8);
            $pdf->SetTextColor(105, 105, 105);
            $pdf->Text(55, 26, utf8_decode("http://www.aragorikerprop.com"));

            $pdf->SetTextColor(0,0,0);

            //TITULO
            $pdf->SetFont($FONT, 'B', 14);
            $pdf->SetFontSize(12);
            $pdf->Sety((66) / 2);
            $pdf->Setx((15) / 2);
            $pdf->MultiCell(180, 6, utf8_decode($post->title));

            //IMAGEN
            $main_image = "noimage.png";
            if (!empty($post->images)) {
                foreach ($post->images as $image) {
                    if ($image->main) {
                        $main_image = $image->url;
                    }
                }
                if ($main_image == "noimage.png") {
                    $main_image = $post->images[0]->url;
                }
            }

            $pdf->imageCenterCell('images/' . $main_image, $pdf->GetX(), $pdf->GetY() + 2, 185, 80);

            //REFERENCIA
            $pdf->SetFont($FONT, 'B', 12);
            $pdf->SetFontSize(12);
            $pdf->Text(10, 133, utf8_decode("REFERENCIA:"));

            $pdf->SetFont($FONT,'', 12);
            $pdf->SetFontSize(12);
            $pdf->Text(42, 133, '#' . $post->code);

            //PRECIO
            $pdf->SetFont($FONT, 'B', 12);
            $pdf->SetFontSize(12);
            $pdf->Text(150, 133, utf8_decode("PRECIO:"));

            $pdf->SetFont($FONT,'',12);
            $pdf->SetFontSize(12);
            $money = $post->money;
            $price = "Consultar";
            if (!empty($post->price)) {
                $price = $paraments->post->money->$money . ' ' . $post->price;
            }
            $pdf->Text(170, 133, $price);

            //DESCRIPCION
            $pdf->SetFont($FONT, 'B', 12);
            $pdf->SetFontSize(12);
            $pdf->Text(10, 143, utf8_decode("DESCRIPCIÓN:"));
            
            $string_width = $pdf->GetStringWidth($post->description);
            
            $count_rows = ceil($string_width / 185);

            $h = 5 ;
            
            $de_mas = $count_rows - $h;

            $h += ($de_mas / 5 > 0 ) ? ceil($de_mas / 5) : 0; 

            $y = 148;

            $pdf->SetFont($FONT, '', 12);
            $pdf->SetFontSize(12);
            $pdf->Sety($y);
            $pdf->Setx(9);
            $pdf->MultiCell(180, $h, utf8_decode($post->description), 0, 'L');

            $y = $pdf->GetY() + 8;

            //CARACTERISTICAS
            $pdf->SetFont($FONT,'B',12);
            $pdf->SetFontSize(12);
            $pdf->Text(10, $y, utf8_decode("CARACTERISTICAS:"));

            $y += 7;

            //BAÑO
            $bath = $post->bath;
            $bath = $paraments->post->bath->$bath;
            $pdf->SetFont($FONT, '', 12);
            $pdf->SetFontSize(12);
            $pdf->Text(10, $y, utf8_decode($bath));

            //DORMITORIOS
            $room = $post->room;
            if (!empty($room)) {
                $room = $paraments->post->room->$room;
            } else {
                $room = 'Sin dormitorios';
            }
            $pdf->SetFont($FONT, '', 12);
            $pdf->SetFontSize(12);
            $pdf->Text(50, $y, utf8_decode($room));

            //AMBIENTES
            $environment = $post->environment;
            if (!empty($environment)) {
                $environment = $paraments->post->environment->$environment;
            } else {
                $environment = 'Sin ambientes';
            }
            $pdf->SetFont($FONT, '', 12);
            $pdf->SetFontSize(12);
            $pdf->Text(110, $y, utf8_decode($environment));

            //COCHERRA
            $garage = $post->garage;
            $garage = $paraments->post->garage->$garage;
            $pdf->SetFont($FONT, '', 12);
            $pdf->SetFontSize(12);
            if ($garage == 5) {
                $garage = "Cochera opcional";
            }
            $pdf->Text(170, $y, utf8_decode($garage));

            $y += 10;

            //UBICACION
            $pdf->SetFont($FONT, 'B', 12);
            $pdf->SetFontSize(12);
            $pdf->Text(10, $y, utf8_decode("UBICACIÓN:"));

            $pdf->SetFont($FONT, '', 12);
            $pdf->SetFontSize(12);
            $pdf->Text(38, $y, utf8_decode($post->address));

            $pdf->Line(10, 30, 210-20, 30);

            $pdf->Image('img/logo.png', 10, 10, 42);

            $fullLink =  Router::url([ 
                'controller' => 'inicio',
                'action' => 'publicaciones', $post->id,
            ], TRUE);

            //QR
            $pdf->Image("https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=$fullLink&choe=UTF-8", 172, 9, 20, 0, 'PNG');

            if ($post->lat != null && $post->lng != null) {
                $zoom = $post->zoom_map_pdf;
                if ($zoom == null) {
                    $zoom = 12;
                }
                $lat = $post->lat;
                $lng = $post->lng;

                $map_url = "https://maps.googleapis.com/maps/api/staticmap?center=$lat,$lng&zoom=$zoom&size=700x260&markers=color:red%7Clabel:P%7C$lat,$lng&key=AIzaSyAZG7dni5-GuQaooMmy6fLtYf3B_kHtAOY";
                $file = file_get_contents($map_url);
                if (file_put_contents(WWW_ROOT . '/images/map.png', $file)) {

                    $y += 5; 

                    if ($y > 208) {

                        $pdf->AddPage();

                        $pdf->SetTitle(utf8_decode("Publicación"));

                        $pdf->SetFont($FONT, '', 14);

                        $pdf->SetFontSize(8);
                        $pdf->SetTextColor(105, 105, 105);
                        $pdf->Text(55, 14, utf8_decode("Obligado 1401 - Rcia - Chaco"));

                        $pdf->SetFontSize(8);
                        $pdf->SetTextColor(105, 105, 105);
                        $pdf->Text(55, 18, utf8_decode("Tel: +54 0362 154221616 | 4573000"));

                        $pdf->SetFontSize(8);
                        $pdf->SetTextColor(105, 105, 105);
                        $pdf->Text(55, 22, utf8_decode("Correo: contacto@aragorikerprop.com"));

                        $pdf->SetFontSize(8);
                        $pdf->SetTextColor(105, 105, 105);
                        $pdf->Text(55, 26, utf8_decode("http://www.aragorikerprop.com"));

                        $pdf->SetTextColor(0, 0, 0);

                        //TITULO
                        $pdf->SetFont($FONT, 'B', 14);
                        $pdf->SetFontSize(12);
                        $pdf->Sety((66) / 2);
                        $pdf->Setx((15) / 2);
                        $pdf->MultiCell(180, 6, utf8_decode($post->title));

                         $pdf->Line(10, 30, 210-20, 30);
                         $pdf->Image('img/logo.png', 10, 10, 42);

                         $pdf->Image("https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=$fullLink&choe=UTF-8", 172, 9, 20, 0, 'PNG');
      
                         $y = 50;
                    }

                    $pdf->imageCenterCell(WWW_ROOT . '/images/map.png', 10, $y, 185, 75);
                    unlink(WWW_ROOT . '/images/map.png');
                }
            }

            $filename = Time::now()->format('d-m-Y') . '_' . "publicacion" . '.pdf';
            $pdf->Output('I', $filename);
        }
    }

    public function buscarPublicacionesTab($tab)
    {
        $this->loadModel('Assets');
        $assets = $this->Assets->find()->where(['visible' => true]);

        $where['operation'] = $tab;

        $where['Posts.deleted'] = false;
        $where['Posts.enabled'] = true;

        $posts = $this->Posts->find()->contain(['Images'])->where($where);

        $this->set(compact('posts', 'tab', 'assets'));
        $this->set('_serialize', ['posts']);
    }

    public function buscarPublicaciones()
    {
        $this->loadModel('Assets');
        $assets = $this->Assets->find()->where(['visible' => true]);

        if ($this->request->is('post')) {

            $tab = "";
            if (array_key_exists('tab', $this->request->data)) {
                $tab = $this->request->data['tab'];
                $where['operation'] = $tab;
            } else {

                if ($this->request->data['operation'] == '' 
                    && $this->request->data['type_property'] == ''
                    && $this->request->data['zone'] == ''
                    && $this->request->data['room'] == ''
                    && $this->request->data['environment'] == ''
                    && $this->request->data['bath'] == ''
                    && $this->request->data['garage'] == ''
                    && $this->request->data['money'] == ''
                    && $this->request->data['min_price'] == ''
                    && $this->request->data['max_price'] == ''
                    && $this->request->data['code'] == '') {

                    $this->Flash->warning(__("Debe ingresar al menos un parametro para realizar la búsqueda."));
                    return $this->redirect(['action' => 'index']);
                }

                $where = [];

                if ($this->request->data['code'] != '') {
                    $where['code LIKE'] = '%' . $this->request->data['code'] . '%';
                } else {

                    if ($this->request->data['operation'] != '') {
                        $where['operation'] = $this->request->data['operation'];
                    }

                    if ($this->request->data['type_property'] != '') {
                        $where['type_property'] = $this->request->data['type_property'];
                    }

                    if ($this->request->data['zone'] != '') {
                        $where['zone'] = $this->request->data['zone'];
                    }

                    if ($this->request->data['room'] != '') {
                        $where['room'] = $this->request->data['room'];
                    }

                    if ($this->request->data['environment'] != '') {
                        $where['environment'] = $this->request->data['environment'];
                    }

                    if ($this->request->data['bath'] != '') {
                        $where['bath'] = $this->request->data['bath'];
                    }

                    if ($this->request->data['garage'] != '') {
                        $where['bath'] = $this->request->data['garage'];
                    }

                    if ($this->request->data['money'] != '') {
                        $where['money'] = $this->request->data['money'];
                    }

                    if ($this->request->data['min_price'] != '') {
                        $where['price >='] = $this->request->data['min_price'];
                    }

                    if ($this->request->data['max_price'] != '') {
                        $where['price <='] = $this->request->data['max_price'];
                    }
                }
            }

            $where['Posts.deleted'] = false;
            $where['Posts.enabled'] = true;

            $posts = $this->Posts->find()->contain(['Images'])->where($where);
        }
        
        $this->set(compact('posts', 'tab', 'assets'));
        $this->set('_serialize', ['posts']);
    }

    public function consulta()
    {
        if ($this->request->is(['post'])) {
            $post = $this->Posts->get($this->request->data['post_id']);
            $this->request->data['code'] = "-----";
            if (!empty($post)) {
                $this->request->data['code'] = $post->code;
            }
            $to = $this->request->data['email_contact'];
            $email = new Email();
            $email->transport('godaddy');
            $email->to($to);
            $email->from('contacto@aragorikerprop.com');
            $email->subject(__('Consulta desde Fomrulario Publicación'));
            $email->emailFormat('html');
            $email->template('post');
            $email->viewVars(['data' => $this->request->data]);
            $email->send();
            $this->Flash->success(__("Se ha enviado correctamente su consulta. En breve estaremos respondiendo sus consultas. Muchas gracias."));

            $this->loadModel('EmailRecords');
            $emailRecord = $this->EmailRecords->newEntity();
            $emailRecord->post_code = $this->request->data['code'];
            $emailRecord->status = "Nuevo";
            $emailRecord->message = $this->request->data['message'];
            $emailRecord->from = "Formulario publicación";
            $emailRecord->customer_name = $this->request->data['name'];
            $emailRecord->customer_email = $this->request->data['email'];
            $emailRecord->customer_phone = $this->request->data['phone'];
            $emailRecord->email_contact = $this->request->data['email_contact'];
            $this->EmailRecords->save($emailRecord);

        }
        return $this->redirect(['action' => 'publicaciones', $this->request->data['post_id']]);
    }
}

class IMGPDF extends FPDF 
{
	function imageCenterCell($file, $x, $y, $w, $h)
	{
		if (!file_exists($file)) 
		{
			$this->Error('File does not exist: ' . $file);
		}
		else
		{
			list($width, $height) = getimagesize($file);
			$ratio=$width/$height;
			$zoneRatio=$w/$h;
			// Same Ratio, put the image in the cell
			if ($ratio==$zoneRatio)
			{
				$this->Image($file, $x, $y, $w, $h);
			}
			// Image is vertical and cell is horizontal
			if ($ratio<$zoneRatio)
			{
				$neww = $h * $ratio; 
				$newx = $x + (($w - $neww) / 2);
				$this->Image($file, $newx, $y, $neww);
			}
			// Image is horizontal and cell is vertical
			if ($ratio>$zoneRatio)
			{
				$newh = $w / $ratio; 
				$newy = $y + (($h - $newh) / 2);
				$this->Image($file, $x, $newy, $w);
			}
		}
	}
}
