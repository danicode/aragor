<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('layoutAdmin');
    }

    public function isAuthorized($user = null) {

        if ($this->request->action == 'login') {
            return true;
        }

        if ($this->request->action == 'logout') {
            return true;
        }

        if ($this->request->action == 'getUsers') {
            return true;
        }

        return parent::isAuthorized($user['id']);
    }

    public function index()
    {
        $users = $this->Users->find()->where(['id !=' => 0, 'deleted' => false]);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    public function view($id = null) 
    {
        $user = $this->Users->get($id);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    public function add() 
    {
        $user = $this->Users->newEntity();

        if ($this->request->is('post')) {

            $this->request->data['deleted'] = false;
            
            $user = $this->Users->patchEntity($user, $this->request->data);

            $username = $this->Users->find()->where(['username' => $user->username]);

            if ($username->count() == 0) {

                $email = $this->Users->find()->where(['email' => $user->email]);

                if ($email->count() == 0) {

                    if ($this->Users->save($user)) {

                        $this->Flash->success(__('El Usuario se ha registrado con éxito'));

                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('El Usuario se ha registrarse. Por favor, intente nuevamente.'));
                    }
                } else {
                    $this->Flash->error(__('El Correo ingresado ya existe. Por favor, intente nuevamente.'));
                }
            } else {
                $this->Flash->error(__('El Usuario ingresado ya existe. Por favor, intente nuevamente.'));
            }

        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function edit($id = null)
    {
        $user = $this->Users->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $user = $this->Users->patchEntity($user, $this->request->data);

            $username = $this->Users->find()->where(['username' => $user->username, 'id !=' => $user->id]);

            if ($username->count() == 0) {

                $email = $this->Users->find()->where(['email' => $user->email, 'id !=' => $user->id]);

                if ($email->count() == 0) {

                    if ($this->Users->save($user)) {

                        parent::activityRecord('Se editó: '.$user->username, 'Usuarios/Editar');

                        $this->Flash->success(__('Cambios Guardados.'));
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('Los Cambios no se guardaron. Por favor intente de nuevo.'));
                    }
                } else {
                    $this->Flash->error(__('El Correo ingresado ya existe. Por favor, intente nuevamente.'));
                }
            } else {
                $this->Flash->error(__('El Usuario ingresado ya existe. Por favor, intente nuevamente.'));
            }
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($_POST['id']);
        $username = $user->username;
        $user->deleted = true;
        if ($this->Users->save($user)) {

            parent::activityRecord('Se elimino Usuario. user: ' . $username, 'Usuarios/Delete');

            $this->Flash->success(__('El usuario ha sido borrado.'));
        } else {
            $this->Flash->error(__('El Usuario no se elimino. Por favor intente de nuevo.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function deleted()
    {
        $users = $this->Users->find()->where(['deleted' => true]);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    public function login() 
    {
        if ($this->request->session()->check('Auth.User')) {
            return $this->redirect(['controller' => 'Users','action' => 'dashboard']);
        }

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {

                if ($user['enabled'] && $user['deleted'] == false) {
                    $this->Auth->setUser($user);
                    parent::activityRecord('Login usuario.', 'Login');
                    $this->request->session()->write('type_user', 'admin');
                    $this->Flash->default(__('Hola! ' . $this->Auth->user()['username']));
                    return $this->redirect(['controller' => 'Users','action' => 'dashboard']);
                } else {
                    if ($user['enabled']) {
                        $this->Flash->warning(__('El Usuario se encuentra inhabilitado.'));
                    } else {
                        $this->Flash->warning(__('El Usuario se encuentra eliminado.'));
                    }
                }

            } else {
                $this->Flash->error(__('El Usuario o la clave son incorrectas.'));
            }
        }
    }

    public function logout() 
    {
        parent::activityRecord('Logout usuario.', 'Logout');
        $this->Auth->logout();
        $this->request->session()->destroy();
        return $this->redirect(['controller' => 'Users','action' => 'login']);
    }

    public function dashboard() 
    {
        $id = $this->Auth->user()['id'];
        $user = $this->Users->get($id);
        $session = $this->request->session();

        $this->loadModel('Posts');

        $current_date = Time::now();

        $post_publication = $this->Posts->find('all', array(
            'fields' => array('id', 'code', 'title', 'type_property', 'zone', 'number_property', 'operation', 'room', 'environment', 'bath', 'garage', 'price', 'money', 'status', 'address', 'map', 'enabled', 'deleted', 'created', 'modified', 'publication_date', 'confirmation_date', 'cover', 'description'),
            'conditions' => array(
                'MONTH(publication_date)' => $current_date->month,
                'YEAR(publication_date)' => $current_date->year

            ),
        ))->count();

        $post_confirmation = $this->Posts->find('all', array(
            'fields' => array('id', 'code', 'title', 'type_property', 'zone', 'number_property', 'operation', 'room', 'environment', 'bath', 'garage', 'price', 'money', 'status', 'address', 'map', 'enabled', 'deleted', 'created', 'modified', 'publication_date', 'confirmation_date', 'cover', 'description'),
            'conditions' => array(
                'MONTH(confirmation_date)' => $current_date->month,
                'YEAR(confirmation_date)' => $current_date->year

            ),
        ))->count();

        //alquilado
        $post_rent = $this->Posts->find()->where(['status' => 8, 'operation' => 1])->count();

        //vendido
        $post_sale = $this->Posts->find()->where(['status' => 8, 'operation' => 2])->count();

        $this->set(compact('user', 'post_publication', 'post_confirmation', 'post_rent', 'post_sale'));
        $this->set('_serialize', ['user']);
    }

    public function restore($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->request->data['deleted'] = false;

        $user = $this->Users->patchEntity($user, $this->request->data);

        if ($this->Users->save($user)) {
            $this->Flash->success(__('Usuario Restaurado.'));
        } else {
            $this->Flash->error(__('Error al restaurar el Usuario.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function customers()
    {
        $this->loadModel('Accounts');
        $accounts = $this->Accounts->find()->where(['deleted' => false]);
        $this->set(compact('accounts'));
        $this->set('_serialize', ['accounts']);
    }
    
    public function deleteCustomer()
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('Accounts');
        $account = $this->Accounts->get($_POST['id']);
        $account_name = $account->name;
        $account->deleted = true;
        if ($this->Accounts->save($account)) {

            parent::activityRecord('Se elimino Cliente. Nombre: ' . $account_name, 'Usuarios/DeleteCustomer');

            $this->Flash->success(__('El Cliente ha sido borrado.'));
        } else {
            $this->Flash->error(__('El Cliente no se elimino. Por favor intente de nuevo.'));
        }
        return $this->redirect(['action' => 'customers']);
    }
    
    public function deletedcustomer()
    {
        $this->loadModel('Accounts');
        $accounts = $this->Accounts->find()->where(['deleted' => true]);
        $this->set(compact('accounts'));
        $this->set('_serialize', ['accounts']);
    }
    
    public function restoreCustomer($id)
    {
        $this->loadModel('Accounts');
        $account = $this->Accounts->get($id, [
            'contain' => []
        ]);

        $this->request->data['deleted'] = false;
        $this->request->data['request_delete'] = false;

        $account = $this->Accounts->patchEntity($account, $this->request->data);

        if ($this->Accounts->save($account)) {
            $this->Flash->success(__('Cliente Restaurado.'));
        } else {
            $this->Flash->error(__('Error al restaurar el Cliente.'));
        }

        return $this->redirect(['action' => 'customers']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Presale id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function accountEditComment($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->loadModel('Accounts');
            $account = $this->Accounts->get(intval($this->request->data['account_id']));
            $account = $this->Accounts->patchEntity($account, $this->request->getData());
            if ($this->Accounts->save($account)) {
                $this->Flash->success(__('Se ha guardado correctamente el comentario.'));

                return $this->redirect(['action' => 'customers']);
            }
            $this->Flash->error(__('No se ha guardado el comentario. Por favor, intente nuevamente.'));
        }
        return $this->redirect(['action' => 'customers']);
    }
}
