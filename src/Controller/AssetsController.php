<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\File;
use Cake\Event\Event;
use Cake\I18n\Time;

/**
 * Assets Controller
 *
 * @property \App\Model\Table\AssetsTable $Assets
 */
class AssetsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('layoutAdmin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $assets = $this->Assets->find();

        $this->set(compact('assets'));
        $this->set('_serialize', ['assets']);
    }

    /**
     * View method
     *
     * @param string|null $id Sections Asset id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $asset = $this->Assets->get($id, [
            'contain' => ['Sections']
        ]);

        $this->set('asset', $asset);
        $this->set('_serialize', ['asset']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {

            foreach ($this->request->data['files'] as $file) {

                if ($file['tmp_name']) {

                    $asset = $this->Assets->newEntity();

                    $ext = explode('.', $file['name']);

                    $now = Time::now();
                    $newName = $now->toUnixString() . '-' . $ext[0] . '.' .  end($ext);
                    $from = $file['tmp_name'];
                    $to = WWW_ROOT . 'images/'. $newName ;

                    if (move_uploaded_file($from, $to)) {
                        $this->request->data['url'] = $newName;
                    }
                }
                $this->request->data['visible'] = false;
                $asset = $this->Assets->patchEntity($asset, $this->request->data);
                if ($this->Assets->save($asset)) {
                    $this->Flash->success(__('Se ha guardado correctamente.'));
                } else {
                    $this->Flash->error(__('No se ha guardado la imagen. Por favor, intente nuevamente.'));
                }
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sections Asset id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $asset = $this->Assets->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $asset = $this->Assets->patchEntity($asset, $this->request->data);
            if ($this->Assets->save($asset)) {
                $this->Flash->success(__('The sections asset has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sections asset could not be saved. Please, try again.'));
        }
        $asset = $this->Assets->find('list', ['limit' => 200]);
        $this->set(compact('asset'));
        $this->set('_serialize', ['asset']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sections Asset id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $asset = $this->Assets->get($_POST['id']);

        if (unlink(WWW_ROOT . 'images/' . $asset->url)) {

            if ($this->Assets->delete($asset)) {
                $this->Flash->success(__('Se ha eliminado correctamente la imagen.'));
            } else {
                $this->Flash->error(__('No se ha eliminado la imagen. Por favor, intente nuevamente.'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Visible method
     *
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function visible()
    {
        $asset = $this->Assets->get($_POST['id']);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $asset = $this->Assets->patchEntity($asset, $this->request->data);
            $asset->visible = !$asset->visible;

            if ($this->Assets->save($asset)) {
                $this->Flash->success(__('Se ha modificado la visibilidad correctamente.'));
            } else {
                $this->Flash->error(__('No se ha modificado la visibilidad. por favor, intente nuevamente.'));
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function changeLink()
    {
        if ($this->request->is('ajax')) {
            $link = $this->request->input('json_decode')->link;
            $id = $this->request->input('json_decode')->id;

            $asset = $this->Assets->get(['id' => $id]);
            $asset->link = $link;
            if ($this->Assets->save($asset)) {
                $data['message'] = "Se agrego correctamente el link: " . $link;
            } else {
                $data['message'] = "No se agrego el link. Por favor intente, nuevamente";
            }
            $this->set('data', $data);
        }
    }

    public function main()
    {
        $asset = $this->Assets->get($_POST['id']);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $assets = $this->Assets->find()->where(['visible' => true]);
            foreach ($assets as $asst) {
                $asst->main = false;
                $this->Assets->save($asst);
            }

            $asset = $this->Assets->patchEntity($asset, $this->request->data);
            $asset->main = !$asset->main;

            if ($this->Assets->save($asset)) {
                $this->Flash->success(__('Se ha marcado como principal la imagen correctamente.'));
            } else {
                $this->Flash->error(__('No se ha podido marcar como principal la imagen seleccionada. por favor, intente nuevamente.'));
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
