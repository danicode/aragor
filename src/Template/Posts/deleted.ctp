<style type="text/css">

    #modal-filter .form-group {
        margin-bottom: 4px;
    }

</style>

<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-filter" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Filtrar por Columnas',
                'class' => 'btn btn-default btn-filter-column',
                'data-toggle' => 'modal', 'data-target' => '#modal-filter',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la tabla',
                'class' => 'btn btn-default btn-export',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered" id="table-posts">
            <thead>
                <tr>
                    <th>Publicado</th>
                    <th>Confirmado</th>
                    <th>Estado</th>
                    <th>Referencia</th>
                    <th>Nro Propiedad</th>
                    <th>Titulo</th>
                    <th>Descripción</th>
                    <th>Propiedad</th>
                    <th>Zona</th>
                    <th>Domicilio</th>
                    <th>Operación</th>
                    <th>Ambiente</th>
                    <th>Baño</th>
                    <th>Cochera</th>
                    <th>Precio</th>
                    <th>Moneda</th>
                    <th>Habilitado</th>
                    <th>Imagen</th>
                    <th>Mapa</th>
                    <th>Creado</th>
                    <th>Modificado</th>
                    <th>Portada</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modal-filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Filtrar por columnas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

          <?= $this->Form->create() ?>

          <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label" for="publication_date">Publicado</label>
                    <div class='input-group date' id='publication_date-datetimepicker'>
                        <span class="input-group-addon mr-0">Fecha</span>
                        <input name="input-publication_date" id="input-publication_date" type='text' class="column_search form-control datetimepicker" data-column="0" />
                        <span class="input-group-addon mr-0">
                            <span class="glyphicon icon-calendar"></span>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="confirmation_date">Confirmación</label>
                    <div class='input-group date' id='confirmation_date-datetimepicker'>
                        <span class="input-group-addon mr-0">Fecha</span>
                        <input name="input-confirmation_date" id="input-confirmation_date" type='text' class="column_search form-control datetimepicker" data-column="1" />
                        <span class="input-group-addon mr-0">
                            <span class="glyphicon icon-calendar"></span>
                        </span>
                    </div>
                </div>
                
                <div class="form-group select">
                    <label class="control-label" for="status">Estado</label>
                    <select name="status" class="column_search form-control" data-column="2" id="status">
                        <?php foreach ($_SESSION['paraments']->post->status as $key => $st): ?>
                            <option value="<?= $key ?>"><?= $st->name ?></option>
                        <?php endforeach; ?>
                    </select></div>
                <?= $this->Form->input('code', ['label' => __('Referencia'), 'class'=> 'column_search', 'data-column' => 3]) ?>
              </div>
              <div class="col-md-4">
                <?= $this->Form->input('number_property', ['label' => __('Nro Propiedad'), 'class'=> 'column_search', 'data-column' => 4]) ?>
                <?= $this->Form->input('title', ['label' => __('Titulo'), 'class'=> 'column_search', 'data-column' => 5]) ?>
                <?= $this->Form->input('type_property', ['label' => __('Propiedad'), 'class'=> 'column_search', 'data-column' => 7, 'options' => $_SESSION['paraments']->post->property]) ?>
                <?= $this->Form->input('zone', ['label' => __('Zona'), 'class'=> 'column_search', 'data-column' => 8, 'options' => $_SESSION['paraments']->post->zone]) ?>
              </div>
              <div class="col-md-4">
                <?= $this->Form->input('address', ['label' => __('Domicilio'), 'class'=> 'column_search', 'data-column' => 9]) ?>  
                <?= $this->Form->input('operation', ['label' => __('Operación'), 'class'=> 'column_search', 'data-column' => 10, 'options' => $_SESSION['paraments']->post->operation]) ?>
                <?= $this->Form->input('room', ['label' => __('Ambiente'), 'class'=> 'column_search', 'data-column' => 11, 'options' => $_SESSION['paraments']->post->room]) ?>
                <?= $this->Form->input('bath', ['label' => __('Baño'), 'class'=> 'column_search', 'data-column' => 12, 'options' => $_SESSION['paraments']->post->bath]) ?>
              </div>
              <div class="col-md-4">
                <?= $this->Form->input('garage', ['label' => __('Cochera'), 'class'=> 'column_search', 'data-column' => 13, 'options' => $_SESSION['paraments']->post->garage]) ?>
                <?= $this->Form->input('price', ['label' => __('Precio'), 'class'=> 'column_search', 'data-column' => 14]) ?>
                <?= $this->Form->input('money', ['label' => __('Moneda'), 'class'=> 'column_search', 'data-column' => 15, 'options' => $_SESSION['paraments']->post->money]) ?>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                        <label class="control-label" for="created">Creado</label>
                        <div class='input-group date' id='created-datetimepicker'>
                            <span class="input-group-addon mr-0">Fecha</span>
                            <input name="input-created" id="input-created" type='text' class="column_search form-control datetimepicker" data-column="18" />
                            <span class="input-group-addon mr-0">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                    </div>
                <div class="form-group">
                    <label class="control-label" for="modified">Modificado</label>
                    <div class='input-group date' id='modified-datetimepicker'>
                        <span class="input-group-addon mr-0">Fecha</span>
                        <input name="input-modified" id="input-modified" type='text' class="column_search form-control datetimepicker" data-column="19" />
                        <span class="input-group-addon mr-0">
                            <span class="glyphicon icon-calendar"></span>
                        </span>
                    </div>
                </div>
              </div>
              <div class="col-md-4">
                  <?= $this->Form->input('cover', ['label' => __('Portada'), 'class'=> 'column_search', 'data-column' => 20, 'checked' => false]) ?>
                  <?= $this->Form->input('enabled', ['label' => __('Habilitado'), 'class'=> 'column_search', 'data-column' => 16, 'checked' => false]) ?>
              </div>
          </div>

          <?= $this->Form->end() ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-clear-filter" >Limpiar</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-restore',
        'name' =>  'Restaurar',
        'icon' =>  'icon-undo2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-posts', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-posts']);
?>

<script type="text/javascript">

    var table_posts = null;
    var post_selected = null;

    $(document).ready(function () {

        $('#table-posts').removeClass('display');

    	table_posts = $('#table-posts').DataTable({
    	    "order": [[ 0, 'asc' ]],
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
    	    "autoWidth": true,
    	    "scrollY": '450px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "deferRender": true,
		    "ajax": {
                "url": "/app/posts/deleted.json",
                "dataSrc": "posts"
            },
            "columns": [
                { 
                    "data": "publication_date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return "Sin publicar";
                    }
                },
                { 
                    "data": "confirmation_date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return "Sin confirmación";
                    }
                },
                { 
                    "data": "status",
                    "render": function ( data, type, row ) {
                        return sessionPHP.paraments.post.status[data].name;
                    }
                },
                { "data": "code" },
                { "data": "id" },
                { "data": "title" },
                { "data": "description" },
                { 
                    "data": "type_property",
                    "render": function ( data, type, row ) {
                        return sessionPHP.paraments.post.property[data];
                    }
                },
                { 
                    "data": "zone",
                    "render": function ( data, type, row ) {
                        return sessionPHP.paraments.post.zone[data];
                    }
                },
                { "data": "address" },
                { 
                    "data": "operation",
                    "render": function ( data, type, row ) {
                        return sessionPHP.paraments.post.operation[data];
                    }
                },
                { 
                    "data": "room",
                    "render": function ( data, type, row ) {
                        return sessionPHP.paraments.post.room[data];
                    }
                },
                { 
                    "data": "bath",
                    "render": function ( data, type, row ) {
                        return sessionPHP.paraments.post.bath[data];
                    }
                },
                { 
                    "data": "garage",
                    "render": function ( data, type, row ) {
                        return sessionPHP.paraments.post.garage[data];
                    }
                },
                { 
                    "data": "price",
                    "render": function ( data, type, row ) {
                        if (data == null) {
                            data = "Sin cargar precio";
                        } else {
                            if (row.money == null) {
                                data = "Sin cargar moneda";
                            } else {
                                data = sessionPHP.paraments.post.money[row.money] + ' ' + new Intl.NumberFormat("es-AR",  {minimumFractionDigits: 2, maximumFractionDigits: 2}).format(data);
                            }
                        }
                        return data;
                    }
                },
                { 
                    "data": "money",
                    "render": function ( data, type, row ) {
                        if (data == null) {
                            data = "Sin cargar moneda";
                        }
                        return data;
                    }
                },
                { 
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        if (type === 'display') {
                            var enabled = '<i class="fa fa-times" aria-hidden="true"></i>';
                            if (data) {
                                enabled = '<i class="fa fa-check" aria-hidden="true"></i>';
                            }
                            data = enabled;
                        }
                        return data;
                    }
                },
                { 
                    "data": "images",
                    "render": function ( data, type, row ) {
                        if (data.length > 0) {
                            if (data.length == 1) {
                                return "1 Imagen";
                            } else {
                                return data.length + " Imagenes";
                            }
                        }
                        return "Sin Imagen";
                    }
                },
                { 
                    "data": "map",
                    "render": function ( data, type, row ) {
                        if (data) {
                            data = "Cargado";
                        } else {
                            data = "Sin Cargar";
                        }
                        return data;
                    }
                },
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "modified",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "cover",
                    "render": function ( data, type, row ) {
                        if (type === 'display') {
                            var enabled = '<i class="fa fa-times" aria-hidden="true"></i>';
                            if (data) {
                                enabled = '<i class="fa fa-check" aria-hidden="true"></i>';
                            }
                            data = enabled;
                        }
                        return data;
                    }
                }
            ],
    	    "columnDefs": [
    	         { "type": 'date-custom', targets: [0, 1, 19, 20] },
    	         {
                    "targets": [ 1, 4, 5, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21 ],
                    "visible": false,
                    "searchable": true
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
    	    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[ 50, 100, -1], [ 50, 100, "Todas"]],
    	});

    	$('#table-posts_filter').closest('div').before($('#btns-tools').contents());

    	loadPreferences(table_posts, 'posts-deleted');
    });

    $('.btn-hide-column').click(function() {
        $('.modal-hide-columns-posts').modal('show');
    });

    $('#table-posts_wrapper .tools').append($('#btns-tools').contents());

    $('#table-posts').on( 'init.dt', function () {
        createModalHideColumn(table_posts, '.modal-hide-columns-posts');
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-posts').tableExport({tableName: 'Publicaciones', type:'excel', escape:'false'});
            }
        });
    });

    $('#btns-tools').show();

    $('#table-posts tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_posts.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            post_selected = table_posts.row( this ).data();

            $('.modal-posts').modal('show');
        }
    });

    $('.column_search').on( 'keyup', function () {

        table_posts
            .columns( $(this).data('column') )
            .search( this.value )
            .draw();

        checkStatusFilter();
    });

    $('#created-datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    }).on('dp.change', function (ev) {

        table_posts
            .columns( 20 )
            .search( $('#input-created').val() )
            .draw();

        checkStatusFilter();
    });

    $('#modified-datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    }).on('dp.change', function (ev) {

        table_posts
            .columns( 21 )
            .search( $('#input-modified').val() )
            .draw();

        checkStatusFilter();
    });

    $('#publication_date-datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    }).on('dp.change', function (ev) {

        table_posts
            .columns( 0 )
            .search( $('#input-created').val() )
            .draw();

        checkStatusFilter();
    });

    $('#confirmation_date-datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    }).on('dp.change', function (ev) {

        table_posts
            .columns( 1 )
            .search( $('#input-modified').val() )
            .draw();

        checkStatusFilter();
    });

    $('input:checkbox.column_search').change(function() {

        table_posts
            .columns( $(this).data('column') )
            .search( $(this).is(":checked") )
            .draw();

        checkStatusFilter();
    });

    $('select.column_search').change(function() {

        var search = this.value;

        if (jQuery.inArray($(this).data('column'), [2, 7, 8, 9, 10, 11, 12, 13]) !== -1) {
            if ($(this).find('option:selected').val() == "") {
                $('.column_search').val('');
                table_posts.search( '' ).columns().search( '' ).draw();
                $('.column_search').prop('checked', false);
                checkStatusFilter();
            } else {
                search = $(this).find('option:selected').text();
            }
        } else if ($(this).data('column') == 15) {
            search = $(this).find('option:selected').val();
        }

        table_posts
            .columns( $(this).data('column') )
            .search( search )
            .draw();

        checkStatusFilter();
    });

    $('.btn-clear-filter').click(function() {
        $('.column_search').val('');
        table_posts.search( '' ).columns().search( '' ).draw();
        $('.column_search').prop('checked', false);
        checkStatusFilter();
    });

    $('#btn-restore').click(function(){
        var action = '/app/posts/restore/' + table_posts.$('tr.selected').attr('id');
        window.open(action, '_self');
    });

    $('#btn-delete').click(function() {

        var text = "¿Está Seguro que desea eliminar la Publicación?. Se eliminaran las imagenes asociadas a dicha publicaión y de la lista de Favoritos de Clientes";
        var id  = post_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/posts/remove/' + id, 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
                .find('#replacer').submit();
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
