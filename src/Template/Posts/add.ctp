<style type="text/css">

    #map {
		height: 370px;
	}

	#floating-panel {
		top: 10px;
		left: 25%;
		z-index: 5;
		background-color: #fff;
		padding: 5px;
		border: 1px solid #999;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
		padding-left: 10px;
	}

	#submit-map {
        padding: 10px;
        top: 2px;
    }

    #update-map {
        padding: 10px;
        top: 2px;
    }

</style>

<?= $this->Form->create($post, ['id' => 'form-post-add']) ?>

    <div class="row">

        <div class="col-xl-12">
            <label style="font-size: 20px;">Nro Propiedad: <?= $number_property ?></label>
        </div>

        <div class="col-xl-3">
            <?php
                echo $this->Form->input('code', ['label' => 'Referencia', 'required' => true]);
            ?>
        </div>

        <div class="col-xl-3">
            <?php
                echo $this->Form->input('type_property', ['label' => 'Tipo Propiedad', 'options' => $this->request->session()->read('paraments')->post->property, 'value' => '',  'required' => true]);
            ?>
        </div>

        <div class="col-xl-3">
            <?php
                echo $this->Form->input('zone', ['label' => 'Zona', 'options' => $this->request->session()->read('paraments')->post->zone, 'value' => '']);
            ?>
        </div>

        <div class="col-xl-3">
            <?php
                echo $this->Form->input('address', ['label' => 'Domicilio', 'type' => 'text',  'required' => true]);
            ?>
        </div>

        <div class="col-xl-3">
            <?php
                echo $this->Form->input('operation', ['label' => 'Tipo Operación', 'options' => $this->request->session()->read('paraments')->post->operation, 'value' => '',  'required' => true]);
            ?>
        </div>

        <div class="col-xl-3">
            <?php
                echo $this->Form->input('room', ['label' => 'Dormitorios', 'options' => $this->request->session()->read('paraments')->post->room, 'value' => '']);
            ?>
        </div>
        
        <div class="col-xl-3">
            <?php
                echo $this->Form->input('environment', ['label' => 'Ambientes', 'options' => $this->request->session()->read('paraments')->post->environment, 'value' => '']);
            ?>
        </div>

        <div class="col-xl-3">
            <?php
                echo $this->Form->input('bath', ['label' => 'Baño', 'options' => $this->request->session()->read('paraments')->post->bath, 'value' => '',  'required' => true]);
            ?>
        </div>

        <div class="col-xl-3">
            <?php
                echo $this->Form->input('garage', ['label' => 'Cochera', 'options' => $this->request->session()->read('paraments')->post->garage, 'value' => '',  'required' => true]);
            ?>
        </div>

        <div class="col-xl-3">
            <?php
                echo $this->Form->input('money', ['label' => 'Moneda', 'options' => $this->request->session()->read('paraments')->post->money, 'value' => '']);
            ?>
        </div>

        <div class="col-xl-3">
            <?php
                echo $this->Form->input('price', ['label' => 'Precio']);
            ?>
        </div>

        <div class="col-xl-3">
            <div class="form-group select required">
                <label class="control-label" for="status">Estado</label>
                <select name="status" required="required" id="status" class="form-control">
                    <?php foreach ($this->request->session()->read('paraments')->post->status as $key => $st): ?>
                        <option value="<?= $key ?>" <?= ($key == 0) ? 'selected="selected"': '' ?>><?= $st->name ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>

        <div class="col-xl-3">
            <?php
                echo $this->Form->input('cover', ['label' => 'Portada']);
                echo $this->Form->hidden('post_id', ['value' => $post->id]);
            ?>
        </div>

        <div class="col-xl-6">
            <?php
                echo $this->Form->input('title', ['type' => 'textarea', 'label' => 'Titulo', 'required' => true]);
            ?>
        </div>

        <div class="col-xl-6">
            <?php
                echo $this->Form->input('description', ['type' => 'textarea', 'label' => 'Descripción', 'required' => true]);
            ?>
        </div>

        <div class="col-xl-3">
            <?php
                echo $this->Form->input('zoom_map_pdf', ['label' => 'Zoom del Mapa PDF', 'type' => 'number']);
            ?>
        </div>

        <div class="col-lg-12 col-xl-12">

            <?php
                echo $this->Form->input('lat', ['type' => 'hidden', 'id' => 'coodmapslat']);
                echo $this->Form->input('lng', ['type' => 'hidden', 'id' => 'coodmapslng']);
            ?>
        	<div id="floating-panel">
    		    <div class="row">
    		        <div class="col-md-12">
        		        <div class="input-group">
        		            <span class="input-group-btn">
                                <button id="update-map" title="Recargar Mapa" class="btn btn-info" type="button"><span class="text-white glyphicon icon-map2" aria-hidden="true"></span></button>
                            </span>
                            <input id="addressmap" type="text" class="form-control margin-top-5" value="" placeholder="Calle #, Ciudad, Provincia">
                            <span class="input-group-btn">
                                <button id="submit-map" title="Buscar Dirección" class="btn btn-success" type="button"><span class="text-white glyphicon icon-search" aria-hidden="true"></span></button>
                            </span>
                        </div>
    		        </div>
    		    </div>
    		</div>

    		<div id="map" class="mb-3"></div>
        </div>

        <div class="col-xl-12">
            <?= $this->Form->button(__('Agregar'), ['class' => 'btn-success margin-left-15', 'name' => 'btn-add', 'value' => '']) ?>
            <?= $this->Form->button(__('Agregar y Salir'), ['class' => 'btn-success margin-left-15', 'name' => 'btn-add', 'value' => 'exit']) ?>
        </div>

    <br>
    </div>

<?= $this->Form->end() ?>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZG7dni5-GuQaooMmy6fLtYf3B_kHtAOY">
</script>

<script type="text/javascript">

    $(document).ready(function() {
        initMap();
    });

    $("#form-post-add").submit(function(e) {

        var environment = $('#environment').val();
        var room = $('#room').val();
        if (environment == "" && room =="") {
            generateNoty('warning', 'Debe seleccionar una opción en ambientes o dormitorios.');
            e.preventDefault();
        }
    });

    $('#update-map').click(function() {
            initMap();
        });

        $('form input').on('keypress', function(e) {
            return e.which !== 13;
        });

        var map = null;
        var geocoder = null;
        var marker = null;

    	function initMap() {

    	    console.log('initMap');

    		geocoder = new google.maps.Geocoder;

    		var myLatlng = {lat: -27.457300000000000039790393202565610408782958984375, lng: -58.99560000000000314912540488876402378082275390625};

    		var mapOptions = {
    			zoom: 14,
    			center: myLatlng,
    			mapTypeId: 'hybrid'   // roadmap, satellite, hybrid, terrain 
    		};

    		map = new google.maps.Map(document.getElementById('map'), mapOptions);

    		document.getElementById('submit-map').addEventListener('click', function() {
    			geocodeAddress(geocoder, map);
    		});

    		var init = {lat: -27.457300000000000039790393202565610408782958984375, lng: -58.99560000000000314912540488876402378082275390625};

            var latLng = new google.maps.LatLng(init.lat, init.lng);

            map.setCenter(latLng);

    		map.addListener('click', function(event) {
    			addMarker(event.latLng);	    
    		});
        }

        function refreshMap() {
            console.log('refreshMap');
            google.maps.event.trigger(map, 'resize');
        }

        $('input#addressmap').keypress(function(event) {

            if ( event.which == 13 ) {
                geocodeAddress(geocoder, map);
            }
        });

    	function addMarker(location) {

    		if (marker != null) {
    			clearMarkers();
    		}

    		marker = new google.maps.Marker({
    			position: location,
    			map: map
    		});
    		marker.setMap(map);	

            var lat = location.lat();

            var lng = location.lng();

    		$('#coodmapslat').val(lat);
    		$('#coodmapslng').val(lng);	
    	}

    	function clearMarkers() {
    		marker.setMap(null);
    	}

    	function geocodeAddress(geocoder, map) {

    		var address = document.getElementById('addressmap').value;

    		geocoder.geocode({
    			'address': address
    			}, function(results, status) {

    				if (status === google.maps.GeocoderStatus.OK) {	
    					addMarker(results[0].geometry.location)  	    
    					map.setCenter(results[0].geometry.location);
    					map.setZoom(14);

        			} else {
        				if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
        			        bootbox.alert('No se encontraron Resultados');
        			    } else {
        			        window.alert('Geocode was not successful for the following reason: ' + status);
        			    }
        			}
    		});
    	}

</script>

