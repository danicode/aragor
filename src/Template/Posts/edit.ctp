<style type="text/css">

    #map {
		height: 370px;
	}

	#floating-panel {
		top: 10px;
		left: 25%;
		z-index: 5;
		background-color: #fff;
		padding: 5px;
		border: 1px solid #999;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
		padding-left: 10px;
	}

	#submit-map {
        padding: 10px;
        top: 2px;
    }

    #update-map {
        padding: 10px;
        top: 2px;
    }

</style>

<div class="row">
    <div class="col-xl-12">
    <?= $this->Form->create($post,  ['class' => 'form-load']) ?>
    
        <div class="row">
    
            <div class="col-xl-12">
                <label style="font-size: 20px;">Nro Propiedad: <?= $number_property ?></label>
            </div>
    
            <div class="col-xl-3">
                <?php
                    echo $this->Form->input('code', ['label' => 'Referencia', 'required' => true]);
                ?>
            </div>
    
            <div class="col-xl-3">
                <?php
                    echo $this->Form->input('type_property', ['label' => 'Tipo Propiedad', 'options' => $this->request->session()->read('paraments')->post->property,  'required' => true]);
                ?>
            </div>
    
            <div class="col-xl-3">
                <?php
                    echo $this->Form->input('zone', ['label' => 'Zona', 'options' => $this->request->session()->read('paraments')->post->zone]);
                ?>
            </div>

            <div class="col-xl-3">
                <?php
                    echo $this->Form->input('address', ['label' => 'Domicilio', 'type' => 'text',  'required' => true]);
                ?>
            </div>
    
            <div class="col-xl-3">
                <?php
                    echo $this->Form->input('operation', ['label' => 'Tipo Operación', 'options' => $this->request->session()->read('paraments')->post->operation,  'required' => true]);
                ?>
            </div>
    
            <div class="col-xl-3">
                <?php
                    echo $this->Form->input('room', ['label' => 'Dormitorios', 'options' => $this->request->session()->read('paraments')->post->room]);
                ?>
            </div>
            
             <div class="col-xl-3">
                <?php
                    echo $this->Form->input('environment', ['label' => 'Ambientes', 'options' => $this->request->session()->read('paraments')->post->environment]);
                ?>
            </div>
    
            <div class="col-xl-3">
                <?php
                    echo $this->Form->input('bath', ['label' => 'Baño', 'options' => $this->request->session()->read('paraments')->post->bath,  'required' => true]);
                ?>
            </div>
    
            <div class="col-xl-3">
                <?php
                    echo $this->Form->input('garage', ['label' => 'Cochera', 'options' => $this->request->session()->read('paraments')->post->garage,  'required' => true]);
                ?>
            </div>
    
            <div class="col-xl-3">
                <?php
                    echo $this->Form->input('money', ['label' => 'Moneda', 'options' => $this->request->session()->read('paraments')->post->money]);
                ?>
            </div>
    
            <div class="col-xl-3">
                <?php
                    echo $this->Form->input('price', ['label' => 'Precio']);
                ?>
            </div>

            <div class="col-xl-3">
                <div class="form-group select required">
                    <label class="control-label" for="status">Estado</label>
                    <select name="status" required="required" id="status" class="form-control">
                        <?php foreach ($this->request->session()->read('paraments')->post->status as $key => $st): ?>
                            <option value="<?= $key ?>" <?= ($key == $post->status) ? 'selected="selected"': '' ?>><?= $st->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="col-xl-1">
                <?php
                    echo $this->Form->input('cover', ['label' => 'Portada']);
                ?>
            </div>

            <div class="col-xl-2">
                <?php
                    echo $this->Form->input('enabled', ['label' => 'Habilitado']);
                ?>
            </div>

            <div class="col-xl-6">
                <?php
                    echo $this->Form->input('title', ['type' => 'textarea', 'label' => 'Titulo', 'required' => true]);
                ?>
            </div>

            <div class="col-xl-6">
                <?php
                    echo $this->Form->input('description', ['type' => 'textarea', 'label' => 'Descripción', 'required' => true]);
                ?>
            </div>

            <div class="col-xl-3">
                <?php
                    echo $this->Form->input('zoom_map_pdf', ['label' => 'Zoom del Mapa PDF', 'type' => 'number']);
                ?>
            </div>

            <div class="col-lg-12 col-xl-12">

                <?php
                    echo $this->Form->input('lat', ['type' => 'hidden', 'id' => 'coodmapslat']);
                    echo $this->Form->input('lng', ['type' => 'hidden', 'id' => 'coodmapslng']);
                ?>
            	<div id="floating-panel">
        		    <div class="row">
        		        <div class="col-md-12">
            		        <div class="input-group">
            		            <span class="input-group-btn">
                                    <button id="update-map" title="Recargar Mapa" class="btn btn-info" type="button"><span class="text-white glyphicon icon-map2" aria-hidden="true"></span></button>
                                </span>
                                <input id="addressmap" type="text" class="form-control margin-top-5" value="" placeholder="Calle #, Ciudad, Provincia">
                                <span class="input-group-btn">
                                    <button id="submit-map" title="Buscar Dirección" class="btn btn-success" type="button"><span class="text-white glyphicon icon-search" aria-hidden="true"></span></button>
                                </span>
                            </div>
        		        </div>
        		    </div>
        		</div>

        		<div id="map" class="mb-3"></div>
            </div>

            <div class="col-xl-12">
                <?= $this->Html->link(__('Cancelar'), ["controller" => "Posts", "action" => "index"], ['class' => 'btn btn-default']) ?>
                <?= $this->Form->button(__('Guardar'), ['class' => 'btn-submit btn-success']) ?>
            </div>
        <br>
        </div>
    
    <?= $this->Form->end() ?>
    </div>

    <div class="col-xl-12">
        <div class="row">
            <div class="col-md-11">
                <legend>
                    <?= __('Galeria') ?>
                    <a style="margin-left: 5px;" class="btn btn-default" title="Nueva Imagen" href="#" role="button" id="btn-add-image" >
                        <span class="glyphicon icon-plus" aria-hidden="true"></span>
                    </a>
                </legend>
                
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="gallery" class="row">
                    <?php foreach ($post->images as $image): ?>
                        <div class='col-md-4 mb-5'>
                            <img class="img-fluid" style="max-width: 100%; height: auto;" src="/app/images/<?= $image->url ?>"></img>
                            <div>
                                <a class="checkbox chck-main" data-id="<?= $image->id ?>" href='javascript:void(0)'>
                                   <?php if ($image->main): ?>
                                       <input style="margin-top: 12px;" type="checkbox" checked value="1">
                                   <?php else: ?>
                                       <input style="margin-top: 12px;" type="checkbox" value="0">
                                   <?php endif; ?>
                                   <?= __('Principal') ?>
                                </a>
                                <a style="margin-left: 34px;" class='btn-delete-image' data-id="<?= $image->id ?>" href='javascript:void(0)'><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="assets-popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="gridSystemModalLabel"><?=  __('Añadir Imagen') ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
           <?= $this->Form->create(null, ['url' => ['controller' => 'Posts' , 'action' => 'addImage', $post->id], 'enctype' => "multipart/form-data", 'class' => 'upload-form' ]) ?>
            <fieldset>
                <?php
                    echo $this->Form->file('files[]', ['required' => true, 'multiple'=>'multiple', 'id' => 'file_upload']);
                    echo $this->Form->input('post_id', ['type' => 'hidden', 'value' => $post->id]);
                ?>
            </fieldset>
            <br>

            <button type="button" class="btn btn-default" data-dismiss="modal"><?=  __('Cancelar') ?></button>
            <?= $this->Form->button(__('Agregar')) ?>
            <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZG7dni5-GuQaooMmy6fLtYf3B_kHtAOY">
</script>

<script type="text/javascript">

    var post = null;

    $(document).ready(function() {
        post = <?= json_encode($post); ?>;
        initMap();
    });
    
    $('#update-map').click(function() {
        initMap();
    });

    $('form input').on('keypress', function(e) {
        return e.which !== 13;
    });

    var map = null;
    var geocoder = null;
    var marker = null;

    function isEmpty(val) {
        return (val === undefined || val == null || val == 0 || val.length <= 0) ? true : false;
    }

	function initMap() {

		geocoder = new google.maps.Geocoder;

		var myLatlng = {lat: -27.457300000000000039790393202565610408782958984375, lng: -58.99560000000000314912540488876402378082275390625};

		var mapOptions = {
			zoom: 14,
			center: myLatlng,
			mapTypeId: 'hybrid'   // roadmap, satellite, hybrid, terrain 
		};

		map = new google.maps.Map(document.getElementById('map'), mapOptions);

		document.getElementById('submit-map').addEventListener('click', function() {
			geocodeAddress(geocoder, map);
		});

	    var init = myLatlng;

        var haveMarker = false;
        if (!isEmpty(post.lat) || !isEmpty(post.lng)) {
            haveMarker = true;
            init.lat = post.lat;
            init.lng = post.lng;
        }

        var latLng = new google.maps.LatLng(init.lat, init.lng);
        if (haveMarker) {
           addMarker(latLng);
        }

        map.setCenter(latLng);

		map.addListener('click', function(event) {
			addMarker(event.latLng);	    
		});
    }

    function refreshMap() {
        google.maps.event.trigger(map, 'resize');
    }

    $('input#addressmap').keypress(function(event) {
        if ( event.which == 13 ) {
            geocodeAddress(geocoder, map);
        }
    });

	function addMarker(location) {

		if (marker != null) {
			clearMarkers();
		}

		marker = new google.maps.Marker({
			position: location,
			animation: google.maps.Animation.DROP,
			map: map
		});
		marker.addListener('click', toggleBounce);
		marker.setMap(map);

        var lat = location.lat();
        var lng = location.lng();

		$('#coodmapslat').val(lat);
		$('#coodmapslng').val(lng);	
	}

	function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

	function clearMarkers() {
		marker.setMap(null);
	}			

	function geocodeAddress(geocoder, map) {

		var address = document.getElementById('addressmap').value;

		geocoder.geocode({
			'address': address
			}, function(results, status) {

				if (status === google.maps.GeocoderStatus.OK) {	
					addMarker(results[0].geometry.location)  	    
					map.setCenter(results[0].geometry.location);
					map.setZoom(14);

    			} else {
    				if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
    			        bootbox.alert('No se encontraron Resultados');
    			    } else {
    			        window.alert('Geocode was not successful for the following reason: ' + status);
    			    }
    			}
		});
	}

    $('a#btn-add-image').click(function() {
        $('#assets-popup').modal('show');
    });

    $('a.btn-delete-image').click(function() {

        var id  = $(this).data('id');
        var text = '¿Está Seguro que desea eliminar la Imagen? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/app/posts/deleteImage/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
                    .find('#replacer').submit();
            }
        });
    });

    $(document).on("click", "a.chck-main", function(e) {

        var id  = $(this).data('id');
        $('body')
            .append( $('<form/>').attr({'action': '/app/posts/mainImage/', 'method': 'post', 'id': 'replacer'})
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
            .find('#replacer').submit();
    });

    $(".upload-form").submit(function(e) {

        var files = $('#file_upload')[0].files;
        var flag = true;
        var size_total = 0;
        $.each(files, function( index, value ) {
            size_total += value.size;
        });
        if (size_total >= 50000000) {
            generateNoty('warning', 'El tamaño total de subida debe ser menor a 50 MB');
            e.preventDefault();
        }
    });

    $(".form-load").submit(function(e) {

        var environment = $('#environment').val();
        var room = $('#room').val();
        if (environment == "" && room =="") {
            generateNoty('warning', 'Debe seleccionar una opción en ambientes o dormitorios.');
            e.preventDefault();
        }
    });

</script>
