<style type="text/css">

    .vertical-table {
        width:100%;
    }

    table.vertical-table tbody tr {
        border-bottom: 1px solid #d8d8d8;
    }

    .sub-title {
        color: #696868;
        font-size: 21px;
        font-weight: bold;
        margin-bottom: 10px;
    }

</style>

<div class="row">
    <div class="col-md-12">
        <legend class="sub-title">Nro Propiedad: <span class="bold-color"><?= $post->number_property ?></span></legend>
    </div>
    <div class="col-md-6">
        <br>
        <table class="vertical-table">

            <tr>
                <th><?= __('Referencia') ?></th>
                <td class="pull-right">
                <b><?= $post->code ?></b>
                </td>
            </tr>
            <tr>
                <th><?= __('Título') ?></th>
                <td class="pull-right">
                <?= $post->title ?>
                </td>
            </tr>
            <tr>
                <th><?= __('Descripción') ?></th>
                <td class="pull-right">
                <?= $post->description ?>
                </td>
            </tr>
            <tr>
                <th><?= __('Tipo de Propiedad') ?></th>
                <?php
                    $property = $post->type_property;
                ?>
                <td class="pull-right"><?= $this->request->session()->read('paraments')->post->property->$property ?></td>
            </tr>
            <tr>
                <th><?= __('Zona') ?></th>
                <?php
                    $zone = $post->zone;
                ?>
                <td class="pull-right"><?= $this->request->session()->read('paraments')->post->zone->$zone ?></td>
            </tr>
            <tr>
                <th><?= __('Domicilio') ?></th>
                <td class="pull-right"><?= $post->address ?></td>
            </tr>
            <tr>
                <th><?= __('Tipo de Operación') ?></th>
                <?php
                    $operation = $post->operation;
                ?>
                <td class="pull-right"><?= $this->request->session()->read('paraments')->post->operation->$operation ?></td>
            </tr>
            <tr>
                <th><?= __('Dormitorios') ?></th>
                <?php
                    $room = $post->room;
                ?>
                <td class="pull-right"><?= $this->request->session()->read('paraments')->post->room->$room ?></td>
            </tr>
            <tr>
                <th><?= __('Ambientes') ?></th>
                <?php
                    $environment = $post->environment;
                ?>
                <td class="pull-right"><?= $this->request->session()->read('paraments')->post->environment->$environment ?></td>
            </tr>
            <tr>
                <th><?= __('Baño') ?></th>
                <?php
                    $bath = $post->bath;
                ?>
                <td class="pull-right"><?= $this->request->session()->read('paraments')->post->bath->$bath ?></td>
            </tr>
            <tr>
                <th><?= __('Cochera') ?></th>
                <?php
                    $garage = $post->garage;
                ?>
                <td class="pull-right"><?= $this->request->session()->read('paraments')->post->garage->$garage ?></td>
            </tr>
            <tr>
                <th><?= __('Moneda') ?></th>
                <?php
                    $money = $post->money;
                ?>
                <td class="pull-right"><?= $this->request->session()->read('paraments')->post->money->$money ?></td>
            </tr>
            <tr>
                <th><?= __('Estado') ?></th>
                <?php
                    $status = $post->status;
                ?>
                <td class="pull-right"><?= $this->request->session()->read('paraments')->post->status->$status->name ?></td>
            </tr>
            <tr>
                <th><?= __('Precio') ?></th>
                
                <td class="pull-right"><b>$<?= number_format($post->price ,2,',','.')?></b></td>
            </tr>
            <tr>
                <th><?= __('Creado') ?></th>
                <td class="pull-right"><?= date('d-m-Y H:i', strtotime($post->created))?></td>
            </tr>
            <tr>
                <th><?= __('Modificado') ?></th>
                <td class="pull-right"><?= date('d-m-Y H:i', strtotime($post->modified))?></td>
            </tr>
            <tr>
                <th><?= __('Fecha de Publicación') ?></th>
                <td class="pull-right"><?= date('d-m-Y H:i', strtotime($post->publication_date))?></td>
            </tr>
            <tr>
                <th><?= __('Fecha de Confirmación') ?></th>
                <td class="pull-right"><?= date('d-m-Y H:i', strtotime($post->confirmation_date))?></td>
            </tr>
            <tr>
                <th><?= __('Portada') ?></th>
                <td class="pull-right"><?= ($post->cover) ? 'SI' : 'NO' ?></td>
            </tr>
            <tr>
                <th><?= __('Mapa') ?></th>
                <td class="pull-right"><?= ($post->map) ? 'Cargado' : 'Sin Cargar' ?></td>
            </tr>
            <tr>
                <th><?= __('Habilitado') ?></th>
                <td class="pull-right"><?= ($post->enabled) ? 'SI' : 'NO' ?></td>
            </tr>

        </table>

    </div>

</div>
