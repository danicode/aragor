<style type="text/css">

    p {
        font-size: 18px;
        font-family: 'Work Sans', sans-serif;
        color: #000;
    }

    .btn {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }

    .btn-primary {
        color: white;
        background-color: #2C3E50;
        border-color: #2C3E50;
        font-weight: 700;
    }

    .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open .dropdown-toggle.btn-primary {
        color: white;
        background-color: #1a242f;
        border-color: #161f29;
    }

    a {
        text-decoration: none !important;
    }

</style>

<br>
<br>

<section id="services">
    <div class="container">
        <div style="background: white;adding-bottom: 10px;padding-top: 10px; text-align: center;">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <center>
                         <?php echo $this->Html->image('logo.png', ['alt' => '', 'width' => 240]);?>
                    </center>
                </div>
            </div>
            <div class="row" style="margin-top: 18px;">
                <div class="col-md-12 text-center">
                    <h2>Ha ocurrido un error</h2>
                    <p>Para volver al Sitio nuevamente presione el botón</p>
                    <?php
                        echo $this->Html->link(
                            'Volver',
                            ['controller' => 'Inicio', 'action' => 'index', '_full' => true],
                            ['class' => 'btn btn-primary']
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>