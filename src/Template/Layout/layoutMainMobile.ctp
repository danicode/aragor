
<!5 html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="ispbrain-icon.ico" type="image/x-icon">
   
   
    <?php echo $this->Html->meta(
          'ispbrain-icon.ico',
          '/ispbrain-icon.ico',
          array('type' => 'icon')
      );
      ?>
    <title>
    </title>


    <?= $this->Html->css([
      
      'font-awesome/css/font-awesome.min',
      
      '/vendor/Animate/animate',
      '/vendor/noty/lib/noty',
      
      'flaticon/flaticon',
      
      '/vendor/DataTables/datatables.min',
      '/vendor/jquery-ui-1.12.1.custom/jquery-ui.min',
 
      '/vendor/icomoon/style',
  
      
      // '/vendor/multilevelpushmenu/css/jquery.multilevelpushmenu',
      // '/vendor/multilevelpushmenu/css/responsive',
      
      '/vendor/multi-level-push-menu-master/jquery.multilevelpushmenu',
      
      
      'jquery-te/jquery-te-1.4.0',
      
      
      '/vendor/Bootstrap/css/bootstrap.min',
      '/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker',
      
      'common',
      'layout',
      'forms',
      'tables',
      
      'multi-level-push-menu-master.mobile'
      
      
      
      ]) ?>
      
      <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.min.css">
      

    
    <?= $this->Html->script([
      
      
       
      '/vendor/jQuery-3.2.1/jquery-3.2.1',
      
      // '/vendor/multilevelpushmenu/js/jquery.multilevelpushmenu',
      
      '/vendor/multi-level-push-menu-master/jquery.multilevelpushmenu',
      
      
      
      
      '/vendor/popper.js-1.12.5/dist/umd/popper.min',
      
      '/vendor/bootstrap-datetimepicker/moment-with-locales',
      
      '/vendor/Bootstrap/js/bootstrap',
      '/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker',
      
      'bootbox.min',
      
      // 'noty/packaged/jquery.noty.packaged.min',
      '/vendor/noty/lib/noty',
      
      '/vendor/DataTables/datatables',
      '/vendor/dataTables-custom/sort',

      '/vendor/jquery-ui-1.12.1.custom/jquery-ui.min',
      
      'table2excel/jquery.table2excel',
      'tableExpor/jquery.base64',
      'tableExpor/tableExport',
      'tableExpor/html2canvas',
      'tableExpor/jspdf/libs/base64',
      'tableExpor/jspdf/libs/sprintf',
      'tableExpor/jspdf/jspdf',
      
     
      // '/vendor/multilevelpushmenu/js/responsive',
      
     

      'jquery-te/jquery-te-1.4.0.min',
      
      'common.mobile',
      'multi-level-push-menu-master.mobile'
      
      ]) ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
    <script type="text/javascript" src="https://oss.maxcdn.com/libs/modernizr/2.6.2/modernizr.min.js"></script>
    
    <script src="/ispbrain/Highstock/code/highstock.js"></script>
    <script src="/ispbrain/Highstock/code/modules/exporting.js"></script>
    
    <script type="text/javascript" src="/ispbrain/ckeditor/ckeditor.js"></script>

    <script type="text/javascript">
    var sessionPHP = <?=json_encode($_SESSION)?>;
    </script>
    
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    
    
    
</head>



<body>

    <?= $this->element('preloader') ?>

    <?= $this->Flash->render() ?>
    <?= $this->Flash->render('auth') ?>

    <?php if($loggedIn):?>
        <div class="container-fluid">
          
            
              <?= $this->element('Layout/title.mobile') ?>
          
           
                <?= $this->fetch('content') ?>
      
               <div id="menu" class="fixed"></div>
          
        </div>
    <?php else:?>
        <div class="container">
          <?= $this->fetch('content') ?>
        </div>
    <?php endif;?>
    
    <?= $this->fetch('script') ?>
    
</body>
</html>

   


