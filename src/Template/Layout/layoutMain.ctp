<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Aragor Iker';
?>
<!DOCTYPE html>
<html>
  <head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/app/ispbrain-icon.ico" type="image/x-icon">

    <?php echo $this->Html->meta(
      'logo-icon.ico',
      '/logo-icon.ico',
      array('type' => 'icon')
      );
    ?>
    <title>
      <?= $cakeDescription ?>
    </title>
    
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <?= $this->Html->css([
        'main/bootstrap/css/bootstrap.min',
        'main/font-awesome/css/font-awesome.min',
        'main/freelancer',
        '/vendor/Animate/animate',
        '/vendor/noty/lib/noty',
    ]) ?>
    <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">

    <?= $this->Html->script([
      'main/jquery/jquery.min',
      'main/bootstrap/js/bootstrap.bundle.min',
      'main/jquery-easing/jquery.easing.min',
      'main/jqBootstrapValidation',
      'main/contact_me',
      '/vendor/noty/lib/noty',
      'bootbox.min',
    ]) ?>

  <script type="text/javascript">
    /** 
       * notificaciones 
       * type: warning, error, information, success
       * text: html
      */
    function generateNoty(type, text, closeWith = ['click']) {
      
        var animationIn  = '';
        var animationOut  = '';
        
        switch(type){
          case 'error':
            animationIn  = 'shake';
            animationOut  = 'fadeOut';
            break;
          case 'warning':
            animationIn  = 'shake';
            animationOut  = 'fadeOut';
            break;
          case 'success':
            animationIn  = 'bounceInRight';
            animationOut  = 'bounceOutLeft';
            break;
          case 'information': 
            animationIn  = 'bounceInRight';
            animationOut  = 'bounceOutLeft';
            break;
        }

          var n = new Noty({
              text        : text,
              type        : type,
              dismissQueue: true,
              layout      : 'topCenter', /*top, topLeft, topCenter, topRight, center, centerLeft, centerRight, bottom, bottomLeft, bottomCenter, bottomRight*/
              closeWith   : closeWith, /*['click', 'button', 'hover', 'backdrop']*/
              theme       : 'semanticui',
              maxVisible  : 5,
              animation   : {
                  open  : 'animated '+animationIn,
                  close : 'animated '+animationOut,
                  easing: 'swing',
                  speed : 500
              }
          });
          
          n.show();
          
          return n;
      }

  </script>

  <?= $this->fetch('meta') ?>
  <?= $this->fetch('css') ?>

  </head>
  <body id="page-top">

    <?php echo $this->element('main_preloader') ?>
    <?= $this->Flash->render('auth') ?>

    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="/app/inicio/index"><?php echo $this->Html->image('logo.png', ['alt' => '', 'width' => 240, 'class' => 'logo']);?></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a data-filter="1" class="nav-link js-scroll-trigger special-tab <?= ($current_controller == 'Inicio' && $current_action == 'buscarPublicaciones' && $tab == 1) ? 'active' : '' ?>" href="javascript:void(0)">Alquileres</a>
            </li>
            <li class="nav-item">
              <a data-filter="2" class="nav-link js-scroll-trigger special-tab <?= ($current_controller == 'Inicio' && $current_action == 'buscarPublicaciones' && $tab == 2) ? 'active' : '' ?>" href="javascript:void(0)">Ventas</a>
            </li>
            <li class="nav-item">
              <a data-filter="3" class="nav-link js-scroll-trigger special-tab <?= ($current_controller == 'Inicio' && $current_action == 'buscarPublicaciones' && $tab == 3) ? 'active' : '' ?>" href="javascript:void(0)">Comerciales</a>
            </li>
            <li class="nav-item">
              <a data-filter="4" class="nav-link js-scroll-trigger special-tab <?= ($current_controller == 'Inicio' && $current_action == 'buscarPublicaciones' && $tab == 4) ? 'active' : '' ?>" href="javascript:void(0)">Remates</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger <?= ($current_controller == 'Inicio' && $current_action == 'servicios') ? 'active' : '' ?>" href="/app/inicio/servicios">Servicios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger <?= ($current_controller == 'Inicio' && $current_action == 'nosotros') ? 'active' : '' ?>" href="/app/inicio/nosotros">Nosotros</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger <?= ($current_controller == 'Inicio' && $current_action == 'contactos') ? 'active' : '' ?>" href="/app/inicio/contactos">Contactos</a>
            </li>
            <li class="nav-item">
              <?php if (!empty($account) && $loggedIn && $this->request->session()->read('type_user') != 'admin'): ?>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle lbl-account-name" href="javascrip:void(0)" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="fa fa-user" aria-hidden="true"></span> <?= $account->name ?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="/app/accounts/edit">Perfil</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="/app/accounts/logout">Cerrar Sesión</a>
                    </div>
                  </li>
              <?php else: ?>
                  <a class="nav-link js-scroll-trigger lbl-login-register" href="/app/accounts/add"><i class="fa fa-user"></i> INGRESAR</a>
              <?php endif; ?>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <?= $this->fetch('content') ?>

    <!-- Footer -->
    <footer class="text-center">
      <div class="footer-above">
        <div class="container">
          <div class="row">
            <div class="footer-col col-md-4">
              <h3>Ubicación</h3>
              <p>Obligado 1401
                <br>3500 Resistencia, Chaco</p>
              <p>Teléfono
                <br><i class="fa fa-phone" aria-hidden="true"></i> +54 - 0362 4573000
                <br><i class="fa fa-whatsapp" aria-hidden="true"></i> 0362 154221616  
              </p>
            </div>
            <div class="footer-col col-md-4">
              <h3>Redes Sociales</h3>
              <ul class="list-inline">
                <li class="list-inline-item">
                  <a target="_blank" class="btn-social btn-outline" href="https://www.facebook.com/AragorIker.Propiedades/">
                    <i class="fa fa-fw fa-facebook"></i>
                  </a>
                </li>
              </ul>
            </div>
            <div class="footer-col col-md-4">
              <h3>Información</h3>
              <ul class="info-footer">
                <li><a class="lbl-info-footer" href="/app/inicio/servicios">Servicios</a></li>
    						<li><a class="lbl-info-footer" href="/app/inicio/nosotros">Nosotros</a></li>
    						<li><a class="lbl-info-footer" href="/app/inicio/contactos">Contactos</a></li>
    					</ul>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-below">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <p style="color: white;">&copy; <span id="lbl_current_year"></span> Todos los derechos reservados Aragor Iker
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top d-lg-none">
      <a class="btn btn-primary js-scroll-trigger" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 mx-auto">
                <div class="modal-body">
                  <h2>ALQUILERES</h2>
                  <hr class="star-primary">
                  <img class="img-fluid img-centered" src="" alt="">
                  <p style="text-align:left;">¡En Arágor Iker Propiedades trabajamos para que su comodidad y tranquilidad sean parte fundamental en su vida!</p>
                  <p style="text-align:left;">Por ello, cuando usted confía su propiedad en nosotros, nos encargamos de ofrecer su propiedad en Alquiler, verificando la situación crediticia y las posibilidades de cumplimiento del pago tanto del interesado como también del garante propuesto, buscando de esta manera la tranquilidad que <strong>¡Usted se Merece!!</strong></p>
                  <p style="text-align:left;">Su Propiedad será ofrecida de manera impetuosa tanto en nuestra ciudad como también en otras localidades y provincias. El ofrecimiento de realiza a través de distintas formas:</p>
                  <ul class="item-details" style="padding-left: 0; text-align: left; margin-left: 60px;">
                    <li>
                      Cartera de clientes
                    </li>
                    <li>
                      Público en general que visita nuestra oficina.
                    </li>
                    <li>
                      Internet
                    </li>
                    <li>
                      Redes sociales.
                    </li>
                    <li>
                      Diarios y otras formas publicitarias impresas.
                    </li>
                    <li>
                      Cartelería Pública
                    </li>
                    <li>
                      Mensajes de textos, WhatsApp, Etc.
                    </li>
                    <li>
                      Y siempre modernizándonos para llegar a más interesados.
                    </li>
                  </ul>
                  <p style="text-align: left;">¡Además! Nos encargamos de la administración a un bajo costo.</p>
                  <button class="btn btn-success" type="button" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                    Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 mx-auto">
                <div class="modal-body">
                  <h2>VENTAS</h2>
                  <hr class="star-primary">
                  <img class="img-fluid img-centered" src="" alt="">
                  <p style="text-align: left;">¡Dos de las principales razones para comprar o vender a través de una inmobiliaria es <strong>SEGURIDAD Y CONFIANZA!!</strong></p>
                  <p style="text-align: left">La negociación a través de Arágor Iker Propiedades brinda al comprador y vendedor  la seguridad de que al realizarse una operación se efectuará en un marco de legalidad y absoluta discreción otorgando una confianza sublime, permitiéndole descansar y compartir junto a su familia!</p>
                  <p style="text-align: left">Otros Beneficios que brindamos a nuestros clientes son:</p>
                  <ul class="item-details" style="padding-left: 0; text-align: left; margin-left: 60px;">
                    <li>
                      Continuo asesoramiento Profesional
                    </li>
                    <li>
                      Actualización de precios en caso de que existan variaciones de mercado
                    </li>
                    <li>
                      Asesoramiento Legal en cuanto a transacciones
                    </li>
                    <li>
                      Realizar financiaciones con garantías previamente investigadas y confiables
                    </li>
                    <li>
                      Resolver la venta en el menor tiempo posible.
                    </li>
                    <li>
                      Evitar desgastes en el proceso de negociación 
                    </li>
                    <li style="text-decoration: underline;">
                      Permitir solo la visita a su inmueble de personas realmente interesadas y con posibilidades económicas
                    </li>
                    <li>
                      Disfrutar de la tranquilidad y solo esperar los mejores resultados
                    </li>
                  </ul>
                  <button class="btn btn-success" type="button" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                    Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 mx-auto">
                <div class="modal-body">
                  <h2>TASACIONES</h2>
                  <hr class="star-primary">
                  <img class="img-fluid img-centered" src="" alt="">
                  <p style="text-align: left;">La tasación de inmuebles no es intuitiva , es un método de trabajo que requiere profundos conocimientos técnicos sin improvisaciones.</p>
                  <p style="text-align: left;">Tasar es poner el precio justo a un bien susceptible de valor económico en un tiempo determinado  en un mercado abierto y competitivo, permitiendo al comprador y el vendedor actuar con prudencia y en beneficio propio.</p>
                  <p style="text-align: left;"> “Por ello una Tasación realizada por Profesionales competentes, garantiza al cliente tener la certeza de cuál es el valor de su propiedad en el mercado actual según las condiciones del inmueble”, dejando de lado valores sugeridos sin fundamentos y documentación respaldatoria  provenientes de personas sin capacitación.</p>
                  <p style="text-align: left;">Para ello, en Arágor Iker Propiedades nos capacitamos y actualizamos continuamente, poniendo, además, nuestra experiencia a su disposición; y así, <strong>Usted. Logre la mejor inversión.</strong></p>
                  <button class="btn btn-success" type="button" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                    Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 mx-auto">
                <div class="modal-body">
                  <h2>ADMINISTRACIÓN DE PROPIEDADES</h2>
                  <hr class="star-primary">
                  <img class="img-fluid img-centered" src="" alt="">
                  <p style="text-align: left;">¡En Arágor Iker Propiedades trabajamos para que su comodidad y tranquilidad sean parte fundamental en su vida!</p>
                  <p style="text-align: left;">Para ello, nos encargamos de administrar su propiedad, evitándole a usted el humor de los inquilinos, sus consultas o  reclamos, filtrando cada charla con el inquilino y acercándoles solo las consultas o reclamos que le sean competente solucionarlos.</p>
                  <p style="text-align: left;">Además, al momento de poner su propiedad en alquiler, prorrogar o renovar los contratos, realizamos todas las gestiones pertinentes, investigando la situación crediticia y las posibilidades de cumplimiento de pago de cada interesado en alquilar y los garantes ofrecidos.</p>
                  <p style="text-align: left;">Todo ello, a un bajo Costo!!</p>
                  <button class="btn btn-success" type="button" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                    Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 mx-auto">
                <div class="modal-body">
                  <h2>CONSULTORÍAS</h2>
                  <hr class="star-primary">
                  <img class="img-fluid img-centered" src="" alt="">
                  <p style="text-align: left;"><strong>¡Nuestra vasta experiencia en el rubro inmobiliario nos permite resolver de forma inmediata cada una de las inquietudes que nos consulta nuestros clientes; tanto en el ámbito Legal como también en el ámbito económico!!</strong></p>
                  <button class="btn btn-success" type="button" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                    Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 mx-auto">
                <div class="modal-body">
                  <h2>ANÁLISIS, DESARROLLO Y COMERCIALIZACIÓN DE PROYECTOS DE INVERSIÓN; FIDEICOMISOS; URBANIZACIONES.</h2>
                  <hr class="star-primary">
                  <img class="img-fluid img-centered" src="" alt="">
                  <p style="text-align: left;">Arágor Iker Propiedades cuenta con un equipo tercerizado de alto perfil y rendimiento en el cual usted podrá confiar sus deseos de inversión, desarrollando un proyecto sólido y sustentable, logrando así, un alto rendimiento de ingresos económicos.</p>
                  <p style="text-align: left;">El equipo evalúa íntegramente cual es la mejor y más rentable inversión a desarrollar en cada zona o lugar de nuestras tierras, teniendo en cuenta:</p>
                  <ul class="item-details" style="padding-left: 0; text-align: left; margin-left: 60px;">
                    <li>
                      Factibilidad Técnica: Gestiones ante organismos y entidades regulatorias.
                    </li>
                    <li>
                      Factibilidad Comercial: investigación de las necesidades y requerimientos de los futuros adquirentes de cada unidad.
                    </li>
                    <li>
                      Factibilidad Financiera: Financiamiento y Proyección de rentabilidad.
                    </li>
                  </ul>
                  <button class="btn btn-success" type="button" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                    Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="portfolio-modal modal fade" id="portfolioModal7" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 mx-auto">
                <div class="modal-body">
                  <h2>FONDOS COMERCIALES</h2>
                  <hr class="star-primary">
                  <img class="img-fluid img-centered" src="" alt="">
                  <p style="text-align: left;">Un Fondo Comercial es una Inversión interesante, dado que ya tiene establecido una cartera de clientes otorgando una base de ventas periódicas.</p>
                  <p style="text-align: left;">En Arágor Iker Propiedades realizamos investigaciones previas al ofrecimiento de un Fondo Comercial, dando tranquilidad al adquirente de que dicho Fondo Comercial tiene solvencia y rentabilidad.</p>
                  <button class="btn btn-success" type="button" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                    Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="portfolio-modal modal fade" id="portfolioModal8" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 mx-auto">
                <div class="modal-body">
                  <h2>SERVICIOS CONTABLES</h2>
                  <hr class="star-primary">
                  <img class="img-fluid img-centered" src="" alt="">
                  <p style="text-align: left;">En Arágor Iker propiedades contamos, además, con el asesoramiento contable necesario para resguardar sus ingresos provenientes de alquileres, ventas, proyectos de inversión, entre otros... </p>
                  <p style="text-align: left;">Evitando, de esta forma, las incomodidades que pudieren generarse por el acatamiento o desconocimiento de las leyes estatales, resoluciones de AFIP, UIF,   ordenanzas municipales, u otros organismos públicos .</p>
                  <button class="btn btn-success" type="button" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                    Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?= $this->Html->script([
      'main/jquery/jquery.min',
      'main/bootstrap/js/bootstrap.bundle.min',
      'main/jquery-easing/jquery.easing.min',
      'main/jqBootstrapValidation',
      'main/contact_me',
      'main/freelancer',
      '/vendor/noty/lib/noty',
    ]) ?>

    <script type="text/javascript">

      var account;

      $(document).ready(function() {

        var current_year = new Date().getFullYear();
        $('#lbl_current_year').text(current_year);

        current_controller = "<?= $current_controller ?>";
        current_action = "<?= $current_action?>";

        $('.special-tab').click(function() {
          var filter = $(this).data('filter');
          var action = "/app/inicio/buscarPublicaciones";

          $('body')
            .append( $('<form/>').attr({'action': action, 'method': 'post', 'id': 'replacer'})
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'tab', 'value': filter}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'order', 'value': ''})))
            .find('#replacer').submit();
        });

      });
      
      window.fbAsyncInit = function() {
        FB.init({
            appId            : '2129739373939642',
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v3.0'
        });
    };

    (function(d, s, id) {
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "https://connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $('.facebook-icon').click(function() {

        var picture = $(this).data('picture');
        picture = picture.replace(/\ /g, '%20');

        FB.ui({
          method: 'share_open_graph',
          action_type: 'og.shares',
          display: 'popup',
          mobile_iframe: true,
          action_properties: JSON.stringify({
            object: {
              'og:url': $(this).data('url'),
              'og:title': $(this).data('title'),
              'og:description': $(this).data('description'),
              'og:image': picture
            }
          })
        }, function(response) {
          // Action after response
        });
    });
    
    
    
    </script>

    <?= $this->fetch('script') ?>

    <?= $this->Flash->render() ?>

  </body>
</html>
