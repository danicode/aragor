<style type="text/css">

    .lbl-title-search {
        font-size: larger;
        font-family: 'Work Sans', sans-serif;
        color: white;
    }

    .search-form-container {
        background: #444242;
        margin-top: 15px;
        margin-bottom: 15px;
        border-radius: 8px;
        box-shadow: 2px 4px 5px #999;
        padding-bottom: 30px;
    }

    .search-form {
        margin-top: 15px;
        margin-bottom: 0px;
        color: white;
    }

    .lbl-range-price {
        color: white;
        font-family: 'Work Sans', sans-serif;
    }

    .post-card {
        max-width: 326px; 
        box-shadow: 1px 1px 7px #999;
        text-align: left;
    }

    .post-properties {
        list-style: none;
        display: inline;
        position: relative;
        margin-top: 0;
        padding-top: 0;
    }

    .lbl-money {
        font-weight: bolder;
        font-size: x-large;
        font-family: 'Work Sans', sans-serif;
    }

    .post-body {
        background: #fff;
        font-family: 'Work Sans', sans-serif;
        color: black;
        padding-top: 0;
        padding-bottom: 0;
    }

    .textarea-message-height{
        height: 114px;
    }
 
    .post-title {
        margin-bottom: 0;
        color: #666666;
        margin-right: 10px;
    }

    .title-container {
        padding: 10px;
        position: relative;
        display: inline;
    }

    #mainNav {
        padding-top: 0;
        padding-bottom: 0;
    }

    .mt-38 {
        margin-top: 38px;
    }

    .mt-20 {
        margin-top: 20px;
    }

    #btn-add_favorite-list {
        outline: none;
        text-decoration: none !important;
        display: block;
        text-align: center;
        width: 100%;
        color: #FF5722;  
        font-family: 'Work Sans', sans-serif;
        font-size: 15px;
        border: 0;
        font-weight: bold;
    }

    #btn-add_favorite-list:hover {
        color: #18BC9C;
        outline: none;
    }

    .facebook-icon {
        color: #4267b2;
    }

    .whatsapp-icon {
         color: rgb(30, 190, 165);
    }

    .btn-send {
        cursor: pointer;
    }

    .btn-search {
        cursor: pointer;
    }

    .lbl-subtitle {
        font-weight: bold;
    }

    .filter-form {
        background: #000;
        margin-top: 71px;
        padding-top: 1px;        
    }

    .carousel-inner {
        height: 100%;
        width: auto;
    }

    .caroussel-img {
        height: auto;
        background: #000;
        width: 100%;
    }    

    .img-public {
        width: auto;
        height: 630px;
    }

    @media (max-width: 1200px) {
        .img-public {
           height: 530px !important;
        }
    }

    @media (max-width: 992px) {
        .img-public {
           height: 390px !important;
        }
    }

    @media (max-width: 768px) {
        .img-public {
           height: 390px !important;
        }
    }

    @media (max-width: 576px) {
        .img-public {
           height: 250px !important;
        }
        
        .filter-form {
            background: #000;
            margin-top: 108px;
            padding-top: 1px;
        }
    }

    @media (min-width: 768px) {

        /* show 3 items */
        .carousel-inner-related .active,
        .carousel-inner-related .active + .carousel-item-related,
        .carousel-inner-related .active + .carousel-item-related + .carousel-item-related,
        .carousel-inner-related .active + .carousel-item-related + .carousel-item-related + .carousel-item-related  {
            display: block;
        }

        .carousel-inner-related .carousel-item-related.active:not(.carousel-item-related-right):not(.carousel-item-related-left),
        .carousel-inner-related .carousel-item-related.active:not(.carousel-item-related-right):not(.carousel-item-related-left) + .carousel-item-related,
        .carousel-inner-related .carousel-item-related.active:not(.carousel-item-related-right):not(.carousel-item-related-left) + .carousel-item-related + .carousel-item-related,
        .carousel-inner-related .carousel-item-related.active:not(.carousel-item-related-right):not(.carousel-item-related-left) + .carousel-item-related + .carousel-item-related + .carousel-item-related {
            transition: none;
        }

        .carousel-inner-related .carousel-item-related-next,
        .carousel-inner-related .carousel-item-related-prev {
          position: relative;
          transform: translate3d(0, 0, 0);
        }

        .carousel-inner-related .active.carousel-item-related + .carousel-item-related + .carousel-item-related + .carousel-item-related + .carousel-item-related {
            position: absolute;
            top: 0;
            right: -25%;
            z-index: -1;
            display: block;
            visibility: visible;
        }

        /* left or forward direction */
        .active.carousel-item-related-left + .carousel-item-related-next.carousel-item-related-left,
        .carousel-item-related-next.carousel-item-related-left + .carousel-item-related,
        .carousel-item-related-next.carousel-item-related-left + .carousel-item-related + .carousel-item-related,
        .carousel-item-related-next.carousel-item-related-left + .carousel-item-related + .carousel-item-related + .carousel-item-related,
        .carousel-item-related-next.carousel-item-related-left + .carousel-item-related + .carousel-item-related + .carousel-item-related + .carousel-item-related {
            position: relative;
            transform: translate3d(-100%, 0, 0);
            visibility: visible;
        }

        /* farthest right hidden item must be abso position for animations */
        .carousel-inner-related .carousel-item-related-prev.carousel-item-related-right {
            position: absolute;
            top: 0;
            left: 0;
            z-index: -1;
            display: block;
            visibility: visible;
        }

        /* right or prev direction */
        .active.carousel-item-related-right + .carousel-item-related-prev.carousel-item-related-right,
        .carousel-item-related-prev.carousel-item-related-right + .carousel-item-related,
        .carousel-item-related-prev.carousel-item-related-right + .carousel-item-related + .carousel-item-related,
        .carousel-item-related-prev.carousel-item-related-right + .carousel-item-related + .carousel-item-related + .carousel-item-related,
        .carousel-item-related-prev.carousel-item-related-right + .carousel-item-related + .carousel-item-related + .carousel-item-related + .carousel-item-related {
            position: relative;
            transform: translate3d(100%, 0, 0);
            visibility: visible;
            display: block;
            visibility: visible;
        }

    }

    .related-post-box {
        border: 1px solid #d6d3d3;
        margin-bottom: 20px;
        padding: 20px;
    }

    .related-image {
        width: auto;
        height: 185px;
    }
    
     #map {
		height: 370px;
	}

	#floating-panel {
		top: 10px;
		left: 25%;
		z-index: 5;
		background-color: #fff;
		padding: 5px;
		border: 1px solid #999;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
		padding-left: 10px;
	}

	#submit-map {
        padding: 10px;
        top: 2px;
    }

    #update-map {
        padding: 10px;
        top: 2px;
    }

</style>

<header class="filter-form">
  <div class="container">
    <div class="search-form">
        <?= $this->Form->create(null, [
              'url' => ['controller' => 'inicio', 'action' => 'buscarPublicaciones']
        ]) ?>
        <div class="row">

          <div class="col-sm-3">
            <?= $this->Form->input('code', ['label' => false, 'type' => 'text', 'placeholder' => 'Código o referencia']) ?>
          </div>

          <div class="col-sm-2">
              <?= $this->Form->input('operation', ['label' => false, 'options' => $_SESSION['paraments']->post->operation]) ?>
          </div>

          <div class="col-sm-3">
            <div class="row">

              <div class="col-sm-6">
                <?= $this->Form->input('bath', ['label' => false, 'options' => $_SESSION['paraments']->post->bath]) ?>
              </div>

              <div class="col-sm-6">
                <?= $this->Form->input('garage', ['label' => false, 'options' => $_SESSION['paraments']->post->garage]) ?>
              </div>

            </div>

          </div>

          <div class="col-sm-2">
              <?= $this->Form->input('type_property', ['label' => false, 'options' => $_SESSION['paraments']->post->property]) ?>
          </div>

          <div class="col-sm-2">
              <?= $this->Form->input('zone', ['label' => false, 'options' => $_SESSION['paraments']->post->zone]) ?>
          </div>

          <div class="col-sm-2">
            <?= $this->Form->input('min_price', ['label' => false, 'type' => 'number', 'min' => '0', 'placeholder' => 'Precio Mínimo']) ?>
          </div>

          <div class="col-sm-2">
            <?= $this->Form->input('max_price', ['label' => false, 'type' => 'number', 'min' => '0', 'placeholder' => 'Precio Máximo']) ?>
          </div>

          <div class="col-sm-2">
              <?= $this->Form->input('room', ['label' => false, 'options' => $_SESSION['paraments']->post->room]) ?>
          </div>

          <div class="col-sm-2">
              <?= $this->Form->input('environment', ['label' => false, 'options' => $_SESSION['paraments']->post->environment]) ?>
          </div>

          <div class="col-sm-2">
            <?= $this->Form->input('money', ['label' => false, 'options' => $_SESSION['paraments']->post->money, 'value' => 'ARS']) ?>
          </div>

          <div class="col-sm-2">
            <button type="submit" class="form-control btn btn-primary btn-search" aria-label="Left Align">
              <span class="fa fa-search" aria-hidden="true"></span> Buscar
            </button>
          </div>

        </div>
        <?= $this->Form->end() ?>
      </div>
  </div>
</header>

<div class="container">
  <div class="row">
    <div class="col-sm-9 mb-3">

        <div id="post-container" style="background: #fff; box-shadow: 2px 4px 5px #999; margin-top: 15px;">
            <div id="carouselExampleIndicators" class="carousel slide caroussel-img" data-ride="carousel">
              <ol class="carousel-indicators">
                <?php if (!empty($post->images)): ?>
                    <?php foreach ($post->images as $key => $image): ?>
                        <li data-target="#carouselExampleIndicators" data-slide-to="<?= $key ?>" class="<?= $key == 0 ? 'active' : '' ?>"></li>
                    <?php endforeach; ?>
                <?php else: ?>
                   <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <?php endif; ?>
              </ol>
              <div class="carousel-inner">
                <?php if (!empty($post->images)): ?>
                    <?php foreach ($post->images as $key => $image): ?>
                        <div class="carousel-item <?= $key == 0 ? 'active' : '' ?>">
                            <img class="d-block img-public" style="margin-left: auto; margin-right: auto;" src="/app/images/<?= $image->url ?>" alt="">
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="carousel-item active">
                        <img class="d-block img-public" style="margin-left: auto; margin-right: auto;" src="/app/images/noimage.png" alt="Sin Imagen">
                    </div>
                <?php endif; ?>
              </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Siguiente</span>
              </a>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="title-container">
                                <label style="font-family: 'Work Sans', sans-serif;" class="post-title">REFERENCIA</label>
                                <h5 style="margin-left: 10px;">#<?= $post->code ?></h5>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="title-container">
                                <i data-id="<?= $post->id ?>" title="Exportar PDF" class="fa fa-print fa-2x" id="btn-export-pdf" style="cursor: pointer; margin-top: 5px"  aria-hidden="true"></i>
                            </div>
                            <?php
                              $flag = true;
                              if (sizeof($post->images) > 0) {
                                foreach ($post->images as $img) {
                                  if ($img->main) {
                                    $flag = false;
                                    $image = $img->url;
                                  }
                                }
                                if ($flag) {
                                  $image = $post->images[0]->url;
                                }
                              } else {
                                $image = "noimage.png";
                              }
                            ?>
                            <a href="javavascript:void(0)" data-title="<?= $post->title ?>" data-description="<?= $post->title ?>" data-picture="<?= $this->Url->build('/images/' . $image , true) ?>" data-url="<?= $this->Url->build('/inicio/publicaciones/' . $post->id , true) ?>" class="fa fa-facebook-square fa-2x facebook-icon"></a>
                            <?php 
                            if ($mobile) {
                              echo $this->SocialShare->fa(
                                'whatsapp_mobile',
                                $this->Url->build('/inicio/publicaciones/' . $post->id , true),
                                ['icon_class' => 'fa fa-whatsapp fa-2x ml-2 whatsapp-icon', 'text' => $post->title]
                              );
                            } else {
                              echo $this->SocialShare->fa(
                                'whatsapp',
                                $this->Url->build('/inicio/publicaciones/' . $post->id , true),
                                ['icon_class' => 'fa fa-whatsapp fa-2x ml-2 whatsapp-icon', 'text' => $post->title]
                              );
                            }
                          ?>
                        </div>

                        <div class="col-sm-4 text-right">
                            <div class="title-container">
                                <?php
                                    $money = $post->money;
                                ?>
                                <?php if (!empty($post->price)): ?>
                                    <label class="post-title" style="font-family: -webkit-pictograph;">PRECIO</label>
                                    <h5 style="margin-right: 10px;"><?= $_SESSION['paraments']->post->money->$money ?> <?= number_format($post->price, 2, $dec_point = ",", $thousands_sep = "." ) ?></h5>
                                <?php else: ?>
                                    <button id="btn-consultar" class="btn btn-price mt-1">Consultar Precio</button>
                                <?php endif; ?>
                            </div>
                        </div>
                     </div>

                </div>

                <div class="col-sm-12" style="padding-left: 25px;">
                    <?php
                        $status_select = 8;
                        $status = $post->status;
                        if ($_SESSION['paraments']->post->status->$status->name != $_SESSION['paraments']->post->status->$status_select->name) {
                            $status = ' - ' . $_SESSION['paraments']->post->status->$status->name;
                        } else {
                            $status = "";
                        }
                    ?>
                    <p style="font-size: 18px; font-family: 'Work Sans', sans-serif; font-weight: bold;">
                        <?= $post->title . $status ?>
                    </p>
                </div>

                <div class="col-sm-12" style="padding-left: 25px; padding-bottom: 15px;">
                    <?php
                        $flag_add_favorite = false;
                        if ($loggedIn) {
                            if (!empty($account->favorites)) {
                                foreach ($account->favorites as $favorite) {
                                    if ($favorite->post_id == $post->id) {
                                        $flag_add_favorite = true; 
                                    }
                                }
                            }
                        }
                    ?>
                    <a data-id = "<?= $post->id ?>" class="text-left" id="btn-add_favorite-list" data-add = "<?= $flag_add_favorite ?>" href="javascript:void(0)"><i class="<?= $flag_add_favorite ? 'fa fa-heart' : 'fa fa-heart-o' ?>" aria-hidden="true"></i> <?= $flag_add_favorite ? 'Añadido lista de Favoritos' : 'Añadir lista de Favoritos' ?></a>
                </div>

                <div class="col-sm-12" style="padding-left: 25px;">
                    <label style="font-family: 'Work Sans', sans-serif; font-weight: bold;">DESCRIPCIÓN</label>
                    <p style="font-size: 16px; margin-right: 10px;"><?= $post->description ?></p>
                </div>

                <div class="col-sm-12" style="padding-left: 25px;">
                    <label style="font-family: 'Work Sans', sans-serif; font-weight: bold;">CARACTERISTICAS</label>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Baño</label>
                        </div>
                        <div class="col-sm-6">
                            <?php
                                $bath = $post->bath;
                            ?>
                            <label><?= $post->bath > 5 ? $_SESSION['paraments']->post->bath->$bath : $bath ?></label>
                        </div>
                        <?php if (!empty($post->room)): ?>
                            <div class="col-sm-6">
                                <label>Domirtorios</label>
                            </div>
                            <div class="col-sm-6">
                                <?php
                                    $room = $post->room;
                                ?>
                                <label><?= $post->room > 10 ? $_SESSION['paraments']->post->room->$room : $room ?></label>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($post->environment)): ?>
                            <div class="col-sm-6">
                                <label>Ambientes</label>
                            </div>
                            <div class="col-sm-6">
                                <?php
                                    $environment = $post->environment;
                                ?>
                                <label><?= $post->environment > 10 ? $_SESSION['paraments']->post->environment->$environment : $environment ?></label>
                            </div>
                        <?php endif; ?>
                        <div class="col-sm-6">
                            <label>Cochera</label>
                        </div>
                        <div class="col-sm-6">
                            <?php
                                $garage = $post->garage;
                            ?>
                            <label><?= $post->garage > 3 ? $_SESSION['paraments']->post->garage->$garage : $garage ?></label>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12" style="padding-left: 25px;">
                    <label style="font-family: 'Work Sans', sans-serif; font-weight: bold;">UBICACIÓN</label>
                    <?php
                        $zone = $post->zone;
                        $zone_value = "";
                        if (!empty($zone)) {
                          $zone_value = " - " . $_SESSION['paraments']->post->zone->$zone;
                        } else {
                          $zone_value = "";
                        }
                    ?>
                    <span style="display: block;"><?= $post->address; ?> <?= $zone_value ?></span>

                    <div id="map" class="mb-3"></div>
                </div>

            </div>
        </div>

    </div>
    <div class="search-form-container col-sm-3">
      <div class="search-form">
        <label class="lbl-title-search">CONSULTAR PROPIEDAD</label>
        <?= $this->Form->create(null, [
            'url' => ['controller' => 'inicio', 'action' => 'consulta']
        ]) ?>
        <div class="row">

            <div class="col-sm-12">
                <div class="form-group select required">
                    <select name="email_contact" id="email_contact" class="form-control" required="required">
                        <option value="">Seleccionar Contacto</option>
                        <?php if (count($emails) > 0): ?>
                            <?php foreach ($emails as $key => $email): ?>
                                <option value="<?= $key ?>"><?= $email ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-12">
                <?php
                    $name = "";
                    $phone = "";
                    $email = "";
                    if ($loggedIn && $this->request->session()->read('type_user') != 'admin') {
                        $name = $account->name;
                        $phone = $account->phone ? $account->phone : "";
                        $email = $account->email;
                    }
                    echo $this->Form->input('name', ['type' => 'text', 'label' => 'Nombre', 'required' => true, 'value' => $name]);
                    echo $this->Form->input('post_id', ['type' => 'hidden', 'value' => $post->id]);
                ?>
            </div>

            <div class="col-sm-12">
                <?php
                    echo $this->Form->input('phone', ['type' => 'text', 'label' => 'Teléfono', 'value' => $phone]);
                ?>
            </div>

            <div class="col-sm-12">
                <?php
                    echo $this->Form->input('email', ['type' => 'text', 'label' => 'Correo', 'required' => true, 'value' => $email]);
                ?>
            </div>

            <div class="col-sm-12">
                <?php
                    echo $this->Form->input('message', ['type' => 'textarea', 'label' => 'Mensaje', 'required' => true]);
                ?>
            </div>

            <div class="col-sm-12 mt-3">
                <button type="submit" class="form-control btn btn-secondary btn-send" aria-label="Left Align">
                    <span class="fa fa-paper-plane" aria-hidden="true"></span> Enviar
                </button>
            </div>

        </div>
        <?= $this->Form->end() ?>
      </div>
      <?php if ($banners_lateral): ?>
        <?= $this->element('banners_lateral') ?>
      <?php endif; ?>
      
    </div>

    <?php if (!empty($related_post) && $related_post->count() > 0): ?>
        <div class="col-md-12 related-post-box">
            <div class="container-fluid">
                <h3 class="text-center" style="font-family: 'Work Sans', sans-serif; font-weight: bold; color: #4e5151;">Publicaciones Relacionadas</h3>
                <div id="carouselExample" class="carousel slide" data-ride="carousel" data-interval="9000">
                    <div class="carousel-inner carousel-inner-related row w-100 mx-auto" role="listbox">
                        <?php foreach ($related_post as $key => $rel_post): ?>
                            <div class="carousel-item carousel-item-related col-md-3 <?= $key == 0 ? 'active' : '' ?>" style="color: black;">
                                <?php
                                    $image = "noimage.png";
                                    if (!empty($rel_post->images)) {
                                        $image = $rel_post->images[0]->url;
                                    }
                                ?>
                                <div style="position: relative; display: block; background: #000;">
                                    <a href="/app/inicio/publicaciones/<?= $rel_post->id ?>"><img class="img-fluid mx-auto d-block related-image" src="/app/images/<?= $image ?>" title="<?= $rel_post->title ?>" alt=""></a>
                                </div>
                                <div style="padding: 10px; border: 1px solid #999; height: 130px; word-wrap: break-word; overflow-x: hidden;">
                                    <p style="font-size: 14px; font-weight: bold;"><?= $rel_post->title ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <a class="carousel-control-prev" style="color: #FF5722;" href="#carouselExample" role="button" data-slide="prev">
                        <i class="fa fa-chevron-left fa-lg"></i>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" style="color: #FF5722;" href="#carouselExample" role="button" data-slide="next">
                        <i class="fa fa-chevron-right fa-lg"></i>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if ($banner): ?>
        <?= $this->element('banners') ?>
    <?php endif; ?>
  </div>
</div>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZG7dni5-GuQaooMmy6fLtYf3B_kHtAOY">
</script>

<script type="text/javascript">

    var loggingIn = <?= json_encode($loggedIn) ?>;
    var type_user = <?= !empty($_SESSION['type_user']) ? json_encode($_SESSION['type_user']) : "[]" ?>;
    if (type_user == "[]") {
      type_user = false;
    }
    var post = null;

    $(document).ready(function () {
        post = <?= json_encode($post); ?>;

        initMap();

        $('#carouselExample').on('slide.bs.carousel', function (e) {

            var $e = $(e.relatedTarget);
            var idx = $e.index();
            var itemsPerSlide = 4;
            var totalItems = $('.carousel-item-related').length;
            
            if (idx >= totalItems-(itemsPerSlide - 1)) {
                var it = itemsPerSlide - (totalItems - idx);
                for (var i = 0; i < it; i++) {
                    // append slides to end
                    if (e.direction == "left") {
                        $('.carousel-item-related').eq(i).appendTo('.carousel-inner-related');
                    }
                    else {
                        $('.carousel-item-related').eq(0).appendTo('.carousel-inner-related');
                    }
                }
            }
        });

        $("#map").width($("#post-container").width() - 19);

        $('#btn-export-pdf').click(function() {
            var id = $(this).data('id');
            if (typeof id === 'undefined') {
                generateNoty('warning', "No es posible generar PDF de la Publicación.");
            } else {
                var action = "/app/inicio/post_export_p_d_f/" + id;
                window.open(action, '_blank');
            }
        });

        $('#btn-add_favorite-list').click(function() {
            var add = $(this).data('add');
            var id = $(this).data('id');

            if (loggingIn && type_user != 'admin') {
                if (add) {
                generateNoty('warning', 'La Publicación ya se encuentra en su lista de Favoritos.');
                } else {
                    var action = "/app/accounts/agregarFavorito/" + id;
                    $('body')
                      .append( $('<form/>').attr({'action': action, 'method': 'post', 'id': 'replacer'})
                      .append( $('<input/>').attr( {'type': 'hidden', 'name': 'action', 'value': 'publicaciones'}))
                      .append( $('<input/>').attr( {'type': 'hidden', 'name': 'post_id', 'value': id})))
                      .find('#replacer').submit();
                }
            } else {
                generateNoty('warning', 'Para hacer uso de la lista de Favoritos, debe contar con una cuenta o ingresar a su cuenta en caso de tener una.');
            }
        });

        $('#btn-consultar').click(function() {
            $('select[name^="email_contact"]').eq("").focus();
            generateNoty('success', 'Para consultar precio debe completar el formulario y enviarlo.');
        });
    });

    $('#update-map').click(function() {
        initMap();
    });

    $('form input').on('keypress', function(e) {
        return e.which !== 13;
    });

    var map = null;
    var geocoder = null;
    var marker = null;

    function isEmpty(val) {
        return (val === undefined || val == null || val == 0 || val.length <= 0) ? true : false;
    }

	function initMap() {

		geocoder = new google.maps.Geocoder;

		var myLatlng = {lat: -27.457300000000000039790393202565610408782958984375, lng: -58.99560000000000314912540488876402378082275390625};

		var mapOptions = {
			zoom: 14,
			center: myLatlng,
			mapTypeId: 'hybrid'   // roadmap, satellite, hybrid, terrain 
		};

		map = new google.maps.Map(document.getElementById('map'), mapOptions);

	    var init = myLatlng;

        var haveMarker = false;
        if (!isEmpty(post.lat) || !isEmpty(post.lng)) {
            haveMarker = true;
            init.lat = post.lat;
            init.lng = post.lng;
        }

        var latLng = new google.maps.LatLng(init.lat, init.lng);
        if (haveMarker) {
           addMarker(latLng);
        }

        map.setCenter(latLng);

		map.addListener('click', function(event) {
			addMarker(event.latLng);	    
		});
    }

    function refreshMap() {
        google.maps.event.trigger(map, 'resize');
    }

    $('input#addressmap').keypress(function(event) {
        if ( event.which == 13 ) {
            geocodeAddress(geocoder, map);
        }
    });

	function addMarker(location) {

		if (marker != null) {
			clearMarkers();
		}

		marker = new google.maps.Marker({
			position: location,
			animation: google.maps.Animation.DROP,
			map: map
		});
		marker.addListener('click', toggleBounce);
		marker.setMap(map);

        var lat = location.lat();
        var lng = location.lng();

		$('#coodmapslat').val(lat);
		$('#coodmapslng').val(lng);	
	}

	function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

	function clearMarkers() {
		marker.setMap(null);
	}			

	function geocodeAddress(geocoder, map) {

		var address = document.getElementById('addressmap').value;

		geocoder.geocode({
			'address': address
			}, function(results, status) {

				if (status === google.maps.GeocoderStatus.OK) {	
					addMarker(results[0].geometry.location)  	    
					map.setCenter(results[0].geometry.location);
					map.setZoom(14);

    			} else {
    				if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
    			        bootbox.alert('No se encontraron Resultados');
    			    } else {
    			        window.alert('Geocode was not successful for the following reason: ' + status);
    			    }
    			}
		});
	}

</script>
