<!-- About Section -->
<section class="success" id="about">
  <div class="container">
    <h2 class="text-center">Nosotros</h2>
    <hr class="star-light">
    <div class="row">
      <div class="col-lg-4 ml-auto">
        <p>Somos una inmobiliaria Joven pero con amplia experiencia en el mercado Inmobiliario, permitiéndonos asesorar a nuestros clientes con eficacia, brindándoles la tranquilidad, confianza y satisfacción de que su operación inmobiliaria se realizó con Total Seriedad, Transparencia, Honestidad y Absoluta Discreción</p>
      </div>
      <div class="col-lg-4 mr-auto">
        <p>Nuestro esfuerzo, dedicación, e idoneidad, nos permite realizar Administraciones, Compras, Ventas, y Tasaciones de Inmuebles; como así también realizar Desarrollos y comercialización de Proyectos de Inversión, con total profesionalismo. </p>
      </div>
      <div class="col-lg-10 ml-auto">
        <p>Los Esperamos!!!</p>
      </div>
    </div>
  </div>
</section>
