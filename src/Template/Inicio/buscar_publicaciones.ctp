<style type="text/css">

  .lbl-title-search {
    font-size: larger;
    font-family: 'Work Sans', sans-serif;
    color: white;
  }

  .search-form-container {
    background: #444242;
    margin-top: 15px;
    margin-bottom: 15px;
    border-radius: 8px;
  }

  .search-form {
    margin-top: 15px;
    margin-bottom: 20px;
  }

  .lbl-range-price {
    color: white;
    font-family: 'Work Sans', sans-serif;
  }

  .post-card {
    height: 540px;
    text-align: left;
    cursor: pointer;
  }

  .post-properties {
    list-style: none;
    display: inline;
    position: relative;
    margin-top: 0;
    padding-top: 0;
  }

  .lbl-money {
    font-weight: bolder;
    font-size: 30px;
    font-family: 'Work Sans', sans-serif;
  }

  .post-body {
    background: #fff;
    font-family: 'Work Sans', sans-serif;
    color: black;
    padding-top: 0;
    padding-bottom: 0;
    display: block;
    position: relative;
    height: 60px;
  }

  .symbol-price {
    font-size: 20px;
  }

  figcaption {
    position: absolute;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 100%;
    padding: 7px;
    top: 0;
    color: #FFF;
    background: #000000;
    background: rgba(0, 0, 0, .6);
    text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.5);
    font-family: 'Work Sans', sans-serif;
  }

  .figure-img {
    margin-bottom: 0;
  }

  .cinta {
    background-image: url(/app/images/logo_status.png);
    background-repeat: no-repeat;
    background-size: auto 65px;
    height: 76px;
    width: 28px;
    position: relative;
    float: right;
    display: block;
    bottom: 80px;
  }

  .lbl-cinta {
    position: relative;
    font-size: 13px;
    color: white;
    margin-left: 10px;
    vertical-align: middle;
    font-family: 'Work Sans', sans-serif;
  }
  
  .status-container {
    position: absolute;
    display: block;
    background: #FF5722;
    height: 20px;
    z-index: 1;
    width: 100%;
    bottom: 0px;
  }

  .caroussel-img {
    height: 100%;
    background: #000;
  }

  .caroussel-container {
    position: relative; 
    display: block; 
    height: 40%;
  }

  .caroussel-item {
    height: 100%;
    font-size: 14px; 
    font-weight: bold; 
    font-family: 'Work Sans', sans-serif;
    width: 100%;
    text-transform: uppercase;
    margin-top: 10px;
  }

  .lbl-title-post {
    font-size: 14px; 
    font-weight: bold; 
    font-family: 'Work Sans', sans-serif;
    height: 77px;
    text-transform: uppercase;
    margin-top: 10px;
  }
  
  .btn-add_favorite-list {
    outline: none;
    text-decoration: none !important;
    display: block;
    text-align: center;
    width: 100%;
    color: #FF5722;  
    font-family: 'Work Sans', sans-serif;
    font-size: 15px;
    border: 0;
    font-weight: bold;
  }

  .facebook-icon {
    color: #4267b2;
  }

  .whatsapp-icon {
     color: rgb(30, 190, 165);
  }

  .btn-search {
    cursor: pointer;
  }

  .content-post {
    height: 262px;
    overflow-x: hidden;
  }

  @media (max-width: 1198px) {
    .content-post {
      width: 100%;
    }
  }

  @media (max-width: 990px) {
    .lbl-title-post {
      width: 100%;
    }
  }

  @media (max-width: 767px) {
    .lbl-title-post {
      height: 77px;
    }

    .content-post {
      height: 262px;
    
    }
  }

  .select {
    margin-bottom: .5rem!important;
  }

  .number {
    margin-bottom: .5rem!important;
  }

  .text {
    margin-bottom: .5rem!important;
  }

  .lbl-no-post {
    color: brown; 
    font-family: 'Work Sans', sans-serif; 
    margin-top: 15px; 
    text-shadow: 1px 1px 1px #444;
  }

  .lbl-environment {
    display: block;
  }

  carousel-inner {
    height: 100%;
    width: auto;
  }

  .carousel-item-cover {
    height: 348px;
  }

</style>

<!-- Header -->
<header class="masthead">
  
  <div class="caroussel-container">

        <div id="carouselExampleIndicators" class="carousel slide caroussel-img" data-ride="carousel">
          <ol class="carousel-indicators">
            <?php if (!empty($assets)): ?>
                <?php foreach ($assets as $key => $asset): ?>
                    <li data-target="#carouselExampleIndicators" data-slide-to="<?= $key ?>" class="<?= $key == 0 ? 'active' : '' ?>"></li>
                <?php endforeach; ?>
            <?php else: ?>
               <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <?php endif; ?>
          </ol>
          <div class="carousel-inner">
            <?php if (!empty($assets)): ?>
                <?php foreach ($assets as $key => $asset): ?>
                    <div data-link="<?= $asset->link ?>" class="carousel-item carousel-item-cover cover-item <?= $key == 0 ? 'active' : '' ?> ">
                        <div class="masthead" style="background-image: url('/app/images/<?= $asset->url ?>');"> 
                           <?php if ($asset->main): ?>
                             <div class="container">
                                  <div class="intro-text">
                                      <span class="name">Aragor Iker</span>
                                          <hr class="star-light">
                                      <span class="skills">Agencia inmobiliaria en Resistencia, Argentina</span>
                                  </div>
                              </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="carousel-item carousel-item-cover active">
                    <img style="width: 100%; height: 100%;" class="" src="/app/images/noimage.png" alt="Sin Imagen">
                </div>
            <?php endif; ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Siguiente</span>
          </a>
        </div>

      </div>

</header>

<div class="container">

    <div class="row">
      <div class="search-form-container col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <div class="search-form">
          <?= $this->Form->create(null, [
              'url' => ['controller' => 'inicio', 'action' => 'buscarPublicaciones']
          ]) ?>
          <label class="lbl-title-search">BUSCAR PROPIEDADES</label>
          <div class="row">

            <div class="col-sm-12">
                <?= $this->Form->input('operation', ['label' => false, 'options' => $_SESSION['paraments']->post->operation]) ?>
            </div>

            <div class="col-sm-12">
                <?= $this->Form->input('type_property', ['label' => false, 'options' => $_SESSION['paraments']->post->property]) ?>
            </div>

            <div class="col-sm-12">
                <?= $this->Form->input('zone', ['label' => false, 'options' => $_SESSION['paraments']->post->zone]) ?>
            </div>

            <div class="col-sm-12">
                <?= $this->Form->input('room', ['label' => false, 'options' => $_SESSION['paraments']->post->room]) ?>
            </div>

            <div class="col-sm-12">
                <?= $this->Form->input('environment', ['label' => false, 'options' => $_SESSION['paraments']->post->environment]) ?>
            </div>

            <div class="col-sm-12">
              <div class="row">

                <div class="col-sm-6">
                  <?= $this->Form->input('bath', ['label' => false, 'options' => $_SESSION['paraments']->post->bath]) ?>
                </div>

                <div class="col-sm-6">
                  <?= $this->Form->input('garage', ['label' => false, 'options' => $_SESSION['paraments']->post->garage]) ?>
                </div>

              </div>

            </div>

            <div class="col-sm-12">
              <?= $this->Form->input('money', ['label' => false, 'options' => $_SESSION['paraments']->post->money]) ?>
            </div>

            <div class="col-sm-12">
              <label class="lbl-range-price">Rango de Precios</label>
              <div class="row">
                <div class="col-sm-6">
                  <?= $this->Form->input('min_price', ['label' => false, 'type' => 'number', 'min' => '0', 'placeholder' => 'Mínimo']) ?>
                </div>
                <div class="col-sm-6">
                  <?= $this->Form->input('max_price', ['label' => false, 'type' => 'number', 'min' => '0', 'placeholder' => 'Máximo']) ?>
                </div>
              </div>

            </div>

            <div class="col-sm-12">
              <?= $this->Form->input('code', ['label' => false, 'type' => 'text', 'placeholder' => 'Código o referencia']) ?>
            </div>

            <div class="col-sm-12">
              <?= $this->Form->button(' Buscar', ['type' => 'submit', 'class' => 'btn btn-secondary btn-block fa fa-search btn-search', 'arial-label' => 'Left Align']) ?>
            </div>

          </div>
          <?= $this->Form->end() ?>
        </div>
        <?php if ($banners_lateral): ?>
          <?= $this->element('banners_lateral') ?>
        <?php endif; ?>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 mb-3">
        <div>
          <div class="row">
            <?php if (!empty($posts) && $posts->count() > 0): ?>
              <?php foreach ($posts as $post): ?>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-4 mt-3">
                  <div data-id = "<?= $post->id ?>" class="card post-card">

                    <?php
                      $image = "noimage.png";
                      if (!empty($post->images)) {
                        foreach ($post->images as $img) {
                          if ($img->main) {
                            $image = $img->url;
                          }
                        }
                        if ($image == "noimage.png") {
                          $image = $post->images[0]->url;
                        }
                      }
                      $status = $post->status;
                    ?>

                    <div class="caroussel-container">
                      <?php if ($image == 'noimage.png'): ?>
                        <div data-id = "<?= $post->id ?>" class="post-target">
                          <img class="card-img-top figure-img img-fluid rounded" style="height: 216px; display: block; margin-left: auto; margin-right: auto;" src="/app/images/<?= $image ?>" alt="">
                        </div>
                      <?php else: ?>
                        <div id="carouselExampleIndicators<?= $post->id ?>" class="carousel slide caroussel-img" data-ride="carousel">
                          <ol class="carousel-indicators">
                            <?php if (!empty($post->images)): ?>
                                <?php foreach ($post->images as $key => $image): ?>
                                    <li data-target="#carouselExampleIndicators<?= $post->id ?>" data-slide-to="<?= $key ?>" class="<?= $key == 0 ? 'active' : '' ?>"></li>
                                <?php endforeach; ?>
                            <?php else: ?>
                               <li data-target="#carouselExampleIndicators<?= $post->id ?>" data-slide-to="0" class="active"></li>
                            <?php endif; ?>
                          </ol>
                          <div class="carousel-inner">
                            <?php if (!empty($post->images)): ?>
                                <?php foreach ($post->images as $key => $image): ?>
                                    <div data-id = "<?= $post->id ?>" class="carousel-item <?= $key == 0 ? 'active' : '' ?> post-target">
                                        <img style="height: 216px; display: block; margin-left: auto; margin-right: auto;" class="" src="/app/images/<?= $image->url ?>" alt="">
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <div class="carousel-item active">
                                    <img style="width: 100%; height: 100%;" class="" src="/app/images/noimage.png" alt="Sin Imagen">
                                </div>
                            <?php endif; ?>
                          </div>
                          <a class="carousel-control-prev" href="#carouselExampleIndicators<?= $post->id ?>" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Anterior</span>
                          </a>
                          <a class="carousel-control-next" href="#carouselExampleIndicators<?= $post->id ?>" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Siguiente</span>
                          </a>
                        </div>
                      <?php endif; ?>
                      <?php
                          $status_select = 8;
                          $show_status = $_SESSION['paraments']->post->status->$status->name != $_SESSION['paraments']->post->status->$status_select->name;
                        ?>
                        <?php if ($show_status): ?>
                          <div class="cinta">
                          </div>
                          <div class="status-container">
                             <label class="lbl-cinta"><?= $_SESSION['paraments']->post->status->$status->name ?></label>
                          </div>
                        <?php endif; ?>
                    </div>

                    <!-- Text Content -->
                    <div class="card-body post-body">
                      <div class="content-post">
                        <label data-id = "<?= $post->id ?>" class="post-target lbl-title-post"><?= $post->title ?></label>

                        <?php
                          $operation = $post->operation;
                          $property = $post->type_property;
                          $room = $post->room;
                          $money = $post->money;
                          $zone = $post->zone;
                          $zone_value = "";
                          if (!empty($zone)) {
                            $zone_value = " - " . $_SESSION['paraments']->post->zone->$zone;
                          } else {
                            $zone_value = "";
                          }
                        ?>

                        <div class="container-fluid">
                          <div class="row">
                            <div class="col-md-8">
                              <label class="lbl-post-item"><?= $_SESSION['paraments']->post->operation->$operation ?> - <?= $_SESSION['paraments']->post->property->$property ?></label>

                              <?php if (!empty($post->environment)): ?>
                                <?php
                                  $environment = $post->environment;
                                  if ($post->environment == 11) {
                                    $environment = "no";
                                  }
                                ?>
                                <label class="lbl-environment lbl-post-item">Ambientes: <?= $environment ?></label>
                              <?php endif; ?>
                              <label class="lbl-post-item"><?= $post->address; ?> <?= $zone_value ?></label>
                            </div>

                            <div class="col-md-4">
                              <?php if (!empty($post->room)): ?>
                                <?php
                                  $room = $post->room;
                                  if ($post->room == 11) {
                                    $room = "no";
                                  }
                                ?>
                                <label><i class="fa fa-bed" aria-hidden="true"></i> <?= $room ?></label>
                              <?php endif; ?>
                              <?php
                                $garage = "";
                                if ($post->garage == 3) {
                                  $garage = "+3";
                                }
                                if ($post->garage == 4) {
                                  $garage = "no";
                                }
                                if ($post->garage == 5) {
                                  $garage = "op";
                                }
                              ?>
                              <label><i class="fa fa-car" aria-hidden="true"></i> <?= $post->garage < 3 ? $post->gerage : $garage ?></label>
                              <?php
                                $bath = $post->bath;
                                if ($post->bath == 6) {
                                  $bath = "no";
                                }
                              ?>
                              <label><i class="fa fa-bath" aria-hidden="true"></i> <?= $bath ?></label>
                            </div>
                          </div>
                        </div>

                        <?php
                          $flag_add_favorite = false;
                          if ($loggedIn) {
                            if (!empty($account->favorites)) {
                              foreach ($account->favorites as $favorite) {
                                if ($favorite->post_id == $post->id) {
                                  $flag_add_favorite = true; 
                                }
                              }
                            }
                          }
                        ?>
                      </div>
                      <div class="post-footer">
                        <a data-id = "<?= $post->id ?>" data-add = "<?= $flag_add_favorite ?>" href="javascript:void(0)" style="" class="btn-add_favorite-list"><i class="<?= $flag_add_favorite ? 'fa fa-heart' : 'fa fa-heart-o' ?>" aria-hidden="true"></i> <?= $flag_add_favorite ? 'Añadido lista de Favoritos' : 'Añadir lista de Favoritos' ?></a>
                        <div class="mb-2 text-center">
                          <?php
                          $flag = true;
                          if (sizeof($post->images) > 0) {
                            foreach ($post->images as $img) {
                              if ($img->main) {
                                $flag = false;
                                $image = $img->url;
                              }
                            }
                            if ($flag) {
                              $image = $post->images[0]->url;
                            }
                          } else {
                            $image = "noimage.png";
                          }
                        ?>
                          <a href="javavascript:void(0)" data-title="<?= $post->title ?>" data-description="<?= $post->title ?>" data-picture="<?= $this->Url->build('/images/' . $image , true) ?>" data-url="<?= $this->Url->build('/inicio/publicaciones/' . $post->id , true) ?>" class="fa fa-facebook-square fa-2x facebook-icon"></a>
                          <?php 
                            if ($mobile) {
                              echo $this->SocialShare->fa(
                                'whatsapp_mobile',
                                $this->Url->build('/inicio/publicaciones/' . $post->id , true),
                                ['icon_class' => 'fa fa-whatsapp fa-2x ml-2 whatsapp-icon', 'text' => $post->title]
                              );
                            } else {
                              echo $this->SocialShare->fa(
                                'whatsapp',
                                $this->Url->build('/inicio/publicaciones/' . $post->id , true),
                                ['icon_class' => 'fa fa-whatsapp fa-2x ml-2 whatsapp-icon', 'text' => $post->title]
                              );
                            }
                          ?>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
            <?php else: ?>
              <h4 class="text-center lbl-no-post"><span>No se ha encontrado ninguna Publicación con los parametros de búsqueda seleccionados.</span></span> <i style="font-size: 20px;" class="fa fa-search"></i></h4>
            <?php endif; ?>
          </div>
        </div>
      </div>

      <?php if ($banner): ?>
        <?= $this->element('banners') ?>
      <?php endif; ?>
    </div>
</div>

<script type="text/javascript">

  var loggingIn = <?= json_encode($loggedIn) ?>;
  var type_user = <?= !empty($_SESSION['type_user']) ? json_encode($_SESSION['type_user']) : "[]" ?>;
  if (type_user == "[]") {
      type_user = false;
  }

  $(document).ready(function () {

    $('.post-target').click(function() {
      var id = $(this).data('id');
      window.location = "/app/inicio/publicaciones/" + id;
    });

    $('.btn-add_favorite-list').click(function() {
      var add = $(this).data('add');
      var id = $(this).data('id');
      if (loggingIn && type_user != 'admin') {
        if (add) {
          generateNoty('warning', 'La Publicación ya se encuentra en su lista de Favoritos.');
        } else {
          var action = "/app/accounts/agregarFavorito/" + id;
          $('body')
            .append( $('<form/>').attr({'action': action, 'method': 'post', 'id': 'replacer'})
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'action', 'value': 'index'}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'order', 'value': ''})))
            .find('#replacer').submit();
        }
      } else {
        generateNoty('warning', 'Para hacer uso de la lista de Favoritos, debe contar con una cuenta o ingresar a su cuenta en caso de tener una.');
      }
    });

    $('.cover-item ').click(function() {
      var link = $(this).data('link');
      if (link != "") {
        window.open(link, '_blank');
      }
    })

  });

</script>
