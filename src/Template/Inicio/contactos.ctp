<style type="text/css">
  hr.star-light,
  hr.star-primary {
    margin: 25px auto 0;
  }
</style>

<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Contactos Section -->
<section id="contact">
  <div class="container">
    <h2 class="text-center">Contactos</h2>
    <hr class="star-primary">
    <div class="row">
      <div class="col-lg-8 mx-auto">
        <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
        <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
        <?= $this->Form->create(null, ['url' => ['controller' => 'inicio' , 'action' => 'contactos']]) ?>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Nombre</label>
              <input name="name" class="form-control" id="name" type="text" placeholder="Nombre" required>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Correo</label>
              <input name="email" class="form-control" id="email" type="email" placeholder="Correo" required>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Teléfono</label>
              <input name="phone" class="form-control" id="phone" type="phone" placeholder="Teléfono" required>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Mensaje</label>
              <textarea name="message" class="form-control" id="message" rows="2" placeholder="Mensaje" required></textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>

          <div id="success"></div>
          <div style="text-align: center; position: relative; display: inline-block;" class="g-recaptcha" data-sitekey="6Ld4jz0UAAAAAL5AeTo3-eYO-U-mUstDN0s7Vz_a"></div>
          <div class="form-group">
            <button type="submit" class="btn btn-success btn-md" id="sendMessageButton">Enviar</button>
          </div>

        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</section>
