<!-- Servicios Grid Section -->
<section id="portfolio">
  <div class="container">
    <h2 class="text-center lbl-title-services">Servicios</h2>
    <hr class="star-primary">
    <div class="row">
      <div class="col-sm-4 portfolio-item">
        <a class="portfolio-link" href="#portfolioModal1" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa fa-search-plus fa-3x icon-search-services"></i>
            </div>
          </div>
          <label class="lbl-services">ALQUILERES</label>
        </a>
      </div>
      <div class="col-sm-4 portfolio-item">
        <a class="portfolio-link" href="#portfolioModal2" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa fa-search-plus fa-3x icon-search-services"></i>
            </div>
          </div>
          <label class="lbl-services">VENTAS</label>
        </a>
      </div>
      <div class="col-sm-4 portfolio-item">
        <a class="portfolio-link" href="#portfolioModal3" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa fa-search-plus fa-3x icon-search-services"></i>
            </div>
          </div>
          <label class="lbl-services">TASACIONES</label>
        </a>
      </div>
      <div class="col-sm-4 portfolio-item">
        <a class="portfolio-link" href="#portfolioModal4" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa fa-search-plus fa-3x icon-search-services"></i>
            </div>
          </div>
          <label class="lbl-services">ADMINISTRACIÓN DE PROPIEDADES</label>
        </a>
      </div>
      <div class="col-sm-4 portfolio-item">
        <a class="portfolio-link" href="#portfolioModal5" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa fa-search-plus fa-3x icon-search-services"></i>
            </div>
          </div>
          <label class="lbl-services">CONSULTORÍAS</label>
        </a>
      </div>
      <div class="col-sm-4 portfolio-item">
        <a class="portfolio-link" href="#portfolioModal6" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa fa-search-plus fa-3x icon-search-services"></i>
            </div>
          </div>
          <label class="lbl-services">URBANIZACIONES</label>
        </a>
      </div>
      <div class="col-sm-4 portfolio-item">
        <a class="portfolio-link" href="#portfolioModal7" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa fa-search-plus fa-3x icon-search-services"></i>
            </div>
          </div>
          <label class="lbl-services">FONDO DE COMERCIALES</label>
        </a>
      </div>
      <div class="col-sm-4 portfolio-item">
        <a class="portfolio-link" href="#portfolioModal8" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa fa-search-plus fa-3x icon-search-services"></i>
            </div>
          </div>
          <label class="lbl-services">SERVICIOS CONTABLES</label>
        </a>
      </div>
    </div>
  </div>
</section>
