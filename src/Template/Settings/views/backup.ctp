<?php $this->extend('/Settings/views/download_upload'); ?>

<?php $this->start('backup'); ?>
    <br>
    <?= $this->Form->create($parament,  ['class' => 'form-load']) ?>
    <fieldset>

        <?= $this->Form->hidden('form', ['value' => 'backup']); ?>
        
        <!--<div class="row">-->
        <!--    <div class="col-xl-12">-->
        <!--         <legend class="sub-title-sm">Configuración</legend>-->
        <!--    </div>-->
        <!--</div>-->


        <div class="row">
            <div class="col-xl-4">
                
                <?= $this->Form->input('backup.enabled_databases', ['label' => 'Habilitar Backup Databases', 'type' => 'checkbox', 'class' => 'mt-0' , 'checked' => $parament->backup->enabled_databases]) ?>
                <?= $this->Form->input('backup.enabled_comprobantes', ['label' => 'Habilitar Backup Comprobantes', 'type' => 'checkbox',  'class' => 'mt-0' , 'checked' => $parament->backup->enabled_comprobantes]) ?>

                <div class="form-group required">
                    <label class="control-label" for="backup-hours-execution">Ejecución del Backup</label>
                    <div class='input-group date' id='backup-hours-execution-datetimepicker'>
                        <span class="input-group-addon mr-0">Horario</span>
                        <input  name="backup[hours_execution]" required="required"  id="backup-hours-execution"  type='text' class="form-control" />
                        <span class="input-group-addon mr-0">
                            <span class="glyphicon icon-calendar"></span>
                        </span>
                    </div>
                </div>
                
                <?= $this->Form->input('backup.folder_name', ['label' => 'Nombre de Carpeta (Dropbox)', 'type' => 'text',  'value' => $parament->backup->folder_name, "required" => true]) ?>
                <?= $this->Form->input('backup.amount_files', ['label' => 'Cantidad de Comprimidos (Comprobantes)', 'type' => 'number',  'value' => $parament->backup->amount_files, "required" => true]) ?>
                
                
            </div>

            <div class="col-xl-4">               
                
               
                <?= $this->Form->input('backup.app_key', ['label' => 'App Key (Dropbox)', 'type' => 'text',  'value' => $parament->backup->app_key, "required" => true]) ?>
                <?= $this->Form->input('backup.app_secret', ['label' => 'App Secret (Dropbox)', 'type' => 'text',  'value' => $parament->backup->app_secret, "required" => true]) ?>
                <?= $this->Form->input('backup.token', ['label' => 'Token (Dropbox)', 'type' => 'text',  'value' => $parament->backup->token, "required" => true]) ?>
            </div>     
            
             <div class="col-xl-4"> 
             <button class="btn btn-info" id="btn-force-backup" type="button" >Forzar Backup</button>
             
             </div>
            
        </div>
    </fieldset>
    <?= $this->Html->link(__('Cancelar'),["action" => "index"], ['class' => 'btn btn-default ']) ?>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
    <?= $this->Form->end() ?>

    

    <script type="text/javascript">

        $(document).ready(function () {

            $('#btn-force-backup').click(function() {

                var text = "¿Está Seguro que desea Forzar un Backup?";

                bootbox.confirm(text, function(result) {
                    if (result) {
                        $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/Settings/forceBackup', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
                        .find('#replacer').submit();
                    }
                });
            });
        });

    </script>
<?php $this->end(); ?>
