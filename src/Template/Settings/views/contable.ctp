<?php $this->extend('/Settings/views/portal'); ?>

<?php $this->start('contable'); ?>
    <?php
        $accountsListTitle = [];
        $accountsListTitle[''] = 'Raiz';
        foreach ($accountsTitles as $a) {
            $accountsListTitle[$a->code] = $a->code . ' ' . $a->name;
        }

        $accountsArray = [];
        $accountsArray[''] = 'Raiz';
        foreach ($accounts as $a) {
            $accountsArray[$a->code] = $a->code . ' ' . $a->name;
        }
    ?>

    <?= $this->Form->create($parament, ['enctype' => 'multipart/form-data',  'class' => 'form-load']) ?>
        <fieldset>

            <?= $this->Form->hidden('form', ['value' => 'contable']); ?>

            <br>
            <div class="row">
                <div class="col-md-10">
                     <table>
                        <tr>
                            <!--<td>Cajas Cuenta Padre  &nbsp; </td>-->
                            <td style="width:300px">
                                <input type="hidden" name="accountant.acounts_parent.cashs"  id="cashs_account_parent" value="<?=($parament->accountant->acounts_parent->cashs) ? $parament->accountant->acounts_parent->cashs : ''  ?>">
                                <input type="text" id="cashs_account_parent-show" value="<?=($parament->accountant->acounts_parent->cashs) ? $accountsArray[$parament->accountant->acounts_parent->cashs] : ''  ?>" class="form-control" placeholder="Cajas Cuenta Padre" readonly>
                            </td>
                            <td> 
                                <a class="btn btn-default btn-search-account btn-search"  href="#" data-input="cashs_account_parent" role="button">
                                    <span class="glyphicon icon-search"  aria-hidden="true"></span>
                                </a>
                            </td>
                            <td> 
                                <a class="btn btn-default btn-bin-account btn-search"  href="#" data-input="cashs_account_parent" role="button">
                                    <span class="glyphicon icon-bin"  aria-hidden="true"></span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <!--<td>Clientes Cuenta Padre  &nbsp; </td>-->
                            <td style="width:250px">
                                <input type="hidden" name="accountant.acounts_parent.customers"  id="customer_account_parent" value="<?=($parament->accountant->acounts_parent->customers) ? $parament->accountant->acounts_parent->customers : '' ?>" >
                                <input type="text" id="customer_account_parent-show" class="form-control" readonly value="<?=($parament->accountant->acounts_parent->customers) ? $accountsArray[$parament->accountant->acounts_parent->customers] : ''  ?>" placeholder="Clientes Cuenta Padre">
                            </td>
                            <td> 
                                <a class="btn btn-default btn-search-account btn-search"  href="#" data-input="customer_account_parent" role="button">
                                    <span class="glyphicon icon-search"  aria-hidden="true"></span>
                                </a>
                            </td>
                            <td> 
                                <a class="btn btn-default btn-bin-account btn-search"  href="#" data-input="customer_account_parent" role="button">
                                    <span class="glyphicon icon-bin"  aria-hidden="true"></span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <!--<td>Servicios Intenet Cuenta Padre  &nbsp; </td>-->
                            <td style="width:250px">
                                <input type="hidden" name="accountant.acounts_parent.services"  id="service_internet_account_parent" value="<?=($parament->accountant->acounts_parent->services) ? $parament->accountant->acounts_parent->services : '' ?>" >
                                <input type="text" id="service_internet_account_parent-show" class="form-control" readonly value="<?=($parament->accountant->acounts_parent->services) ? $accountsArray[$parament->accountant->acounts_parent->services] : '' ?>" placeholder="Servicios Intenet Cuenta Padre">
                            </td>
                            <td> 
                                <a class="btn btn-default btn-search-account btn-search"  href="#" data-input="service_internet_account_parent" role="button">
                                    <span class="glyphicon icon-search"  aria-hidden="true"></span>
                                </a>
                            </td>
                            <td> 
                                <a class="btn btn-default btn-bin-account btn-search"  href="#" data-input="service_internet_account_parent" role="button">
                                    <span class="glyphicon icon-bin"  aria-hidden="true"></span>
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <!--<td>Paquetes de Instalación Cuenta Padre  &nbsp; </td>-->
                            <td style="width:250px">
                                <input type="hidden" name="accountant.acounts_parent.packages"  id="packages_account_parent" value="<?=($parament->accountant->acounts_parent->packages) ? $parament->accountant->acounts_parent->packages : '' ?>" >
                                <input type="text" id="packages_account_parent-show" class="form-control" readonly value="<?=($parament->accountant->acounts_parent->packages) ? $accountsArray[$parament->accountant->acounts_parent->packages]: '' ?>" placeholder="Paquetes Cuenta Padre">
                            </td>
                            <td> 
                                <a class="btn btn-default btn-search-account btn-search"  href="#" data-input="packages_account_parent" role="button">
                                    <span class="glyphicon icon-search"  aria-hidden="true"></span>
                                </a>
                            </td>
                            <td> 
                                <a class="btn btn-default btn-bin-account btn-search"  href="#" data-input="packages_account_parent" role="button">
                                    <span class="glyphicon icon-bin"  aria-hidden="true"></span>
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <!--<td>Productos Cuenta Padre  &nbsp; </td>-->
                            <td style="width:250px">
                                <input type="hidden" name="accountant.acounts_parent.products"  id="products_account_parent" value="<?=($parament->accountant->acounts_parent->products) ? $parament->accountant->acounts_parent->products : '' ?>">
                                <input type="text" id="products_account_parent-show" class="form-control" readonly value="<?=($parament->accountant->acounts_parent->products) ? $accountsArray[$parament->accountant->acounts_parent->products] : '' ?>" placeholder="Productos Cuenta Padre">
                            </td>
                            <td> 
                                <a class="btn btn-default btn-search-account btn-search"  href="#" data-input="products_account_parent" role="button">
                                    <span class="glyphicon icon-search"  aria-hidden="true"></span>
                                </a>
                            </td>
                            <td> 
                                <a class="btn btn-default btn-bin-account btn-search"  href="#" data-input="products_account_parent" role="button">
                                    <span class="glyphicon icon-bin"  aria-hidden="true"></span>
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <!--<td>Cuenta de Recargos &nbsp; </td>-->
                            <td style="width:250px">
                                <input type="hidden" name="accountant.acounts_parent.surcharge"  id="recarg_account_code" value="<?=($parament->accountant->acounts_parent->surcharge) ? $parament->accountant->acounts_parent->surcharge : '' ?>" >
                                <input type="text" id="recarg_account_code-show" class="form-control" readonly value="<?=($parament->accountant->acounts_parent->surcharge) ? $accountsArray[$parament->accountant->acounts_parent->surcharge] : '' ?>" placeholder="Cuenta de Recargos">
                            </td>
                            <td> 
                                <a class="btn btn-default btn-search-account btn-search"  href="#" data-input="recarg_account_code" role="button">
                                    <span class="glyphicon icon-search"  aria-hidden="true"></span>
                                </a>
                            </td>
                            <td> 
                                <a class="btn btn-default btn-bin-account btn-search"  href="#" data-input="recarg_account_code" role="button">
                                    <span class="glyphicon icon-bin"  aria-hidden="true"></span>
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <!--<td>Cuenta de Bonificiones &nbsp; </td>-->
                            <td style="width:250px">
                                <input type="hidden" name="accountant.acounts_parent.bonus"  id="bonif_account_code" value="<?=($parament->accountant->acounts_parent->bonus) ? $parament->accountant->acounts_parent->bonus : '' ?>" >
                                <input type="text" id="bonif_account_code-show" class="form-control" readonly value="<?=($parament->accountant->acounts_parent->bonus) ? $accountsArray[$parament->accountant->acounts_parent->bonus] : '' ?>" placeholder="Cuenta de Bonificiones">
                            </td>
                            <td> 
                                <a class="btn btn-default btn-search-account btn-search"  href="#" data-input="bonif_account_code" role="button">
                                    <span class="glyphicon icon-search"  aria-hidden="true"></span>
                                </a>
                            </td>
                             <td> 
                                <a class="btn btn-default btn-bin-account btn-search"  href="#" data-input="bonif_account_code" role="button">
                                    <span class="glyphicon icon-bin"  aria-hidden="true"></span>
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <!--<td>Cuenta de Servicios Gratis &nbsp; </td>-->
                            <td style="width:250px">
                                <input type="hidden" name="accountant.acounts_parent.services_free"  id="bonif_services_account_code" value="<?=($parament->accountant->acounts_parent->services_free) ? $parament->accountant->acounts_parent->services_free : '' ?>" >
                                <input type="text" id="bonif_services_account_code-show" class="form-control" readonly value="<?=($parament->accountant->acounts_parent->services_free) ? $accountsArray[$parament->accountant->acounts_parent->services_free]: '' ?>" placeholder="Cuenta de Servicios Gratis">
                            </td>
                            <td> 
                                <a class="btn btn-default btn-search-account btn-search"  href="#" data-input="bonif_services_account_code" role="button">
                                    <span class="glyphicon icon-search"  aria-hidden="true"></span>
                                </a>
                            </td>
                             <td> 
                                <a class="btn btn-default btn-bin-account btn-search"  href="#" data-input="bonif_services_account_code" role="button">
                                    <span class="glyphicon icon-bin"  aria-hidden="true"></span>
                                </a>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </fieldset>
        <br>
        <?= $this->Html->link(__('Cancelar'),["action" => "index"], ['class' => 'btn btn-default']) ?>
        <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
    <?= $this->Form->end() ?>
<?php $this->end(); ?>
