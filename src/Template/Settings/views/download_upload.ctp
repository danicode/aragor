<?php $this->extend('/Settings/views/tabs'); ?>

<?php $this->start('download_upload'); ?>
    <br>
    <?= $this->Form->create($parament, ['enctype' => "multipart/form-data",  'class' => 'form-load', 'id' => 'form-download_upload'] ) ?>

    <fieldset>

        <?= $this->Form->hidden('form', ['value' => 'download_upload']); ?>

        <div class="row">
            <div class="col-md-4">
                
                <legend class="sub-title-sm">Descargar Archivo de Configuración</legend>
                <?=$this->Html->link('Descargar','/Settings/download', ['class' => 'btn btn-default', 'target' => '_self']) ?> 
                <br>
                <br>

                <legend class="sub-title-sm">Subir Archivo de Configuración</legend>
                <input name="upload_parament" type="file" id="upload_parament">
                <br>

            </div>
        </div>
    </fieldset>
    <?= $this->Html->link(__('Cancelar'),["action" => "index"], ['class' => 'btn btn-default ']) ?>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
    <?= $this->Form->end() ?>
<?php $this->end(); ?>
