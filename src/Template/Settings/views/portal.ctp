<?php $this->extend('/Settings/views/backup'); ?>

<?php $this->start('portal'); ?>
    <br>
    <?= $this->Form->create($parament,  ['class' => 'form-load']) ?>
    <fieldset>

        <?= $this->Form->hidden('form', ['value' => 'portal']); ?>

        <div class="row">
            <div class="col-md-2">
                <legend class="sub-title-sm">Mostrar</legend>
                <?= $this->Form->input('portal.show_resume', ['label' => 'Resumen de Cuenta', 'type' => 'checkbox',  'checked' => $parament->portal->show_resume]) ?>
                <?= $this->Form->input('portal.show_receipt', ['label' => 'Recibo', 'type' => 'checkbox',  'checked' => $parament->portal->show_receipt]) ?>
            </div>
        </div>
    </fieldset>
    <?= $this->Html->link(__('Cancelar'),["action" => "index"], ['class' => 'btn btn-default ']) ?>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
    <?= $this->Form->end() ?>
<?php $this->end(); ?>
