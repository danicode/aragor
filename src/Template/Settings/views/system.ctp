<?php $this->extend('/Settings/views/contable'); ?>

<?php $this->start('system'); ?>
    <br>
    <?= $this->Form->create($parament,  ['class' => 'form-load']) ?>
    <fieldset>

        <?= $this->Form->hidden('form', ['value' => 'system']); ?>

        <div class="row">

            <div class="col-md-2">

                <legend class="sub-title-sm">Mapas</legend>
                <?= $this->Form->input('system.map.lng', ['label' => 'Latitud', 'type' => 'number', 'step' => 'any', 'value' =>$parament->system->map->lng]) ?>
                <?= $this->Form->input('system.map.lat', ['label' => 'Longitud', 'type' => 'number', 'step' => 'any', 'value' =>$parament->system->map->lat]) ?>

                <legend class="sub-title-sm">Server</legend>
                <?= $this->Form->input('system.server.private_ip', ['label' => 'IP Privada', 'value' => $parament->system->server->private_ip]) ?>
                <?= $this->Form->input('system.server.public_ip', ['label' => 'IP Publica', 'value' => $parament->system->server->public_ip]) ?>

            </div>

            <div class="col-md-3">
                <legend class="sub-title-sm">Clientes</legend>
                <?= $this->Form->input('customer.doc_validate', ['label' => 'Validación por DNI', 'type' => 'checkbox',  'checked' => $parament->customer->doc_validate]) ?>
                <?= $this->Form->input('customer.searching_code', ['label' => 'Busqueda por Código', 'type' => 'checkbox',  'checked' => $parament->customer->searching_code]) ?>

                <legend class="sub-title-sm">Integración</legend>
                <?= $this->Form->input('system.integration', ['label' => 'Conexión con los Controladores', 'type' => 'checkbox', 'checked' => $parament->system->integration]) ?>

                <legend class="sub-title">Conexiones</legend>

                <legend class="sub-title-sm">Credenciales Autogenerado</legend>
                <?= $this->Form->input('connection.credencials', ['label' => 'Habilitado', 'type' => 'checkbox', 'checked' => $parament->connection->credencials]) ?>

                <legend class="sub-title-sm">Accounting Trafic</legend>
                <?= $this->Form->input('connection.accounting_traffic.enabled', ['label' => 'Habilitado', 'type' => 'checkbox', 'checked' => $parament->connection->accounting_traffic->enabled == 'yes'? true : false]) ?>

                <table>
                    <tr>
                        <td>Delay &nbsp;</td>
                         <td style="width: 100px">
                           <?= $this->Form->number('connection.accounting_traffic.delay', [ 'min' => 1, 'value' => $parament->connection->accounting_traffic->delay]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Threshold &nbsp; </td>
                        <td style="width: 100px">
                           <?= $this->Form->number('connection.accounting_traffic.threshold', ['min' => 256, 'value' => $parament->connection->accounting_traffic->threshold]) ?>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </fieldset>
    <?= $this->Html->link(__('Cancelar'),[ "action" => "index"], ['class' => 'btn btn-default ']) ?>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
    <?= $this->Form->end() ?>
<?php $this->end(); ?>
