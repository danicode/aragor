<?php $this->extend('/Settings/views/system'); ?>

<?php $this->start('bills'); ?>

    <style type="text/css">

        .td-presupuesto, .td-denomination {
            padding-bottom: 8px;
            width:200px;
        }

        .td-deuda {
            width: 200px;
        }

        .table-integracion-afip2 {
            margin-top: 10px;
        }

        .table-integracion-afip2 td {
            padding-top: 15px;
        }

        #accountant-surcharges-enabled {
            margin-top: 10px;
        }

        #invoicing-is-presupuesto {
            margin-top: 20px;
        }

        #accountant-service-debt-proporcional {
            margin-top: 20px;
        }

        #accountant-service-debt-change {
            margin-top: 10px;
        }

        #customer-doc-validate {
            margin-top: 15px;
        }

        #customer-searching-code {
            margin-top: 15px;
        }

        #system-integration {
            margin-top: 15px;
        }

        #connection-credencials {
            margin-top: 15px;
        }

        #connection-accounting-traffic-enabled {
            margin-top: 15px;
        }

        .sub-title-sm {
            margin-top: 15px;
        }

        .sub-title {
           margin-top: 15px;
        }

        #portal-show-resume{
            margin-top: 15px;
        }

        #portal-show-receipt {
            margin-top: 15px;
            margin-bottom: 15px;
        }

        #upload_parament {
            margin-bottom: 15px;
        }

    </style>

    <?= $this->Form->create($parament, ['enctype' => "multipart/form-data",  'class' => 'form-load', 'id' => 'form-bills']) ?>
        <fieldset>

            <?= $this->Form->hidden('form', ['value' => 'bills']); ?>


            <?php
            
            $responsiblesCustom = [ 1 => "IVA Responsable Inscripto", 6 => "Responsable Monotributo" ];
            
            ?>

            <br>
            <div class="row">

                <div class="col-md-5">

                    <table>
                        <tr>
                            <td colspan="2"><legend class="sub-title-sm">Información de la Empresa </legend></td>
                        </tr>
                        <tr>
                            <td>Nombre &nbsp; </td>
                            <td style="width:250px"><?=$this->Form->text('invoicing.company.name', ['value' => $parament->invoicing->company->name])?></td>
                        </tr>
                        <tr>
                            <td>Domicilio &nbsp; </td>
                            <td style="width:250px"><?=$this->Form->text('invoicing.company.address', ['value' => $parament->invoicing->company->address])?></td>
                        </tr>
                        <tr>
                            <td>Código Postal &nbsp; </td>
                            <td style="width:250px"><?=$this->Form->text('invoicing.company.cp', ['value' => $parament->invoicing->company->cp])?></td>
                        </tr>
                        <tr>
                            <td>Ciudad &nbsp; </td>
                            <td style="width:250px"><?=$this->Form->text('invoicing.company.city', ['value' => $parament->invoicing->company->city])?></td>
                        </tr>
                         <tr>
                            <td>Teléfono &nbsp; </td>
                            <td style="width:250px"><?=$this->Form->text('invoicing.company.phone', ['value' => $parament->invoicing->company->phone])?></td>
                        </tr>
                         <tr>
                            <td>Fax &nbsp; </td>
                            <td style="width:250px"><?=$this->Form->text('invoicing.company.fax', ['value' => $parament->invoicing->company->fax])?></td>
                        </tr>

                        <tr>
                            <td>CUIT &nbsp; </td>
                            <td style="width:250px"><?=$this->Form->text('invoicing.company.ident', ['value' => $parament->invoicing->company->ident])?></td>
                        </tr>
                         <tr>
                            <td>Ingreso Bruto &nbsp; </td>
                            <td style="width:250px"><?=$this->Form->text('invoicing.company.ing_bruto', ['value' => $parament->invoicing->company->ing_bruto])?></td>
                        </tr>
                         <td>Email &nbsp; </td>
                            <td style="width:250px"><?=$this->Form->text('invoicing.company.email', ['value' => $parament->invoicing->company->email])?></td>
                        </tr>
                        <tr>
                            <td>Web &nbsp; </td>
                            <td style="width:250px"><?=$this->Form->text('invoicing.company.web', ['value' => $parament->invoicing->company->web])?></td>
                        </tr>
                        <tr>
                            <td>Inicio de actividades  &nbsp; </td>
                            <td>
                                <div class="form-group date required">
                                	<input type="date"  class="form-control" name="invoicing.company.init_act" id="invoicing-company-init_act" value="<?= date('Y-m-d', strtotime($parament->invoicing->company->init_act)) ?>" required>
                            	</div>
                        	</td>
                        </tr>
                        <tr>
                            <td>Responsabilidad ante IVA &nbsp; </td>
                            <td style="width:250px"><?=$this->Form->select('invoicing.company.responsible', $responsiblesCustom, ['value' => $parament->invoicing->company->responsible ])?></td>
                        </tr>
                        <tr>
                            <td>Copias de Factura &nbsp; </td>
                            <td style="width:250px"><?=$this->Form->select('invoicing.copias', [1 => 'Solo Original', 2 => 'Duplicado', 3 => 'Triplicado'], ['value' => $parament->invoicing->copias ])?></td>
                        </tr>
                    </table>
                    <br>
                    <table>
                        <tr>
                            <td colspan="3"><legend class="sub-title-sm">Estilo PDF </legend></td>
                        </tr>
                         <tr>
                            <td>Color &nbsp;</td>
                            <td  style="width:80px"><?= $this->Form->number('invoicing.color.hex', ['type' => 'color', 'value' => $parament->invoicing->color->hex, 'id' => 'invoicing_color_hex'])?></td>
                            <td>
                               <label  title="Click para cambiar de imagen." style="padding-left: 10px; ">
                                    <img src="/ispbrain/img/<?=$parament->invoicing->company->logo?>" id="image-preview" style="max-width: 100px; "></img>
                                    <br> Buscar Imagen <input name="invoicing.company.logo" type="file" style="display: none;" id="image-logo">
                                </label>
                            </td>
                        </tr>
                    </table>

                    <?= $this->Form->hidden('invoicing.color.rgb', ['type' => 'color', 'value' => $parament->invoicing->color->rgb, 'id' => 'invoicing_color_rgb'])?>

                </div>

                <div class="col-md-4">

                    <table>
                        <tr>
                            <td colspan="2"><legend class="sub-title-sm">Facturación</legend></td>
                        </tr>
                        <tr>
                            <td>Tipo de documento por defecto &nbsp;</td>
                            <td  style="width:200px"><?= $this->Form->select('customer.doc_type', $doc_types, ['value' => $parament->customer->doc_type])?></td>
                        </tr>
                        <tr>
                            <td>Tipo de cliente por defecto &nbsp;</td>
                            <td  style="width:200px"><?= $this->Form->select('customer.responsible', $responsibles,  ['value' => $parament->customer->responsible])?></td>
                        </tr>
                         <tr>
                            <td>Denominación &nbsp;</td>
                            <td class="td-denomination"><?= $this->Form->input('customer.denomination', ['label' => 'Habilitado', 'checked' => $parament->customer->denomination])?></td>
                        </tr>
                         <tr>
                            <td>Condiciòn de venta por defecto &nbsp;</td>
                            <td  style="width:200px"><?= $this->Form->select('customer.cond_venta', $cond_venta,  ['value' => $parament->customer->cond_venta])?></td>
                        </tr>
                        <tr>
                            <td>Presupuesto &nbsp;</td>
                            <td class="td-presupuesto"><?= $this->Form->input('invoicing.is_presupuesto', ['label' => 'Habilitado', 'checked' => $parament->invoicing->is_presupuesto])?></td>
                        </tr>
                        <tr>
                            <td>Tipo de vencimiento &nbsp;</td>
                            <td  style="width:200px"><?= $this->Form->select('accountant.type', $accountant_types, ['value' => $parament->accountant->type])?></td>
                        </tr>
                        <tr>
                            <td>Día de vencimiento &nbsp; </td>
                            <td style="width:80px"><?= $this->Form->number('accountant.daydue', ['value' => $parament->accountant->daydue])?></td>
                        </tr>
                        <tr>
                            <td>Deuda por servicio nuevo &nbsp;</td>
                            <td  style="width:200px"><?= $this->Form->input('accountant.service_debt', ['label' => false, 'options' => ['Proporcional' => 'Proporcional', /*'Completo' => 'Completo',*/ 'None' => 'None'], 'type' => 'radio', 'value' => $parament->accountant->service_debt])?></td>
                        </tr>
                         <tr>
                            <td style="padding-bottom: 5px;">Deuda por Cambio de Plan &nbsp;</td>
                            <td class="td-deuda"><?= $this->Form->input('accountant.service_debt_change', ['label' => 'Ajustar', 'type' => 'checkbox', 'checked' => $parament->accountant->service_debt_change])?></td>
                        </tr>
                    </table>
                    <table>
                         <tr>
                            <td style="padding-top: 14px;">Punto de Venta &nbsp; </td>
                            <td style="width:80px; padding-top: 12px;"><?= $this->Form->number('invoicing.pto_vta', ['value' => $parament->invoicing->pto_vta])?></td>
                        </tr>
                    </table>

                    <br>

                    <legend class="sub-title-sm">Integración AFIP</legend>
                    <table>
                        <tr>
                            <td><?= $this->Form->input('invoicing.integration_afip', ['label' => 'Habilitado &nbsp; &nbsp;', 'escape' => false, 'checked' => $parament->invoicing->integration_afip])?></td>
    
                            <td><?= $this->Form->input('invoicing.company.webservice_afip.testing', ['label' => 'Testing &nbsp; &nbsp;', 'escape' => false, 'checked' => $parament->invoicing->company->webservice_afip->testing])?></td>
    
                            <td><?= $this->Form->input('invoicing.company.webservice_afip.log', ['label' => 'Log &nbsp; &nbsp;', 'escape' => false, 'checked' => $parament->invoicing->company->webservice_afip->log])?>  </td>
                        </tr>
                    </table>

                    <table class="table-integracion-afip2">
                        <tr>
                            <td>Alisas: &nbsp;</td>
                            <td colspan="3">
                                <?=$this->Form->text('invoicing.company.webservice_afip.alias', ['value' => $parament->invoicing->company->webservice_afip->alias])?>
                            </td>
                        </tr>
                        <tr>
                            <td>Archivo .csr: &nbsp;</td>
                            <td ><?= $parament->invoicing->company->webservice_afip->csr ?> &nbsp; &nbsp;</td>
                            <td><?= $this->Html->link('Generar','/Settings/generateCSR',['class' => 'button', 'target' => '_blank']) ?> &nbsp;</td>
                            <td><?= $this->Html->link('Descargar','/Settings/downloadCSR',['class' => 'button', 'target' => '_blank']) ?> &nbsp;</td>
                        </tr>
                        <tr>
                            <td>Archivo .crt: &nbsp;</td>
                            <td id="crt-preview" ><?=$parament->invoicing->company->webservice_afip->crt?> &nbsp; &nbsp;</td>
                            <td>
                                <label style="cursor: pointer; cursor: hand; margin-bottom: 0" >
                                    Subir <input name="invoicing.company.webservice_afip.crt" type="file" style="display: none;" id="webservice_afip_crt">
                                </label>
                            </td>
                            <td><?=$this->Html->link('Descargar','/Settings/downloadCRT',['class' => 'button', 'target' => '_blank']) ?> &nbsp;</td>
                        </tr>
                    </table>

                </div>

                <div class="col-md-3">
                    <table>
                        <tr>
                            <td colspan="2"><legend class="sub-title-sm">Intereses Cuotas</legend></td>
                        </tr>
                        <?php foreach($parament->accountant->dues as $num => $due): ?>
    
                        <tr>
                            <td>En <?= $num + 1 ?> cuotas &nbsp; </td>
                            <td><?= $this->Form->number('accountant.dues[]', ['value' => $due, 'step' => 0.01])?></td>
                        </tr>
    
                        <?php endforeach;?>
                    </table>
                    <br>
                    <table>
                        <tr>
                            <td colspan="2"><legend class="sub-title-sm">Recargo por Moroso</legend></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width:50px;"/><?=$this->Form->input('accountant.surcharges.enabled', ['type' => 'checkbox', 'label' => 'Habilitado','checked' => $parament->accountant->surcharges->enabled]);?></td>
                        </tr>
                        <tr>
                            <td>Recargo &nbsp; </td>
                            <td style="width:80px;"/><?=$this->Form->number('accountant.surcharges.value', ['value' => $parament->accountant->surcharges->value, 'step' => 0.01]);?></td>
                        </tr>
                        <tr>
                            <td>Alícuota &nbsp; </td>
                            <td style="width:180px;"/><?=$this->Form->select('accountant.surcharges.aliquot', $alicuotas_types, ['value' => $parament->accountant->surcharges->aliquot]);?></td>
                        </tr>
                        
                    </table>
                    
                    <br>
                     <table>
                        <tr>
                            <td colspan="2"><legend class="sub-title-sm">Corte automatico </legend></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width:50px;"/><?=$this->Form->input('accountant.disable_connections_auto.enabled', ['type' => 'checkbox', 'label' => 'Habilitado','checked' => $parament->accountant->disable_connections_auto->enabled]);?></td>
                        </tr>
                        <tr>
                            <td>Día &nbsp; </td>
                            <td style="width:80px;"/><?=$this->Form->number('accountant.disable_connections_auto.day', ['value' => $parament->accountant->disable_connections_auto->day, 'min' => 1, 'max' => 31]);?></td>
                        </tr>
                        <tr>
                            <td>Deuda mayor que &nbsp; </td>
                            <td style="width:80px;"/><?=$this->Form->number('accountant.disable_connections_auto.condition', ['value' => $parament->accountant->disable_connections_auto->condition, 'step' => 0.01]);?></td>
                        </tr>
                        <tr>
                            <td>Recargo &nbsp; </td>
                            <td style="width:80px;"/><?=$this->Form->number('accountant.disable_connections_auto.value', ['value' => $parament->accountant->disable_connections_auto->value, 'step' => 0.01]);?></td>
                        </tr>
                        <tr>
                            <td>Alícuota &nbsp; </td>
                            <td style="width:180px;"/><?=$this->Form->select('accountant.disable_connections_auto.aliquot', $alicuotas_types, ['value' => $parament->accountant->disable_connections_auto->aliquot]);?></td>
                        </tr>
                        
                    </table>
                    
                </div>

            </div>
       
        </fieldset>
        <br>
        <?= $this->Html->link(__('Cancelar'),["action" => "indx"], ['class' => 'btn btn-default']) ?>
        <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
    <?= $this->Form->end() ?>

<?php $this->end(); ?>
