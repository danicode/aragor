<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    .table > tbody > tr > td.left {
        text-align: left;
    }

    tr.title{
        color: #696868 !important;
        font-weight: bold;
    }

    .btn-search {
        padding: 2px;
        margin: 5px;
    }

    textarea.title {
         height: 25px !important;
    }

    textarea.des {
         height: 100px !important;
    }
    
    #invoicing_color_hex {
        height: 35px;
    }

</style>

<?php

    $accountsArray = [];
    foreach($accounts as $account){
        $accountsArray[$account->code] = $account->code .' '. $account->name;
    }

?>

<div class="row">
    <div class="col-md-12">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" id="myTab" role="tablist">

            <li class="nav-item">
                <a class="nav-link active" id="bills-tab" data-target="#bills" role="tab" aria-controls="bills" aria-expanded="true">Facturación</a>
            </li>

            <li class="nav-item">
                <a class="nav-link " id="system-tab" data-target="#system" role="tab" aria-controls="system" aria-expanded="false">Sistema</a>
            </li>

            <li class="nav-item">
                <a class="nav-link " id="contable-tab" data-target="#contable" role="tab" aria-controls="contable" aria-expanded="false">Contable</a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link " id="portal-tab" data-target="#portal" role="tab" aria-controls="portal" aria-expanded="false">Portal</a>
            </li>

            <li class="nav-item">
                <a class="nav-link " id="backup-tab" data-target="#backup" role="tab" aria-controls="backup" aria-expanded="false">Backup</a>
            </li>

            <li class="nav-item">
                <a class="nav-link " id="download_upload-tab" data-target="#download_upload" role="tab" aria-controls="download_upload" aria-expanded="false">Descargar/Subir</a>
            </li>

        </ul>

        <!-- Tab panes -->
        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="bills" role="tabpanel" aria-labelledby="bills-tab">
                <?= $this->fetch('bills') ?>
            </div>

            <div class="tab-pane fade" id="system" role="tabpanel" aria-labelledby="system-tab">
                <?= $this->fetch('system') ?>
            </div>

            <div class="tab-pane fade" id="contable" role="tabpanel" aria-labelledby="contable-tab">
                <?= $this->fetch('contable') ?>
            </div>

            <div class="tab-pane fade" id="portal" role="tabpanel" aria-labelledby="portal-tab">
                <?= $this->fetch('portal') ?>
            </div>

            <div class="tab-pane fade" id="backup" role="tabpanel" aria-labelledby="backup-tab">
                <?= $this->fetch('backup') ?>
            </div>

            <div class="tab-pane fade" id="download_upload" role="tabpanel" aria-labelledby="download_upload-tab">
                <?= $this->fetch('download_upload') ?>
            </div>

        </div>

    </div>
</div>

<div class="modal fade" id="modal-accounts" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel">Seleccionar Cuenta</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">

                    <table class="table table-hover" id="table-accounts">
                        <thead>
                            <tr>
                                <th>Cód.</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($accounts as $acc): ?>
                            <tr class="<?= ($acc->title) ? 'title' : '' ?> <?= (!$acc->status) ? ' font-through' : '' ?>"
                                data-code="<?= $acc->code ?>"
                                data-name="<?= $acc->name ?>"
                                >
                                <td class="left"><?= $acc->code ?></td>
                                <td class="left"><?php

                                    $hrchy = substr(strval($acc->code),0,4);
                                    $hrchy = str_split($hrchy);
                                    foreach ($hrchy as $h) {
                                        if ($h != '0') {
                                            echo '&nbsp;&nbsp;&nbsp;'; 
                                        }
                                    }
                                    if (!$acc->title) {
                                        echo '&nbsp;&nbsp;&nbsp;'; 
                                    }
                                    echo $acc->name; 
                                ?>
                                </td>
                                <td class="left"><?= $acc->description ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <br>

                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" id="btn-account-selected" class="btn btn-success">Seleccionar</button> 
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    var parament;

    function hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    var table_acccounts = null;

    $(document).ready(function () {

        parament = <?= json_encode($parament) ?>;

        $('#table-accounts').removeClass('display');

		table_acccounts = $('#table-accounts').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '480px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 100, -1], [50, 100, "Todas"]],
		});

		$('#table-accounts_filter').closest('div').before($('#btns-tools').contents());   

		var input_select = null;

		$('.btn-search-account').click(function() {

		    input_select = $(this).data('input');
		    $('#modal-accounts').modal('show');
		});
		
		$('.btn-bin-account').click(function(){

		    var input_select_temp = $(this).data('input');
		    $('#'+input_select_temp).val('');
		    $('#'+input_select_temp+'-show').val('');
		});

		$('#btn-account-selected').click(function() {

		    var code = table_acccounts.$('tr.selected').data('code');
		    var name = table_acccounts.$('tr.selected').data('name');

		    $('#'+input_select).val(code);
		    $('#'+input_select+'-show').val(code + ' ' + name);

		    $('#modal-accounts').modal('hide');
		});

		$('#modal-accounts').on('shown.bs.modal', function (e) {
		    table_acccounts.draw();
        });

        $('#table-accounts tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_acccounts.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });

        $('#invoicing_color_hex').change(function() {
            var hex = $(this).val();
            var rgb = hexToRgb(hex);
            if (!rgb) {
                generateNoty('warning', 'No se pudo generar el RGB del color seleccionado.');
            }
            $('#invoicing_color_rgb').val(rgb.r +','+rgb.g +','+rgb.b);
        });

        $('#myTab a').click(function (e) {

            e.preventDefault();

            var target = $(this).data('target');
            $('#myTab a.active').removeClass('active');

            $('#myTabContent .active').fadeOut(100, function() {
            $('#myTabContent .active').removeClass('active');

                $(target).fadeIn(100, function() {
                   $(target).addClass('active show');
                   $(target+'-tab').addClass('active');
                   $(target+'-tab').trigger('shown.bs.tab');
                });
            });
        });

        var defaultDate = "2017-10-01T" + parament.backup.hours_execution;
        $('#backup-hours-execution-datetimepicker').datetimepicker({
            defaultDate: defaultDate,
            format: 'HH:mm',
        });
    });

    document.getElementById("image-logo").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("image-preview").src = e.target.result;
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    };

    document.getElementById("webservice_afip_crt").onchange = function () {
        $("#crt-preview").html(this.files[0].name);
    };

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
