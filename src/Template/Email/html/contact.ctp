<h2>Datos de Contacto</h2>
<p>Correo enviado desde el formulario de contacto del Sitio Web</p>
<br>
<h3>Nombre</h3>
<label><?= $data['name'] ?></label>
<h3>Correo</h3>
<label><?= $data['email'] ?></label>
<h3>Teléfono</h3>
<label><?= $data['phone'] ?></label>
<h3>Mensaje</h3>
<p><?= $data['message'] ?></p>
