<h2>Publicaciones que pueden interesarte</h2>
<br>
<?php foreach ($data->posts as $post): ?>
    <h3><?= $post->title ?></h3>
    <?php
        echo $this->Html->link(
            __('Ver publicación'),
            ['controller' => 'Inicio', 'action' => 'publicaciones', $post->id, '_full' => true]
        );
    ?>
    <br>
<?php endforeach; ?>
<p>¡Muchas gracias por comunicarte con nosotros!</p>
