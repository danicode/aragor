<style type="text/css">

    #modal-filter .form-group {
        margin-bottom: 4px;
    }

    .query-message-title {
        font-weight: bold;
        color: #8c8f8f;
    }

    .query-code-title {
        font-weight: bold;
        color: #8c8f8f;
    }

    .query-super-title {
        border-bottom: 1px solid #dfdbe5;
        color: #8c8f8f;
    }

</style>

<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-filter" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Filtrar por Columnas',
                'class' => 'btn btn-default btn-filter-column',
                'data-toggle' => 'modal', 'data-target' => '#modal-filter',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la tabla',
                'class' => 'btn btn-default btn-export',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-selection',
        'name' =>  'Responder',
        'icon' =>  'fa fa-envelope-o',
        'type' =>  'btn-info'
    ];

    $buttons[] = [
        'id'   =>  '',
        'name' =>  'Comentario',
        'icon' =>  'fa fa-edit',
        'type' =>  'btn-success btn-edit-comment'
    ];

    echo $this->element('actions', ['modal'=> 'modal-email-records', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered" id="table-email-records">
            <thead>
                <tr>
                    <th>Estado</th>     <!-- 1 -->
                    <th>Mensaje</th>    <!-- 2 -->
                    <th>Desde</th>      <!-- 3 -->
                    <th>Cód. Ref.</th>  <!-- 4 -->
                    <th>Nombre</th>     <!-- 5 -->
                    <th>Teléfono</th>   <!-- 6 -->
                    <th>Correo</th>     <!-- 7 -->
                    <th>Enviado a:</th> <!-- 8 -->
                    <th>Comentario</th> <!-- 9 -->
                    <th>Creado</th>     <!-- 10 -->
                    <th>Modificado</th> <!-- 11 -->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modal-filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Filtrar por columnas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

          <?= $this->Form->create() ?>

          <div class="row">
              <div class="col-md-4">
                <div class="form-group select">
                    <label class="control-label" for="status">Estado</label>
                    <select name="status" class="column_search form-control" data-column="0" id="status">
                        <option value="">Seleccionar Estado</option>
                        <option value="Nuevo">Nuevo</option>
                        <option value="Enviado">Enviado</option>
                    </select>
                </div>
                <?= $this->Form->input('message', ['label' => __('Mensaje'), 'class'=> 'column_search', 'data-column' => 1]) ?>
                <div class="form-group select">
                    <label class="control-label" for="status">Desde</label>
                    <select name="send_from" class="column_search form-control" data-column="2" id="send_from">
                        <option value="">Seleccionar</option>
                        <option value="Formulario Contacto">Formulario Contacto</option>
                        <option value="Formulario Publicación">Formulario Publicación</option>
                    </select>
                </div>
                <?= $this->Form->input('post_code', ['label' => __('Cód. Ref.'), 'class'=> 'column_search', 'data-column' => 3]) ?>
              </div>
              <div class="col-md-4">
                <?= $this->Form->input('customer_name', ['label' => __('Nombre'), 'class'=> 'column_search', 'data-column' => 4]) ?>
                <?= $this->Form->input('customer_phone', ['label' => __('Teléfono'), 'class'=> 'column_search', 'data-column' => 5]) ?>
                <?= $this->Form->input('customer_email', ['label' => __('Correo'), 'class'=> 'column_search', 'data-column' => 6]) ?>
                <?= $this->Form->input('email_contact', ['label' => __('Enviado a:'), 'class'=> 'column_search', 'data-column' => 7]) ?>
              </div>
              <div class="col-md-4">
                <?= $this->Form->input('email_contact', ['label' => __('Enviado a:'), 'class'=> 'column_search', 'data-column' => 8]) ?>
                <div class="form-group">
                        <label class="control-label" for="created">Creado</label>
                        <div class='input-group date' id='created-datetimepicker'>
                            <span class="input-group-addon mr-0">Fecha</span>
                            <input name="input-created" id="input-created" type='text' class="column_search form-control datetimepicker" data-column="9" />
                            <span class="input-group-addon mr-0">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                    </div>
                <div class="form-group">
                    <label class="control-label" for="modified">Modificado</label>
                    <div class='input-group date' id='modified-datetimepicker'>
                        <span class="input-group-addon mr-0">Fecha</span>
                        <input name="input-modified" id="input-modified" type='text' class="column_search form-control datetimepicker" data-column="10" />
                        <span class="input-group-addon mr-0">
                            <span class="glyphicon icon-calendar"></span>
                        </span>
                    </div>
                </div>
              </div>
          </div>

          <?= $this->Form->end() ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-clear-filter" >Limpiar</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-edit-comments" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit-comments">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Comentario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "EmailRecords", "action" => "EditComment"]) ?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">

                        <div class="col-xl-12">
                            <fieldset>
                                <?php
                                    echo $this->Form->input('comments', ['type' => 'textarea', 'label' => 'Comentarios']);
                                ?>
                                <input type="hidden" name="email_record_id" id="email_record_id">
                            </fieldset>

                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit">Guardar</button> 

                        </div>
                    </div>

                </form>
            </div>
            </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  '',
        'name' =>  'Comentario',
        'icon' =>  'fa fa-edit',
        'type' =>  'btn-success btn-edit-comment'
    ];

    $buttons[] = [
        'id'   =>  'btn-resend',
        'name' =>  'Reenviar',
        'icon' =>  'fa fa-envelope-o',
        'type' =>  'btn-info'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-alternative', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-email-records']);
?>

<style type="text/css">

    .modal a.btn{
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .modal span{
       margin-right: 15px;
    }

</style>

<div class="modal fade modal-posts" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="gridSystemModalLabel">Seleccionar Publicaciones</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row"->
            <div class="col-md-12">
                <h4 class="query-super-title">Datos de la consulta</h4>
            </div>
            <div class="col-md-12">
                <span class="query-message-title">Mensaje</span>
                <p class="query-message"></p>
            </div>
            <div class="col-md-12">
                <span class="query-code-title">Cód. Ref.:</span>
                <span class="query-code"></span>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
            <div class="col-md-12">
                <h4>Tabla de publicaciones: seleccionar publicaciones a enviar</h4>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered" id="table-posts">
                    <thead>
                        <tr>
                            <th>Cód. Ref.</th>  <!-- 1 -->
                            <th>Titulo</th>     <!-- 2 -->
                            <th>Estado</th>     <!-- 3 -->
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <button id="btn-send" type="button" class="btn btn-success"><?=  __('Enviar') ?></button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    var table_email_records = null;
    var email_record_selected = null;

    var table_posts = null;
    var posts_selected = [];

    $(document).ready(function () {

        $('#table-email-records').removeClass('display');
        $('#table-posts').removeClass('display');

    	table_email_records = $('#table-email-records').DataTable({
    	    "order": [[ 0, 'asc' ]],
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
    	    "autoWidth": true,
    	    "scrollY": '450px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "deferRender": true,
		    "ajax": {
                "url": "/app/email-records/index.json",
                "dataSrc": "emailRecords"
            },
            "columns": [
                { "data": "status" },
                { "data": "message" },
                { "data": "send_from" },
                { "data": "post_code" },
                { "data": "customer_name" },
                { "data": "customer_phone" },
                { "data": "customer_email" },
                { "data": "email_contact" },
                { "data": "comments" },
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "modified",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
            ],
    	    "columnDefs": [
    	        { "type": 'date-custom', targets: [8, 9] },
    	        {
                    "targets": [],
                    "visible": false,
                    "searchable": true
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
    	    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[ 50, 100, -1], [ 50, 100, "Todas"]],
    	});

    	$('#table-email-records_filter').closest('div').before($('#btns-tools').contents());

        loadPreferences(table_email_records, 'email-records-index');
    });

    $('#table-posts').removeClass('display');

	table_posts = $('#table-posts').DataTable({
	    "order": [[ 0, 'asc' ]],
	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
	    "autoWidth": true,
	    "scrollY": '450px',
	    "scrollCollapse": true,
	    "scrollX": true,
	    "deferRender": true,
	    "ajax": {
            "url": "/app/posts/email-records.json",
            "dataSrc": "posts"
        },
        "columns": [
            { "data": "code" },
            { "data": "title" },
            { 
                "data": "status",
                "render": function ( data, type, row ) {
                    return sessionPHP.paraments.post.status[data].name;
                }
            },
        ],
        "createdRow" : function( row, data, index ) {
            row.id = data.id;
        },
	    "language": {
            "decimal":        "",
            "emptyTable":     "Sin resultados.",
            "info":           "_START_ al _END_ de _TOTAL_",
            "infoEmpty":      "0 al 0 de 0",
            "infoFiltered":   "",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ filas",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar:",
            "zeroRecords":    "Sin resultados",
            "paginate": {
                "first":      "Primero",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna ascendente",
                "sortDescending": ": Activar para ordenar la columna descendente"
            }
        },
        "lengthMenu": [[ 50, 100, -1], [ 50, 100, "Todas"]],
	});

    $('#table-posts_wrapper .tools').append($('#btns-tools').contents());

    $('#table-posts tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $(this).addClass('selected');
            }
        }
    });

	$('.btn-hide-column').click(function() {
        $('.modal-hide-columns-email-records').modal('show');
    });

    $('#table-email-records_wrapper .tools').append($('#btns-tools').contents());

    $('#table-email-records').on( 'init.dt', function () {
        createModalHideColumn(table_email_records, '.modal-hide-columns-email-records');
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-email-records').tableExport({tableName: 'Correos', type:'excel', escape:'false'});
            }
        });
    });

    $('#btns-tools').show();

    $('#table-email-records tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_email_records.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            email_records_selected = table_email_records.row( this ).data();

            if (email_records_selected.status == "Nuevo") {
                $('.query-message').text("");
                $('.query-message').text(email_records_selected.message);
                $('.query-code').text("");
                var code = email_records_selected.post_code == null ? '---' : email_records_selected.post_code;
                $('.query-code').text(code);
                table_posts.ajax.reload();
                $('.modal-email-records').modal('show');
            } else {
                $('.modal-alternative').modal('show');
            }
        }
    });

    $('.column_search').on( 'keyup', function () {

        table_email_records
            .columns( $(this).data('column') )
            .search( this.value )
            .draw();

        checkStatusFilter();
    });

    $('#created-datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    }).on('dp.change', function (ev) {

        table_email_records
            .columns( 8 )
            .search( $('#input-created').val() )
            .draw();

        checkStatusFilter();
    });

    $('#modified-datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    }).on('dp.change', function (ev) {

        table_email_records
            .columns( 9 )
            .search( $('#input-modified').val() )
            .draw();

        checkStatusFilter();
    });

    $('input:checkbox.column_search').change(function() {

        table_email_records
            .columns( $(this).data('column') )
            .search( $(this).is(":checked") )
            .draw();

        checkStatusFilter();
    });

    $('select.column_search').change(function() {

        var search = this.value;

        if (jQuery.inArray($(this).data('column'), [0, 2]) !== -1) {
            if ($(this).find('option:selected').val() == "") {
                $('.column_search').val('');
                table_email_records.search( '' ).columns().search( '' ).draw();
                $('.column_search').prop('checked', false);
                checkStatusFilter();
            } else {
                search = $(this).find('option:selected').text();
            }
        }

        table_email_records
            .columns( $(this).data('column') )
            .search( search )
            .draw();

        checkStatusFilter();
    });

    $('.btn-clear-filter').click(function() {
        $('.column_search').val('');
        table_email_records.search( '' ).columns().search( '' ).draw();
        $('.column_search').prop('checked', false);
        checkStatusFilter();
    });

    $('#btn-delete').click(function() {

        var text = "¿Está Seguro que desea eliminar el registro?";
        var id  = email_records_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/email-records/delete/' + id, 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
                .find('#replacer').submit();
            }
        });
    });

    $('.btn-edit-comment').click(function() {
        var email_record_id = email_records_selected.id;
        var comment = email_records_selected.comments;

        $('input#email_record_id').val(email_record_id);
        $('textarea#comments').html(comment);
        $('.modal-alternative').modal('hide');
        $('.modal-email-records').modal('hide');
        $('#modal-edit-comments').modal('show');
    });

    $('a#btn-status').click(function() {
        $("select#status").val(email_records_selected.status);
        $('input[name="post_id"]').val(email_records_selected.id);
        $('.modal-status').modal('show');
    });

    $('#btn-send').click(function() {
        $('#table-posts tbody tr').each(function(index, value) {
            if (value.className.indexOf("selected") >= 0) {
                var data = table_posts.row( index ).data();
                posts_selected.push(data);
            }
        });

        if (posts_selected.length > 0) {

            var data = JSON.stringify({
                id: email_records_selected.id,
                customer_email: email_records_selected.customer_email,
                customer_name: email_records_selected.customer_name,
                customer_phone: email_records_selected.customer_phone,
                posts: posts_selected,
            });

            $('body')
                .append( $('<form/>').attr({'action': '/app/EmailRecords/sendEmail', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': data}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'order', 'value': null})))
                .find('#replacer').submit();

        } else {
            generateNoty('warning', 'Debe seleccionar al menos una publicación para enviar.');
        }
    });

    $('#btn-resend').click(function() {

        $('.query-message').text("");
        $('.query-message').text(email_records_selected.message);
        $('.query-code').text("");
        var code = email_records_selected.post_code == null ? '---' : email_records_selected.post_code;
        $('.query-code').text(code);
        table_posts.ajax.reload();
        $('.modal-posts').modal('show');
    });

    $('#btn-selection').click(function() {
        $('.modal-posts').modal('show');
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
