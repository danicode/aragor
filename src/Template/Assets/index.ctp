<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    .lbl-cover-title {
        font-size: 25px;
    }

</style>

<div class="row">

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-1">
                <label class="lbl-cover-title"><?= __('Imagenes') ?></label>
            </div>
            <div class="col-md-1">
                <div class="pull-right">
                    <a class="btn btn-default" title="<?= __('Nueva Imagen') ?>" href="#" role="button" id="btn-add-asset" >
                        <span class="glyphicon icon-plus" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>
        <?php if (count($assets) > 0): ?>
            <div class='row'>
                <?php foreach ($assets as $asset): ?>

                    <div class='col-md-4'>
                        <img style="max-width: 400px; height: auto; display: block;" src="/app/images/<?= $asset->url ?>"></img>
                        <div class='row'>
                            <div class='col-md-4'>
                                <a class='btn-delete-asset' data-id="<?= $asset->id ?>" href='javascript:void(0)'><?= __('Quitar') ?></a>
                            </div>
                            <div class='col-md-12'>
                                <div class="form-group link">
                                    <label for="link">Link</label>
                                    <input data-id="<?= $asset->id ?>" style="width: 70%" type="text" name="link" maxlength="255" class="link" class="form-control" value="<?= $asset->link ?>">
                                </div>
                            </div>
                            <div class='col-md-3'>
                                <div class="checkbox chck-visible" data-id="<?= $asset->id ?>">
                                    <label>
                                       <?php if ($asset->visible): ?>
                                           <input type="checkbox" checked value="1">
                                       <?php else: ?>
                                           <input type="checkbox" value="0">
                                       <?php endif; ?>
                                       <?= __('Visible') ?>
                                    </label>
                                </div>
                            </div>
                            <div class='col-md-3'>
                                <div class="checkbox chck-main-visible" data-id="<?= $asset->id ?>">
                                    <label>
                                       <?php if ($asset->main): ?>
                                           <input type="checkbox" checked value="1">
                                       <?php else: ?>
                                           <input type="checkbox" value="0">
                                       <?php endif; ?>
                                       <?= __('Principal') ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
        <?php else: ?>
            <label>Sin Imagenes</label>
        <?php endif; ?>
    </div>

</div>

<div id="assets-popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Añadir Imagen') ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
           <?= $this->Form->create(null, ['url' => ['controller' => 'Assets' , 'action' => 'add'], 'enctype' => "multipart/form-data" ]) ?>
            <fieldset>
                <?php
                    echo $this->Form->file('files[]', ['required' => true, 'multiple'=>'multiple']);
                ?>
            </fieldset>
            <br>
            <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancelar') ?></button>
            <?= $this->Form->button(__('Agregar')) ?>
            <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    $('a#btn-add-asset').click(function() {
        $('#assets-popup').modal('show');
    });

    $(document).on("click", "a.btn-delete-asset", function(e) {

        var id  = $(this).data('id');
        var text = '¿Está Seguro que desea eliminar la Imagen? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/app/assets/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
                    .find('#replacer').submit();
            }
        });
    });

    $(document).on("click", "div.chck-visible", function(e) {

        var id  = $(this).data('id');
        $('body')
            .append( $('<form/>').attr({'action': '/app/assets/visible/', 'method': 'post', 'id': 'replacer'})
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
            .find('#replacer').submit();
    });

    $(document).on("click", "div.chck-main-visible", function(e) {

        var id  = $(this).data('id');
        $('body')
            .append( $('<form/>').attr({'action': '/app/assets/main/', 'method': 'post', 'id': 'replacer'})
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
            .find('#replacer').submit();
    });

    $(".link").blur(function() {
        console.log("link: " +  $(this).val());
        console.log("id: " +  $(this).data('id'));
        $.ajax({
            url: "<?= $this->Url->build(['controller' => 'Assets', 'action' => 'changeLink']) ?>",
            type: 'POST',
            dataType: "json",
            data: JSON.stringify({ 
                link: $(this).val(),
                id: $(this).data('id')
            }),
            success: function(data) {
                if (data != false) {
                    console.log(data);
                    generateNoty('success', 'Se ha cambiado correctamente el link.');
                }
            },
            error: function () {
                generateNoty('error', 'Error al agregar el link.');
            }
        });
    });

</script>
