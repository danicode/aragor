<div class="card">
    <div class="card-header card-connections-seeker">
        <h5 class="<?= isset($class_title) ? $class_title : '' ?>" > <?= isset($title) ? $title : 'Seleccionar Conexión del Cliente' ?></h5>
    </div>
    <div class="card-block text-secondary">
        <table class="table table-bordered table-hover" id="table-connections">
            <thead>
                <tr>
                    <th><?= __('Controlador') ?></th>
                    <th><?= __('Servicio') ?></th>
                    <th><?= __('IP') ?></th>
                    <th><?= __('Domicilio') ?></th>
                    <th><?= __('Área') ?></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">

    var connection_selected = null;
    var table_connections = null;
    
    function clear_card_connecion_seeker(){
        
        connection_selected = null;
        table_connections.clear().draw();;
    }

    $(document).ready(function() {

        $('#table-connections').removeClass('display');

        table_connections = $('#table-connections').DataTable({
		    "order": [[ 0, 'desc' ]],
            "deferRender": true,
            "bFilter": false,
	        "ajax": {
                "url": "/ispbrain/connections/get_connection_customer.json",
                "dataSrc": "connections",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "customer_code":  (customer_selected) ? customer_selected.code : ''
                    });
                }
            },
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": false,
		    "columns": [
                { 
                    "data": "controller.name",
                },
                { "data": "service.name" },
                { 
                    "data": "ip",
                },
                { "data": "address" },
                { "data": "zone.name" },
            ],
		    "columnDefs": [
                { 
                    "class": "left", targets: [1,3]
                },
            ],
             "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar Conexión:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 100, -1], [50, 100, "Todas"]],
		});

        $('#table-connections tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                if ($(this).hasClass('selected')) {
                    
                     table_connections.$('tr.selected').removeClass('selected');
                     $(document).trigger('CONNECTION_UNSELECTED');
                     connection_selected = null;
                    
                } else {
                    table_connections.$('tr.selected').removeClass('selected');
                    connection_selected = table_connections.row( this ).data();
                    $(this).addClass('selected');
                    $(document).trigger('CONNECTION_SELECTED');
                    
                    
                    console.log(connection_selected);
                    
                }
            }
        });
    });
</script>
