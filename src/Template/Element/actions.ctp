<style type="text/css">

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .modal span {
       margin-right: 15px;
    }

</style>

<div class="modal fade <?=$modal?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <center>
                    
                    <?php 
                    foreach($buttons as $button){
                
                         echo $this->Html->link(
                                '<span class="glyphicon '.$button['icon'] .' text-white" aria-hidden="true"></span>'.$button['name'] .'',
                                'javascript:void(0)',
                                ['id' => $button['id'], 'class' => 'btn '. $button['type']  , 'escape' => false]
                            );
                       }
                   ?>
                </center>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>


