
<style type="text/css">
    
    .header-title{
        text-align: center;
        color: #696868; 
        font-size: 19px;
        background-color: #bfbfbf; 
        margin: 0 0 10px 0; 
        border-radius: 0 0 5px 5px;
        font-weight: bold;
    }
    
    
    /*.info-License{*/
    /*    font-size: 12px;*/
    /*    background-color: white;*/
    /*    color: #f05f40;*/
    /*    padding: 0 5px;*/
    /*    margin-left: 20px;*/
    /*}*/
    
    /*.info-License-due{*/
    /*    font-size: 12px;*/
    /*    background-color: white;*/
    /*    color: red;*/
    /*    padding: 0 5px;*/
    /*    margin-left: 20px;*/
    /*}*/

   
    
    

</style>



<div class="row header-title">
    <div class="col-1 pl-1 pt-1 pb-1 pr-0">
         <?php
            echo $this->Html->link(
                '<span class="glyphicon icon-arrow-left"  aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-back-page',
                    'class' => 'btn btn-default btn-back-page',
                    'escape' => false
                ]);
              
        ?>
    </div>
    <div class="col-9 pl-0 pr-0 mr-0 ">
        <?=$this->request->session()->read('titles')[$this->request->controller][$this->request->action] ?>
    </div>
    <div class="col-1 ml-0 pl-0 pr-1 pt-1 pb-1">
        <?php
            echo $this->Html->link(
                '<span class="glyphicon icon-profile"  aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-userlogin',
                    'class' => 'btn btn-default btn-userlogin',
                    'escape' => false
                ]);
        ?>
    </div>
</div>

<div class="modal fade modal-userlogin" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Sesión de Usuario</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
              <table style="width:100%">
                <tr>
                  <td style="width:20%"><label class="control-label" for="">Usuario:&nbsp;&nbsp;</label></td>
                  <td style="width:80%"><input type="text" class=" form-control" value="<?=$username?>"readonly></td>
                </tr>
              </table>
              <br>
              <legend class="sub-title-sm"><?=  __('Licencia del sistema') ?></legend>
              <table style="width:100%">
                <tr>
                  <td style="width:20%"><label class="control-label" for="">Limite de clientes:&nbsp;&nbsp;</label></td>
                  <td style="width:80%"><input type="text" class=" form-control" value="<?=$license['amount_customers']?>"readonly></td>
                </tr>
                <?php if($license['duedate'] > $license['now']):?>
                 <tr>
                  <td style="width:60%"><label class="control-label" for="">Próximo Vencimiento:&nbsp;&nbsp;</label></td>
                  <td style="width:40%"><input type="text" class=" form-control" value="<?=  ($license['duedate']) ? $license['duedate']->format('d/m/Y') : '' ?>" readonly></td>
                 </tr>
                <?php else:?>
                 <tr>
                  <td style="width:60%; color:red;"><label class="control-label" for="">Factura Vencida:&nbsp;&nbsp;</label></td>
                  <td style="width:40%; color:red;"><input type="text" class=" form-control" value="<?= ($license['duedate']) ? $license['duedate']->format('d/m/Y') : '' ?>"readonly></td>
                 </tr>
                <?php endif;?>
               
              </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">

    var version = <?=json_encode($version)?>;
    
    $(document).ready(function (){
        
        $('.btn-back-page').click(function(){
    		parent.history.back();
    		return false;
    	});
    	
    	$('.btn-userlogin').click(function(){
    	  $('.modal-userlogin').modal('show');
    	});
        
        
      
        var text = $('#head-title').val();
        var desc = 'ISBRAIN: Software de Gestion para ISP | ' + version;
        document.title = text  + ' | ' + desc;

        $('.btn-userlogin').click(function(){
            $('.modal-userlogin').modal('show');
        });
            
    });
    
</script>