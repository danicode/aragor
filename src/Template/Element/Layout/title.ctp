
<style type="text/css">
    
    .header-title{
        text-align: center;
        color: #696868; 
        font-size: 24px;
        background-color: #bfbfbf; 
        margin: 0 0 10px 0; 
        border-radius: 0 0 5px 5px;
        font-weight: bold;
    }
    
    
    /*.info-License{*/
    /*    font-size: 12px;*/
    /*    background-color: white;*/
    /*    color: #f05f40;*/
    /*    padding: 0 5px;*/
    /*    margin-left: 20px;*/
    /*}*/
    
    /*.info-License-due{*/
    /*    font-size: 12px;*/
    /*    background-color: white;*/
    /*    color: red;*/
    /*    padding: 0 5px;*/
    /*    margin-left: 20px;*/
    /*}*/

   
    
    

</style>



<div class="row header-title">
    <div class="col-1 pl-1 pt-1 pb-1">
         <?php
            echo $this->Html->link(
                '<span class="glyphicon icon-arrow-left"  aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-back-page',
                    'class' => 'btn btn-default btn-back-page float-left',
                    'escape' => false
                ]);
              
        ?>
    </div>
    <div class="col">
        <?=$this->request->session()->read('titles')[$this->request->controller][$this->request->action] ?>
    </div>
    <div class="col-1 pr-1 pt-1 pb-1 invisible">
        <?php
            echo $this->Html->link(
                '<span class="glyphicon icon-profile"  aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-userlogin',
                    'class' => 'btn btn-default btn-userlogin float-right',
                    'escape' => false
                ]);
        ?>
    </div>
</div>

<div class="modal fade modal-userlogin" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="gridSystemModalLabel">Sesión de Usuario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
              <table style="width:100%">
                <tr>
                  <td style="width:20%"><label class="control-label" for="">Usuario:&nbsp;&nbsp;</label></td>
                  <td style="width:80%"><input type="text" class=" form-control" value="<?=$username?>"readonly></td>
                </tr>
              </table>
              <br>
              <legend class="sub-title-sm"><?=  __('Licencia del sistema') ?></legend>
              <table style="width:100%">
                <tr>
                  <td style="width:20%"><label class="control-label" for="">Limite de clientes:&nbsp;&nbsp;</label></td>
                  <td style="width:80%"><input type="text" class=" form-control" value="<?=$license['amount_customers']?>"readonly></td>
                </tr>
                <?php if($license['duedate'] > $license['now']):?>
                 <tr>
                  <td style="width:60%"><label class="control-label" for="">Próximo Vencimiento:&nbsp;&nbsp;</label></td>
                  <td style="width:40%"><input type="text" class=" form-control" value="<?=  ($license['duedate']) ? $license['duedate']->format('d/m/Y') : '' ?>" readonly></td>
                 </tr>
                <?php else:?>
                 <tr>
                  <td style="width:60%; color:red;"><label class="control-label" for="">Factura Vencida:&nbsp;&nbsp;</label></td>
                  <td style="width:40%; color:red;"><input type="text" class=" form-control" value="<?= ($license['duedate']) ? $license['duedate']->format('d/m/Y') : '' ?>"readonly></td>
                 </tr>
                <?php endif;?>
               
              </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">

    var version = <?=json_encode($version)?>;
    
    // function back1(url) {
    //     if (parent.history.length > 2 || document.referrer.length > 0) {
    //         // go back:
    //         parent.history.back();
    //     } else if (url) {
    //         // go to specified fallback url:
    //         parent.history.replaceState(null, null, url);
    //     }
    // }
    
    // function back2(){
        
    //     var currentUrl = window.location.href;
    //     parent.history.back();
    //     setTimeout(function(){
    //         // if location was not changed in 100 ms, then there is no history back
    //         if(currentUrl === window.location.href){
    //             // redirect to site root
    //             window.location.href = '/ispbrain';
    //         }
    //     }, 100);
            
    // }
    
    
     var controller = <?=json_encode($this->request->controller)?>;
     var action = <?=json_encode($this->request->action)?>;
    

    $(document).ready(function (){
        
        var index = 1;
        
        if(document.referrer == window.location.href){
            index+=1;
        }
        
        $('.btn-back-page').click(function(){
            
            parent.history.go(-1 * index);
    		return false;
    	});
    	
    	$('.btn-userlogin').click(function(){
    // 	  $('.modal-userlogin').modal('show');
    	});
        
      
        var text = sessionPHP.titles[controller][action];
        var desc = 'ISBRAIN: Software de Gestion para ISP | ' + version;
        document.title = text  + ' | ' + desc;

    });
    
</script>