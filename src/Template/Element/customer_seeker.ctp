

<style type="text/css">
    
  
    #table-customers_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
    .input-group{
        
    }
    
</style>

<div class="modal fade" id="modal-customer-seeker" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <div class="row">
              <div class="col-xl-12">
                   <table class="table table-bordered table-hover" id="table-customers">
                        <thead>
                            <tr>
                                <th >Código</th>
                                <th >Nombre</th>
                                <th >Documento</th>
                                <th >Domicilio</th>
                                <th >Área</th>
                                <th >Teléfono</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
              </div>
          </div>
     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btn-selected">Seleccionar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="card border-secondary mb-3" id="card-customer-seeker">
    
   
    <div class="card-header w-100">
         <div class="row">
            <div class="col-10">
                <h5 class="<?= isset($class_title) ? $class_title : '' ?>"> <?= isset($title) ? $title : '1. Buscar Cliente' ?></h5>
            </div>
            <div class="col-2">
                <?php
                
                 echo $this->Html->link(
                        '<span class="glyphicon icon-bin" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Limpiar fomulario',
                        'class' => 'btn btn-default clear-from-customer',
                        'escape' => false
                    ]);
                
                ?>
            </div>
        </div>
    </div>
    
    
    <div class="card-body text-secondary ">
        
         <table style="width:100%">
             <tr  id="input-searching-code-customer">
                <td style="width:33%"><label class="control-label" for="code">Código</label></td>
                <td colspan="2" style="width:67%">
                    <?php echo $this->Form->input('customer_code', ['label' => false, 'autofocus' => true, 'autocomplete' => 'off', 'required' => false]); ?>
                </td>
            </tr>
            <tr>
                <td style="width:33%"><label class="control-label" for="code">Documento</label></td>
                <td style="width:28%">
                    <?php echo $this->Form->input('customer_doc_type', ['options' => $doc_types, 'label' => false]); ?>
                </td>
                <td style="width:39%">
                      <?php echo $this->Form->input('customer_ident', ['label' => false,'autocomplete' => 'off', 'required' => false]); ?>
                </td>
            </tr>
        </table>
    </div>
     <ul class="list-group list-group-flush">
        <li class="list-group-item">
            <?php if(isset($listCustomers) && $listCustomers):?>
             <button type="button" class="btn btn-secondary btn-customer-list float-left">Lista de Clientes</button>
            <?php endif;?>
            
            <button type="button" class="btn btn-success float-right btn-search">Buscar</button>
        </li>
        <li class="list-group-item">
            <div class="input-group">
              <span class="input-group-addon" >Código</span>
              <input type="text" class="form-control text-right" name="customer_code_seleted" id="customer_code_seleted" readonly="true" value="">
            </div>
            <div class="input-group">
              <span class="input-group-addon">Doc.</span>
              <input type="text" class="form-control text-right" name="customer_doc_seleted" id="customer_doc_seleted" readonly="true" value="" >
            </div>
            <div class="input-group">
              <span class="input-group-addon">Nombre</span>
              <input type="text" class="form-control text-right" name="customer_name_seleted" id="customer_name_seleted" readonly="true" value="" >
            </div>
            
           <?php if (isset($showDebtMonth) && $showDebtMonth):?>
            
            <div class="input-group">
              <span class="input-group-addon">Deuda Mes</span>
              <input type="text" class="form-control text-right font-weight-bold" name="customer_debt_month_seleted" id="customer_debt_month_seleted" readonly="true" value="" >
            </div>
            
            <?php endif; ?>
        </li>
        
        <?php
   
        if ($showbtn):?>
            <li class="list-group-item">
                  <button type="button" class="btn btn-secondary btn-go-customer float-left" id="Customers-view">Mas Info</button>
                  <button type="button" class="btn btn-secondary btn-print-resume float-right" id="Customers-printresume">Resumen</button>
            </li>
        <?php endif;?>
        
        
      </ul>
   
</div>





<script type="text/javascript">

    var table_customers = null;
    var customer_selected = null;
    
    function clear_card_customer_seeker(){
        
        // customer_selected = null;
        
        $('#card-customer-seeker #customer-code').val('');
        $('#card-customer-seeker #customer-doc-type').val(96);
        $('#card-customer-seeker #customer-ident').val('');
        $('#card-customer-seeker #customer_code_seleted').val('');
        $('#card-customer-seeker #customer_doc_seleted').val('');
        $('#card-customer-seeker #customer_name_seleted').val('');
        $('#card-customer-seeker #customer_debt_month_seleted').val('');
        
    }
    

    $(document).ready(function(){
        
        if(!sessionPHP.paraments.customer.searching_code){
            $('#input-searching-code-customer').hide();
        }
        
        $('#customer-code').keypress(function(e) {
            if(e.which == 13) {
                $('#card-customer-seeker .btn-search').click();
            }
        });
        
        $('#customer-ident').keypress(function(e) {
            if(e.which == 13) {
                $('#card-customer-seeker .btn-search').click();
            }
        });
        
        $('#card-customer-seeker .btn-search').click(function(){
            
            var customer = {
                code: '',
                doc_type: '',
                ident: ''
            };
            
            
            if($('#customer-code').val().length > 0 || $('#customer-ident').val().length > 0){
               
                customer.code = $('#customer-code').val().trim();
                customer.doc_type = $('#customer-doc-type').val();
                customer.ident = $('#customer-ident').val().replace("-", "").replace("-", "").trim();
                
                $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'Customers', 'action' => 'geCustomerAjax'])?>",
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify(customer),
                    success: function(data){
                        
                        var doc_type_name = sessionPHP.afip_codes.doc_types[data.customer.doc_type];
                      
                        if(!data.customer){
                            generateNoty('warning', 'No se pudo encontrar el cliente. Verifique los datos de busqueda.');
                        }else{
                            $('#card-customer-seeker #customer_code_seleted').val(pad(data.customer.code,5));
                            $('#card-customer-seeker #customer_doc_seleted').val(doc_type_name +' '+ data.customer.ident);
                            $('#card-customer-seeker #customer_name_seleted').val(data.customer.name.toUpperCase());
                            $('#card-customer-seeker #customer_debt_month_seleted').val(data.customer.debt_month.toFixed(2));
                            
                            
                            customer_selected = data.customer;
                            
                            $('#card-customer-seeker').trigger('CUSTOMER_SELECTED');
                        }
                        
                    },
                    error: function () {
                        generateNoty('error', 'Error al buscar el cliente.');
                    }
                });
            }
            else{
                generateNoty('warning', 'Debe ingresar el código o documento del cliente.');
            }
            
        });
        
        $('#table-customers').removeClass('display');
		
	    table_customers = $('#table-customers').DataTable({
		    "order": [[ 0, 'desc' ]],

            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/customers/get_customers.json",
                "dataSrc": "customers"
            },
     
		    "autoWidth": true,
		    "scrollY": '380px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    
		    "columns": [
                { 
                    "data": "code",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { "data": "name" },
                { 
                    "data": "ident",
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[row.doc_type] +' '+ data;
                    }
                },
                { "data": "address" },
                { 
                    "data": "zone",
                    "render": function ( data, type, row ) {
                        return data ? data.name : '';
                    }
                },
                { "data": "phone" },
            ],
		    "columnDefs": [
                { 
                    "class": "left", targets: [1,3]
                },
            ],
             "createdRow" : function( row, data, index ) {
                row.id = data.code;
            },
	
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar Cliente:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[500], [500]],
            "dom":
    	    	"<'row'<'col-md-6 title'><'col-md-6'f>>" +
        		"<'row'<'col-md-12'tr>>" +
        		"<'row'<'col-md-5'i><'col-md-7'p>>",
		});
		
		$('#table-customers_wrapper .title').append('<h5>Lista de Clientes</h5>');
		
		$('.btn-customer-list').click(function(){
            $("#modal-customer-seeker").modal('show');
        });
		
		$("#modal-customer-seeker").on('shown.bs.modal', function (e) {
            table_customers.draw();
        });
        
        $('#table-customers tbody').on( 'click', 'tr', function (e) {
            
            if(!$(this).find('.dataTables_empty').length){
            
                table_customers.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                
                customer_selected =  table_customers.row( this ).data() ;
            
            }
        });
        
        $('#modal-customer-seeker #btn-selected').click(function() {
        
            if(customer_selected){
                
                clear_card_customer_seeker();
                
                var doc_type_name = sessionPHP.afip_codes.doc_types[customer_selected.doc_type];
                
                $('#card-customer-seeker #customer_code_seleted').val(pad(customer_selected.code,5));
                $('#card-customer-seeker #customer_doc_seleted').val(doc_type_name +' '+ customer_selected.ident);
                $('#card-customer-seeker #customer_name_seleted').val(customer_selected.name.toUpperCase());
                $('#card-customer-seeker #customer_debt_month_seleted').val(customer_selected.debt_month.toFixed(2));
              
                $('#modal-customer-seeker').modal('hide');
                
                $('#card-customer-seeker').trigger('CUSTOMER_SELECTED');
                
                
            }else{
                generateNoty('warning', 'Debe seleccionar un cliente.');
            }
        });
    
        $('.btn-print-resume').click(function(){
             
             if(customer_selected){
                 var url = "/ispbrain/customers/printresume/" + customer_selected.code;
                window.open(url, 'Resumen', "width=500, height=300");
             }else{
                generateNoty('warning', 'Debe seleccionar un cliente.');
             }
        });
        
        $('.btn-go-customer').click(function(){
          
            if(customer_selected){
                var url = "/ispbrain/customers/view/" + customer_selected.code;
                 window.open(url, '_blank');
            }else{
                generateNoty('warning', 'Debe seleccionar un cliente.');
            }
        });
        
        $('.clear-from-customer').click(function(){
          
          clear_card_customer_seeker();
          
           $('#card-customer-seeker').trigger('CUSTOMER_CLEAR');
           
        });
            
        
        
    });
    
       
    
    
</script>