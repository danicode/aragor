

<div class="modal fade edit-debt-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Editar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                
                <form id="from-edit-debt">
                   
                    <fieldset>
            
                        <div class="row">
                            
                            <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                                <?php echo $this->Form->input('quantity', ['label' => 'Cantidad', 'readonly' => true]); ?>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                                <?php echo $this->Form->input('price', ['label' => 'Precio', 'readonly' => true]); ?>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                                <?php echo $this->Form->input('sum_price', ['label' => 'Subtotal', 'readonly' => true]); ?>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                                <?php echo $this->Form->input('tax', ['label' => 'Alícuota', 'readonly' => true]); ?>
                            </div>
                             <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                                <?php echo $this->Form->input('sum_tax', ['label' => 'Impuesto', 'readonly' => true]); ?>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                                <?php 
                                    echo $this->Form->input('total', ['label' => 'Total', 'readonly' => true]);
                                    echo $this->Form->input('debt_id', ['id' => 'debt_id', 'type' => 'hidden']);
                                ?>
                            </div>
                            
                            <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 d-none">
                                <?php echo $this->Form->input('created', ['readonly' => true, 'type' => 'date', 'type' => 'text']); ?>
                            </div>
                            
                             <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <?php echo $this->Form->input('description', ['label' => 'Description', 'rows' => "3", 'readonly' => true]); ?>
                            </div>
                            
                            <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                                <div class='form-group' >
                                    <label class="control-label" for="">Vencimiento</label>
                                    <div class='input-group date' id='duedate-datetimepicker'>
                                        <input name="duedate" id="duedate"  type='text' class="form-control is-valid" required />
                                        <span class="input-group-addon mr-0">
                                            <span class="glyphicon icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                                
                                <?php 
                                
                                $types = [];
                                
                                    switch ($paraments->invoicing->company->responsible) {
                                        case '1':
                                            $types = ['XXX' => 'PRESU X', '001' => 'FACTURA A', '006' => 'FACTURA B'];
                                            break;
                                            
                                        case '6':
                                            $types = ['XXX' => 'PRESU X', '011' => 'FACTURA C'];
                                            break;
                                        
                                        default:
                                            $types = ['XXX' => 'PRESU X', '011' => 'FACTURA C'];
                                            break;
                                    }
                                
                                    echo $this->Form->input('tipo_comp', [
                                        'label' => 'Destino',
                                        'class' => 'is-valid',
                                        'options' => $types
                                        ]);
                                ?>
                            </div>
                            
                            
                          
                        </div>
                           
                    </fieldset>
                    
                	<button type="button" class="btn btn-default mt-2" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success mt-2 float-right">Guardar</button>
                </form>     
                  
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
     var debt_selected = null;
    
    $(document).ready(function(){
        
        $('.edit-debt-modal #duedate-datetimepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });

           
        $('#btn-edit-debt').click(function(){
           
           if(!table_debts){
               generateNoty('error', 'Error no exite la tabla de deudas');
           }
           else{
               
               if(debt_selected){
                
                    $('.edit-debt-modal #debt_id').val(debt_selected.id);
                    
                    var created = debt_selected.created.split('T')[0].split('-');
                    created = created[2]+'/'+created[1]+'/'+created[0];
                    
                    $('.edit-debt-modal #created').val(created);
                    var duedate = debt_selected.duedate.split('T')[0].split('-');
                     duedate = duedate[2]+'/'+duedate[1]+'/'+duedate[0];
                    
             
                    $('.edit-debt-modal #quantity').val(debt_selected.quantity);
                    $('.edit-debt-modal #price').val(debt_selected.price.toFixed(2));
                    $('.edit-debt-modal #sum-price').val(debt_selected.sum_price.toFixed(2));
                    $('.edit-debt-modal #tax').val(sessionPHP.afip_codes.alicuotas_types[debt_selected.tax]);
                    $('.edit-debt-modal #sum-tax').val(debt_selected.sum_tax.toFixed(2));
                    $('.edit-debt-modal #total').val(debt_selected.total.toFixed(2));
                    
                    
                    $('.edit-debt-modal #duedate').val(duedate);
                    $('.edit-debt-modal #tipo-comp').val(debt_selected.tipo_comp);
                    $('.edit-debt-modal #description').val(debt_selected.description);
                   
                }
                
                $('.moda').modal('hide');
                $('.edit-debt-modal').modal('show');
           }
        
        });
        
        $('#from-edit-debt').submit(function(event){
           
           event.preventDefault();
           
            var send_data = {};  
            
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val){
                send_data[val.name] = val.value;
            });
            
            $.ajax({
                url: "<?=$this->Url->build(['controller' => 'debts', 'action' => 'editAjax'])?>" ,
                type: 'POST',
                dataType: "json",
                data: JSON.stringify(send_data),
                success: function(data){
                    
                    if(data.debt){
                        
                        debt_selected = data.debt ;
                        
                        table_debts.row( $('#table-debts tr#'+data.debt.id) ).data( debt_selected ).draw();
                        $('.modal').modal('hide');
                        
                         $('.edit-debt-modal').trigger('EDIT_DEBT_COMPLETED');
                        
                    }
                },
                error: function () {

                    generateNoty('error', 'Error al intentar editar la deuda');
                }
            });
            
        });
        
        $('.edit-debt-modal #from-edit-debt').keypress(function( event ) {
          if ( event.which == 13 ) {
             event.preventDefault();
             return false;
          }
        });
        
    });
    
    
</script>