<style type="text/css">

    #table-debts td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 0px 0px 0px !important;
        vertical-align: middle;
    }

    #table-debts {
        width: 100% !important;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

    #table-debts-without td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 0px 0px 0px !important;
        vertical-align: middle;
    }

    #table-debts-without {
        width: 100% !important;
    }

    .txt-letter {
        text-transform: uppercase;
    }

</style>

<div class="col-md-4" style="margin-top: 50px;">

    <div class="panel panel-default">
        <div class="panel-heading panel-debts-import"><?= __('3. Importar Deudas') ?></div>
        <div class="panel-body">
            <?= $this->Form->create(null, ['url' => ['controller' => 'importers' , 'action' => 'debtsPreview'], 'name' => 'debts-form', 'type' => 'file','role'=>'form']) ?>
                <div class="form-group">
                    <label class="sr-only" for="csv"><?= __('CSV') ?></label>
                    <?php echo $this->Form->input('csv', ['type'=>'file', 'accept' => '.csv', 'class' => 'form-control', 'label' => false, 'placeholder' => __('csv upload'), 'required' => true]); ?>
                </div>
                <legend><?= __('Letra de Columna') ?></legend>
                <?php $count = 0; ?>
                <?php foreach ($debtImport as $key => $item): ?>
                    <?php $placeholder = 'letra'; ?>
                    <?php if ($key == 'additional'): ?>
                        <?php $placeholder = 'letra, letra'; ?>
                    <?php endif; ?>
                    <?php if ($key != 'csv' && $key != 'comprobantes'): ?>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <?php echo $this->Form->input($key, ['label' => __($key), 'placeholder' => __($placeholder), 'type' => 'text', 'class' => 'txt-letter', 'value' => $debtImport[$key]]); ?>
                            <?php $count++; ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
                <legend><?= __('Valores por defecto') ?></legend>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <?php echo $this->Form->input('comprobantes', ['options' => $comprobantes, 'label' => __('comprobantes'), 'value' => 'XXX']); ?>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-default btn-import"><?= __('Vista Previa') ?></button>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?= $this->element('Importers/debt_information') ?>

<div class="col-md-12" style="margin-top: 30px;">

    <?php if (count($debts_data) > 0): ?>
        <legend><?= __('Tabla Deudas con Datos Completos') ?></legend>
        <div id="btns-tools-debts">
            <div class="pull-right btns-tools margin-bottom-5" >
                <button id="btn-export-debts" type="button" class="btn btn-default btn-export" title="<?= __('Exportar en formato excel el contenido de la tabla') ?>" >
                    <span class="glyphicon icon-file-excel" aria-hidden="true" ></span>
                </button>
            </div>
            <div class="pull-right btns-tools margin-bottom-5" >
                <button id="btn-import-debts" type="button" class="btn btn-default btn-import" title="<?= __('Importar datos de la Tabla') ?>" >
                    <span class="glyphicon glyphicon-upload" aria-hidden="true" ></span>
                </button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover" id="table-debts" >
                    <thead>
                        <tr>
                            <th><?= __('#') ?></th>
                            <th><?= __('code') ?></th>
                            <th><?= __('name') ?></th>
                            <th><?= __('doc_type') ?></th>
                            <th><?= __('ident') ?></th>
                            <th><?= __('plan_ask') ?></th>
                            <th><?= __('address') ?></th>
                            <th><?= __('zone_id') ?></th>
                            <th><?= __('phone') ?></th>
                            <th><?= __('email') ?></th>
                            <th><?= __('responsible') ?></th>
                            <th><?= __('tipo_comp') ?></th>
                            <th><?= __('seller') ?></th>
                            <th><?= __('status') ?></th>
                            <th><?= __('comments') ?></th>
                            <th><?= __('is_presupuesto') ?></th>
                            <th><?= __('clave_portal') ?></th>
                            <th><?= __('error') ?></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php foreach ($debts_data as $key => $debt_data): ?>
                        <tr data-id="<?= ++$key ?>">
                            <td><?= $key ?></td>
                            <td><?= $debt_data['code'] ?></td>
                            <td><?= h($debt_data['name']) ?></td>
                            <td><?= h($debt_data['doc_type']) ?></td>
                            <td><?= h($debt_data['ident']) ?></td>
                            <td><?= h($debt_data['plan_ask']) ?></td>
                            <td><?= h($debt_data['address']) ?></td>
                            <td><?= h($debt_data['zone_id']) ?></td>
                            <td><?= h($debt_data['phone']) ?></td>
                            <td><?= h($debt_data['email']) ?></td>
                            <td><?= h($debt_data['responsible']) ?></td>
                            <td><?= h($debt_data['tipo_comp']) ?></td>
                            <td><?= h($debt_data['seller']) ?></td>
                            <td><?= h($debt_data['status']) ?></td>
                            <td><?= h($debt_data['comments']) ?></td>
                            <td><?= h($debt_data['is_presupuesto']) ?></td>
                            <td><?= h($debt_data['clave_portal']) ?></td>
                            <td><?= h($debt_data['error']) ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?>
</div>

<div class="col-md-12" style="margin-top: 30px; margin-bottom: 50px;">

    <?php if (count($debts_without_data) > 0): ?>
        <legend><?= __('Tabla Deudas con Datos Incompletos') ?></legend>
        <div id="btns-tools-debts-without">
            <div class="pull-right btns-tools margin-bottom-5" >
                <button id="btn-export-debts-without" type="button" class="btn btn-default btn-export" title="<?= __('Exportar en formato excel el contenido de la tabla') ?>" >
                    <span class="glyphicon icon-file-excel"  aria-hidden="true" ></span>
                </button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover" id="table-debts-without" >
                    <thead>
                        <tr>
                            <th><?= __('#') ?></th>
                            <th><?= __('code') ?></th>
                            <th><?= __('name') ?></th>
                            <th><?= __('doc_type') ?></th>
                            <th><?= __('ident') ?></th>
                            <th><?= __('plan_ask') ?></th>
                            <th><?= __('address') ?></th>
                            <th><?= __('zone_id') ?></th>
                            <th><?= __('phone') ?></th>
                            <th><?= __('email') ?></th>
                            <th><?= __('responsible') ?></th>
                            <th><?= __('tipo_comp') ?></th>
                            <th><?= __('seller') ?></th>
                            <th><?= __('status') ?></th>
                            <th><?= __('comments') ?></th>
                            <th><?= __('is_presupuesto') ?></th>
                            <th><?= __('clave_portal') ?></th>
                            <th><?= __('error') ?></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php foreach ($debts_without_data as $key => $debt_without_data): ?>
                        <tr data-id="<?= ++$key ?>">
                            <td><?= $key ?></td>
                            <td><?= $debt_without_data['code'] ?></td>
                            <td><?= h($debt_without_data['name']) ?></td>
                            <td><?= h($debt_without_data['doc_type']) ?></td>
                            <td><?= h($debt_without_data['ident']) ?></td>
                            <td><?= h($debt_without_data['plan_ask']) ?></td>
                            <td><?= h($debt_without_data['address']) ?></td>
                            <td><?= h($debt_without_data['zone_id']) ?></td>
                            <td><?= h($debt_without_data['phone']) ?></td>
                            <td><?= h($debt_without_data['email']) ?></td>
                            <td><?= h($debt_without_data['responsible']) ?></td>
                            <td><?= h($debt_without_data['tipo_comp']) ?></td>
                            <td><?= h($debt_without_data['seller']) ?></td>
                            <td><?= h($debt_without_data['status']) ?></td>
                            <td><?= h($debt_without_data['comments']) ?></td>
                            <td><?= h($debt_without_data['is_presupuesto']) ?></td>
                            <td><?= h($debt_without_data['clave_portal']) ?></td>
                            <td><?= h($debt_without_data['error']) ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?>
</div>

<script type="text/javascript">

   var table_debts = null;
   var table_debts_without = null;

   $(document).ready(function () {

    $('#table-debts').removeClass('display');
    $('#table-debts-without').removeClass('display');

		table_debts = $('#table-debts').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cangando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 1000, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});

		$('#table-debts_filter').closest('div').before($('#btns-tools-debts').contents());

    $('#table-debts-without').removeClass('display');

		table_debts_without = $('#table-debts-without').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cangando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 1000, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});

		$('#table-debts-without_filter').closest('div').before($('#btns-tools-debts-without').contents());
    });

    $('#table-debts-without tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_debts_without.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            $('.modal-actions').modal('show');
        }
    });

    $("#btn-export-debts").click(function() {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-debts').tableExport({tableName: 'Deudas Datos Completos', type:'excel', escape:'false'});
            }
        });
    });

    $("#btn-export-debts-without").click(function() {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-debts-without').tableExport({tableName: 'Deudas Datos Incompletos', type:'excel', escape:'false'});
            }
        });
    });

    $("#btn-import-debts").click(function() {
        var text = '¿Está Seguro que desea importar la Tabla de Deudas?';
        var controller = 'Importers';
        var id = table_debts.$('tr.selected').data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/debtsImport/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
                .find('#replacer').submit();
            }
        });
    });

</script>
