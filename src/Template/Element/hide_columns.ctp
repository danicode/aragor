

<div class="modal fade  <?=$modal?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                
                <div class="row w-100">
                    <div class="col-xl-8">
                          <h5 class="modal-title float-left" id="exampleModalLabel">Columnas</h5>
                    </div>
                    <div class="col-xl-3">
                         <?php
                
                         echo $this->Html->link(
                            '<span class="glyphicon icon-floppy-disk" aria-hidden="true"></span> Guardar',
                            'javascript:void(0)',
                            [
                                'id' => 'btn-save-hidden-column-selected',
                                'title' => '',
                                'class' => 'btn btn-default float-right',
                                'escape' => false
                            ]);
                        
                        ?>
                    </div>
                    <div class="col-xl-1">
                         <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
               
            </div>
            <div class="modal-body pt-0">
              
                <div class="row checkbox-container">
               
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    
    var table_selected = null;
    var index_selected = '';

    
    
    $(document).ready(function(){
        
       // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });
    });
    
    $('#btn-save-hidden-column-selected').click(function(){
        savePreferences(table_selected, index_selected );
    });
    
    /* parameters: 
     * table: objeto tabla de datatable
     * index: indice para ubicar lo que se guarda, como ayuda se puede usar la seccion
     */
    function loadPreferences(table, index) {

        table_selected = table;
        index_selected = index;

        if (typeof(Storage) !== "undefined") {
            // Code for localStorage/sessionStorage.

            var preferences = JSON.parse(localStorage.getItem(index + '-' + sessionPHP.Auth.User.id));

            if (preferences !== null) {
                
                $.each(preferences, function(i, val) {

                    var column = table.column(i);
                    column.visible( val );
                    table.columns.adjust().draw();

                });
            }else{
                console.log('preferences no saved');
            }

        } else {
            // Sorry! No Web Storage support..
             generateNoty('information', 'Sorry! No Web Storage support');
        }

    }

    /* parameters: 
     * table: objeto tabla de datatable
     * index: indice para ubicar lo que se guarda, como ayuda se puede usar la seccion
     */
    function savePreferences(table, index) {

        if (typeof(Storage) !== "undefined") {
            // Code for localStorage/sessionStorage.

            var preferences = {};

            $.each(table.columns()[0], function(i, val) {
                
                console.log(val);
                
                var column = table.column(i);
                preferences[i] = column.visible();
            });

            if (Object.keys(preferences).length > 0) {
                var item = index + '-' + sessionPHP.Auth.User.id;
                
                console.log(item);
                
                localStorage.setItem(item, JSON.stringify(preferences));
            }
            
            generateNoty('information', 'Selección guardada.');

        } else {
            // Sorry! No Web Storage support..
        }

        // $('.modal-hide-columns-customers').modal('hide');
    }
    
    
    
    
</script>