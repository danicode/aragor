<div class="col-md-12 mt-3">
    <?php foreach ($banners_lateral as $banner): ?>
        <div class="text-center mt-3 mb-3" style="height: auto; width: 100%; position: relative; display: block; vertical-align: middle;">
            <a target="_blank" href="<?= $banner->link ?>"><img class="img-fluid" style="width: 100%;" src="/app/images/<?= $banner->url ?>" alt=""></a>
        </div>
    <?php endforeach; ?>
</div>
