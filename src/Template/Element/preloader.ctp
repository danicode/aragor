<style type="text/css">

    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
    	position: fixed;
    	left: 0px;
    	top: 0px;
    	width: 100%;
    	height: 100%;
    	z-index: 9999;
    	background: url(/app/img/simple-pre-loader/loader-64x/Preloader_3.gif) center no-repeat #fff;
    }

</style>

<div class="se-pre-con"></div>

<script type="text/javascript">

    window.onload = function(e) {

      $(".se-pre-con").fadeOut(800);

    }

</script>
