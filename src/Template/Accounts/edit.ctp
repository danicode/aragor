<style type="text/css">

  .lbl-title-search {
    font-size: larger;
    font-family: 'Work Sans', sans-serif;
    color: white;
  }

  .search-form-container {
    background: #444242;
    margin-top: 15px;
    margin-bottom: 15px;
    border-radius: 8px;
  }

  .search-form {
    margin-top: 15px;
    margin-bottom: 20px;
  }

  .lbl-range-price {
    color: white;
    font-family: 'Work Sans', sans-serif;
  }

  .post-card {
    height: 540px;
    text-align: left;
    cursor: pointer;
  }

  .post-properties {
    list-style: none;
    display: inline;
    position: relative;
    margin-top: 0;
    padding-top: 0;
  }

  .lbl-money {
    font-weight: bolder;
    font-size: 30px;
    font-family: 'Work Sans', sans-serif;
  }

  .post-body {
    background: #fff;
    font-family: 'Work Sans', sans-serif;
    color: black;
    padding-top: 0;
    padding-bottom: 0;
    position: relative;
    display: block;
    height: 60%;
  }

  .symbol-price {
    font-size: 20px;
  }

  figcaption {
    position: absolute;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 100%;
    padding: 7px;
    top: 0;
    color: #FFF;
    background: #000000;
    background: rgba(0, 0, 0, .6);
    text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.5);
    font-family: 'Work Sans', sans-serif;
  }

  .figure-img {
    margin-bottom: 0;
  }

  .cinta {
    background-image: url(/app/images/logo_status.png);
    background-repeat: no-repeat;
    background-size: auto 65px;
    height: 76px;
    width: 28px;
    position: relative;
    float: right;
    display: block;
    bottom: 80px;
  }

  .lbl-cinta {
    position: relative;
    font-size: 13px;
    color: white;
    margin-left: 10px;
    vertical-align: middle;
    font-family: 'Work Sans', sans-serif;
  }
  
  .status-container {
    position: absolute;
    display: block;
    background: #FF5722;
    height: 20px;
    z-index: 1;
    width: 100%;
    bottom: 0px;
  }

  .caroussel-container {
    position: relative; 
    display: block; 
    height: 40%;
  }

  .caroussel-img {
    height: 100%;
    background: #000;
  }

  .caroussel-item {
    height: 100%;
    width: auto;
  }

  .carousel-inner {
    height: 100%;
    width: auto;
  }

  .lbl-title-post {
    font-size: 14px; 
    font-weight: bold; 
    font-family: 'Work Sans', sans-serif;
    height: 77px;
    text-transform: uppercase;
    margin-top: 10px;
  }

  .btn {
    cursor: pointer;
  }

  .content-post {
    height: 274px;
    overflow-x: hidden;
  }

  @media (max-width: 1198px) {
    .content-post {
      height: 276px;
    }
  }

  @media (max-width: 990px) {
    .lbl-title-post {
      height: 150px;
    }
  }

  @media (max-width: 767px) {

    .lbl-title-post {
      height: 77px;
    }

    .content-post {
      height: 274px;
    
    }
  }

</style>

<div style="margin-top: 25px; margin-bottom: 25px;" class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">

            <?= $this->Form->create($account,  [
			    'class' => 'form-load', 
			    'style' => 'margin-top: 67px;',
			    'url' => ['controller' => 'accounts', 'action' => 'edit'],
		    ]) ?>
            <fieldset>
                <legend>Perfil</legend>
                <div class="row">
                    <div class="col-md-12">
          						<?= $this->Form->input('name', ['label' => 'Nombre', 'required' => true, 'type' => 'text', 'name' => 'firstname', 'id' => 'firstname']) ?>
        				    </div>
            				<div class="col-md-12">
        						<?= $this->Form->input('email', ['label' => 'Correo', 'required' => true]); ?>
            				</div>
            				<div class="col-md-12">
            				    <?= $this->Form->input('phone', ['label' => 'Teléfono', 'required' => true]); ?>
                    </div>
                    <div class="col-md-12">
            				    <?= $this->Form->input('alternative_phone', ['label' => 'Teléfono alterntivo', 'required' => false]); ?>
                    </div>
                    <div class="col-md-12">
                        <?= $this->Form->input('password', ['label' => 'Clave', 'required' => false, 'type' => 'password', 'name' => 'password', 'id' => 'password', 'value' => '']) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $this->Form->input('confirm_password', ['label' => 'Confirmar Clave', 'required' => false, 'type' => 'password', 'id' => 'confirm_password', 'value' => '']) ?>
                    </div>
                </div>
            </fieldset>
            <?= $this->Form->button('Modificar', ['class' => 'btn-block btn-success']) ?>
            <a data-id="" id="btn-request-delete" href="javascript:void(0)" class="btn btn-danger btn-block <?= $account->request_delete ? 'disabled' : '' ?>"><?= $account->request_delete ? 'Solicitud de Baja Enviada' : 'Pedir Baja' ?></a>
            <?= $this->Form->end() ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 mb-3" style="margin-top: 67px !important;">
            <legend>Favoritos</legend>
            <div class="row">

              <?php if (!empty($account) && !empty($account->favorites)): ?>
                <?php foreach ($account->favorites as $favorite): ?>
                  <?php 
                    $post = $favorite->post;
                  ?>
                  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 mt-3">
                    <div class="card post-card">
  
                      <?php
                        $image = "noimage.png";
                        if (!empty($post->images)) {
                          foreach ($post->images as $img) {
                            if ($img->main) {
                              $image = $img->url;
                            }
                          }
                          if ($image == "noimage.png") {
                            $image = $post->images[0]->url;
                          }
                        }
                        $status = $post->status;
                      ?>
  
                      <div class="caroussel-container">
                      <?php if ($image == 'noimage.png'): ?>
                        <div data-id = "<?= $post->id ?>" class="post-target">
                          <img style="width: 100%; height: 100%" class="" src="/app/images/<?= $image ?>" alt="">
                        </div>
                      <?php else: ?>
                        <div id="carouselExampleIndicators<?= $post->id ?>" class="carousel slide caroussel-img" data-ride="carousel">
                          <ol class="carousel-indicators">
                            <?php if (!empty($post->images)): ?>
                                <?php foreach ($post->images as $key => $image): ?>
                                    <li data-target="#carouselExampleIndicators<?= $post->id ?>" data-slide-to="<?= $key ?>" class="<?= $key == 0 ? 'active' : '' ?>"></li>
                                <?php endforeach; ?>
                            <?php else: ?>
                               <li data-target="#carouselExampleIndicators<?= $post->id ?>" data-slide-to="0" class="active"></li>
                            <?php endif; ?>
                          </ol>
                          <div class="carousel-inner">
                            <?php if (!empty($post->images)): ?>
                                <?php foreach ($post->images as $key => $image): ?>
                                    <div data-id = "<?= $post->id ?>" class="carousel-item <?= $key == 0 ? 'active' : '' ?> post-target">
                                        <img style="width: 100%; height: 100%;" class="" src="/app/images/<?= $image->url ?>" alt="">
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <div class="carousel-item active">
                                    <img style="width: 100%; height: 100%;" class="" src="/app/images/noimage.png" alt="Sin Imagen">
                                </div>
                            <?php endif; ?>
                          </div>
                          <a class="carousel-control-prev" href="#carouselExampleIndicators<?= $post->id ?>" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Anterior</span>
                          </a>
                          <a class="carousel-control-next" href="#carouselExampleIndicators<?= $post->id ?>" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Siguiente</span>
                          </a>
                        </div>
                      <?php endif; ?>
                      <?php
                          $status_select = 8;
                          $show_status = $_SESSION['paraments']->post->status->$status->name != $_SESSION['paraments']->post->status->$status_select->name;
                        ?>
                        <?php if ($show_status): ?>
                          <div class="cinta">
                          </div>
                          <div class="status-container">
                             <label class="lbl-cinta"><?= $_SESSION['paraments']->post->status->$status->name ?></label>
                          </div>
                        <?php endif; ?>
                      </div>

                      <!-- Text Content -->
                      <div class="card-body post-body">
                        <div class="content-post">
                          <label data-id = "<?= $post->id ?>" class="post-target lbl-title-post"><?= $post->title ?></label>
                          <?php
                            $operation = $post->operation;
                            $property = $post->type_property;
                            $room = $post->room;
                            $money = $post->money;
                            $zone = $post->zone;
                          ?>

                          <div class="row">
                            <div class="col-md-9">
                              <label><?= $_SESSION['paraments']->post->operation->$operation ?> - <?= $_SESSION['paraments']->post->property->$property ?></label>
                              <label><?= $post->address ?> - <?= $_SESSION['paraments']->post->zone->$zone ?></label>
                            </div>

                            <div class="col-md-3">
                              <label><i class="fa fa-bed" aria-hidden="true"></i> <?= $post->room ?></label>
                              <label><i class="fa fa-car" aria-hidden="true"></i> <?= $post->garage ?></label>
                              <label><i class="fa fa-bath" aria-hidden="true"></i> <?= $post->bath ?></label>
                            </div>
                          </div>

                        </div>

                        <div class="mb-2 text-center post-footer">
                          <button data-id="<?= $favorite->post->id ?>" title="Exporta PDF" class="order-pdf btn btn-primary btn-xs"><span class="fa fa-print"></span></button>
                          <button data-id="<?= $favorite->id ?>" title="Eliminar Publicación" class="btn-cancel btn btn-danger btn-xs"><span class="fa fa-times"></span></button>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endforeach; ?>
              <?php else: ?>
                 <label>Lista Vacia</label>
              <?php endif; ?>
  
          </div>
        </div>
    </div>
</div>

<script type="text/javascript">

  var account;

  $(document).ready(function() {

      account = <?= json_encode($account) ?>;

      $('.order-pdf').click(function() {
        var id = $(this).data('id');
        var action = '/app/inicio/postExportPDF/' + id;
        window.open(action, '_blank');
      });

      $('.btn-cancel').click(function() {
        var id = $(this).data('id');
        var text = '¿Está Seguro que desea Eliminar la Publicación de su lista de Favoritos? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                  .append( $('<form/>').attr({'action': '/app/accounts/eliminarFavorito/' + id, 'method': 'post', 'id': 'replacer'})
                  .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': ''}))
                  .append( $('<input/>').attr( {'type': 'hidden', 'name': 'product_id', 'value': ''})))
                  .find('#replacer').submit();
            }
        });
      });

      $('.post-target').click(function() {
          var id = $(this).data('id');
          window.location = "/app/inicio/publicaciones/" + id;
      });

      $('#btn-request-delete').click(function() {
        var id = account.id;
        console.log(id);
        var text = '¿Está Seguro que desea dar de baja su Cuenta? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                  .append( $('<form/>').attr({'action': '/app/accounts/pedirBaja/' + id, 'method': 'post', 'id': 'replacer'})
                  .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': ''}))
                  .append( $('<input/>').attr( {'type': 'hidden', 'name': 'product_id', 'value': ''})))
                  .find('#replacer').submit();
            }
        });
      });
  });

</script>
