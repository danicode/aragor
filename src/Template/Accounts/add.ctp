<style type="text/css">

	.card-default {
		box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
	    -webkit-box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 1px 15px rgba(0, 0, 0, 0.23);
	    webkit-transition: all 400ms cubic-bezier(0.4, 0, 0.2, 1);
	}

	.g-recaptcha {
		width: 100%;
	}

	@media (max-width: 390px) {
		.card-container {
			margin-top: 40px;
		}
	}

	@media (max-width: 767px) {
		.card-login-container {
			margin-top: 20px;
		}
	}

</style>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="container" style="margin-top: 115px; margin-bottom: 25px;">
	<div class="row">
		<div class="col-md-6">
			<div class="card card-default card-container">
				<div class="card-body">
					<?= $this->Form->create($account,  ['class' => 'form-load']) ?>

						<div class="form-group">
							<h2 style="color: #535d5d; font-size: 25px;">Registrarse</h2>
						</div>

						<div class="row">

							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<?= $this->Form->input('name', ['label' => 'Nombre', 'required' => true]); ?>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<?= $this->Form->input('phone', ['label' => 'Teléfono', 'type' => 'number', 'required' => true]); ?>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<?= $this->Form->input('alternative_phone', ['label' => 'Teléfono alternativo', 'type' => 'number', 'required' => false]); ?>
										</div>
									</div>

									<div class="col-md-12">
										<div class="form-group">
											<?= $this->Form->input('email', ['label' => 'Correo', 'required' => true]); ?>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<?= $this->Form->input('password', ['label' => 'Clave', 'required' => true]); ?>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<?= $this->Form->input('confirm_password', ['label' => __('Confirmar Clave'), 'type' => 'password', 'required' => true]); ?>
								</div>
							</div>
				
							<div class="col-md-12">
								<div style="text-align: center; position: relative; display: inline-block;" class="g-recaptcha" data-sitekey="6Ld4jz0UAAAAAL5AeTo3-eYO-U-mUstDN0s7Vz_a"></div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<?= $this->Form->button('Agregar', ['class' => 'btn-block btn-info disabled', 'id' => 'btn-add-account']) ?>
								</div>
							</div>
						</div>
						

        			<?= $this->Form->end() ?>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="card card-default card-login-container">
				<div class="card-body">
					<?= $this->Form->create(null, [
		                    'url' => ['controller' => 'accounts', 'action' => 'login']
		                ]) ?>
						<div class="form-group">
							<h2 style="color: #535d5d; font-size: 25px;">Ingresar</h2>
						</div>
						<div class="form-group">
							<?= $this->Form->input('email', ['label' => 'Correo', 'required' => true]); ?>
						</div>
						<div class="form-group">
							<?= $this->Form->input('password', ['label' => 'Clave', 'required' => true]); ?>
						</div>
						<div class="form-group">
							<?= $this->Form->button('Entrar', ['class' => 'btn-block btn-success']) ?>
						</div>
						<p><a id="forget-password" href="javascript:void(0)">Ha olvidado su Clave</a></p>
					<?= $this->Form->end() ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="forget-password-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          	<div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <div class="row">
	                <div class="col-md-12">

						<div class="register forget">
							<?= $this->Form->create(null, ['url' => ['controller' => 'accounts', 'action' => 'recuperar-clave']]) ?>
		                    <?= $this->Form->input('', ['label' => __('Correo'), 'required' => true, 'type' => 'email', 'name' => 'email', 'id' => 'email']) ?>
		                    <div class="sign-up">
								<?= $this->Form->input(__('Entrar'), ['id' => 'btn-forget', 'class' => 'btn btn-primary', 'type' => 'submit']) ?>
		                    </div>
		                    <?= $this->Form->end() ?>
						</div>

	                </div>
	            </div>
          	</div>
        </div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function() {
		$('#forget-password').click(function() {
			$('#forget-password-popup').modal('show');
        });
	});

</script>
