<div class="row">
     <div class="col-md-7">
        <div class="pull-right">
            <a class="btn btn-default" title="Ir a la lista de Usuarios." href="<?= $this->Url->build(["action" => "index"]) ?>" role="button">
                <span class="glyphicon icon-arrow-left" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="usuarios form large-9 medium-8 columns content">
    <?= $this->Form->create($user, ['class' => 'form-load']) ?>
    <fieldset>
        <legend><?= __('Editar Usuario') ?></legend>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group text required">
                    <label class="control-label" for="name">Nombre</label>
                    <input type="text" name="name" required="required" maxlength="100" id="name" class="form-control" value="<?= $user->name ?>">
                    <small class="form-text text-muted">
                      Por ej.: Juan Perez (consulta alquileres)
                    </small>
                    <small class="form-text text-muted">
                      Esto en caso que el usuario administre correos relacionados a consultas de alquileres.
                    </small>
                </div>
                <?php
                    echo $this->Form->control('username', ['label' => 'Usuario']);
                    echo $this->Form->control('password', ['label' => 'Clave', 'value' => '', 'required' => false]);
                    echo $this->Form->control('email', ['label' => 'Correo']);
                    echo $this->Form->control('contact', ['label' => 'Recibir Correo']);
                    echo $this->Form->control('enabled', ['label' => 'Habilitar']);
                ?>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
</div>
