<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5" >

        <?php

             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la tabla',
                'class' => 'btn btn-default btn-export',
                'escape' => false
                ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered" id="table-customers">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Teléfono</th>
                    <th>Creado</th>
                    <th>Modificado</th> 
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-restore',
        'name' =>  'Restaurar',
        'icon' =>  'icon-undo2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-customers-deleted', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var table_customers = null;
    var customer_selected = null;

    $(document).ready(function () {

        $('#table-customers').removeClass('display');

    	table_customers = $('#table-customers').DataTable({
    	    "order": [[ 0, 'asc' ]],
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
    	    "autoWidth": true,
    	    "scrollY": '450px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "deferRender": true,
		    "ajax": {
                "url": "/app/users/deletedcustomer.json",
                "dataSrc": "accounts"
            },
            "columns": [
                { "data": "name" },
                { "data": "email" },
                { "data": "phone" },
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "modified",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                }
            ],
    	    "columnDefs": [
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
    	    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[ 50, 100, -1], [ 50, 100, "Todas"]],
    	});

    	$('#table-customers_filter').closest('div').before($('#btns-tools').contents());

    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-customers').tableExport({tableName: 'Clientes', type:'excel', escape:'false'});
            }
        });
    });

    $('#table-customers tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_customers.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            customer_selected = table_customers.row( this ).data();

            $('.modal-customers-deleted').modal('show');
        }
    });

    $('#btn-restore').click(function() {
        var action = '/app/users/restoreCustomer/' + table_customers.$('tr.selected').attr('id');
        window.open(action, '_self');
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
