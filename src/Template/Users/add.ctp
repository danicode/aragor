<div class="usuarios form large-4 medium-3 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Agregar Usuario') ?></legend>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group text required">
                    <label class="control-label" for="name">Nombre</label>
                    <input type="text" name="name" required="required" maxlength="100" id="name" class="form-control">
                    <small class="form-text text-muted">
                      Por ej.: Juan Perez (consulta alquileres)
                    </small>
                    <small class="form-text text-muted">
                      Esto en caso que el usuario administre correos relacionados a consultas de alquileres.
                    </small>
                </div>
                <?php
                    echo $this->Form->control('username', ['label' => 'Usuario']);
                    echo $this->Form->control('password', ['label' => 'Clave']);
                    echo $this->Form->control('email', ['label' => 'Correo']);
                    echo $this->Form->control('contact', ['label' => 'Recibir Correo']);
                    echo $this->Form->control('enabled', ['label' => 'Habilitar']);
                ?>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Aceptar')) ?>
    <?= $this->Form->end() ?>
</div>
