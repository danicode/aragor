<br>
<br>

<div class="mx-auto" style="width: 240px;">
  <?php echo $this->Html->image('logo.png', ['alt' => 'ARAGOR IKER', 'width' => 240]);?>
</div>
<br>
<div class="mx-auto" style="width: 200px;">
  <?= $this->Form->create() ?>
  <?= $this->Form->input('username', ['label' => __('Usuario'), 'autofocus' => true]) ?>
  <?= $this->Form->input('password', ['label' => __('Clave')]) ?>
  <?= $this->Form->button(__('Entrar'), ['class' => 'btn btn-primary']) ?>
  <?= $this->Form->end() ?>
</div>
