<style type="text/css">

    .huge {
      font-size: 40px;
    }
    .panel-green {
      border-color: #5cb85c;
    }
    .panel-green > .panel-heading {
      border-color: #5cb85c;
      color: white;
      background-color: #5cb85c;
    }
    .panel-green > a {
      color: #5cb85c;
    }
    .panel-green > a:hover {
      color: #3d8b3d;
    }
    .panel-red {
      border-color: #d9534f;
    }
    .panel-red > .panel-heading {
      border-color: #d9534f;
      color: white;
      background-color: #d9534f;
    }
    .panel-red > a {
      color: #d9534f;
    }
    .panel-red > a:hover {
      color: #b52b27;
    }
    .panel-yellow {
      border-color: #f0ad4e;
    }
    .panel-yellow > .panel-heading {
      border-color: #f0ad4e;
      color: white;
      background-color: #f0ad4e;
    }
    .panel-yellow > a {
      color: #f0ad4e;
    }
    .panel-yellow > a:hover {
      color: #df8a13;
    }

</style>

<p><?= __('BIENVENIDO') ?> <span class="badge badge-success"><?= h($user['username']) ?></span></p>

<div class="row">
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 mb-2" id="Home-customers">
        <div class="card border-success">
            <h5 class="card-header">Publicaciones Nuevas Mes Actual</h5>
            <div class="card-body text-success">
               <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button class="btn btn-success w-100" type="button">Total</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="<?= $post_publication; ?>" readonly>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 mb-2" id="Home-connections">
        <div class="card border-info">
            <h5 class="card-header">Publicaciones Confirm. Mes Actual</h5>
            <div class="card-body text-info">
               <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button class="btn btn-info w-100" type="button">Total</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="<?= $post_confirmation; ?>" readonly>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 mb-2" id="Home-tickets">
        <div class="card border-secondary">
            <h5 class="card-header">Publicaciones en Alquiler</h5>
            <div class="card-body text-secondary">
               <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button class="btn btn-secondary w-100" type="button">Total</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="<?= $post_rent; ?>" readonly>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 mb-2" id="Home-payments">
        <div class="card border-warning">
            <h5 class="card-header">Publicaciones en Ventas</h5>
            <div class="card-body text-warning">
               <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button class="btn btn-warning w-100" type="button">Total</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="<?= $post_sale; ?>" readonly>
                </div>
            </div>
        </div>
    </div>

</div>
