<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5" >

        <?php

           echo $this->Html->link(
                '<span class="glyphicon icon-filter" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Filtrar por Columnas',
                'class' => 'btn btn-default btn-filter-column',
                'data-toggle' => 'modal', 'data-target' => '#modal-filter',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la tabla',
                'class' => 'btn btn-default btn-export',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered" id="table-customers">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Teléfono</th>
                    <th>Teléfono alternativo</th>
                    <th>Comentario</th>
                    <th>Creado</th>
                    <th>Modificado</th> 
                    <th>Solicitud eliminación</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Comentarios',
        'icon' =>  'glyphicon icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-customers', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="modal fade" id="modal-edit-comments" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit-comments">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Comentario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "users", "action" => "accountEditComment"]) ?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">

                        <div class="col-xl-12">
                            <fieldset>
                                <?php
                                    echo $this->Form->input('comments', ['type' => 'textarea', 'label' => 'Comentarios']);
                                ?>
                                <input type="hidden" name="account_id" id="account_id">
                            </fieldset>

                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit">Guardar</button> 

                        </div>
                    </div>

                </form>
            </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filtrar por columnas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?= $this->Form->create() ?>

                <div class="row">
                    <div class="col-md-4">
                        <?= $this->Form->input('name', ['label' => __('Nombre'), 'class'=> 'column_search', 'data-column' => 0]) ?>
                        <?= $this->Form->input('phone', ['label' => __('Teléfono'), 'class'=> 'column_search', 'data-column' => 2]) ?>
                        <?= $this->Form->input('phone', ['label' => __('Teléfono Alternativo'), 'class'=> 'column_search', 'data-column' => 3]) ?>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= $this->Form->input('comments', ['label' => __('Comentario'), 'class'=> 'column_search', 'data-column' => 4]) ?>
                            <label class="control-label" for="created">Creado</label>
                            <div class='input-group date' id='created-datetimepicker'>
                                <span class="input-group-addon mr-0">Fecha</span>
                                <input name="input-created" id="input-created" type='text' class="column_search form-control datetimepicker" data-column="5" />
                                <span class="input-group-addon mr-0">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="modified">Modificado</label>
                            <div class='input-group date' id='modified-datetimepicker'>
                                <span class="input-group-addon mr-0">Fecha</span>
                                <input name="input-modified" id="input-modified" type='text' class="column_search form-control datetimepicker" data-column="6" />
                                <span class="input-group-addon mr-0">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <?= $this->Form->input('email', ['label' => __('Correo'), 'class'=> 'column_search', 'data-column' => 1]) ?>
                        <?= $this->Form->input('request_deleted', ['label' => __('Solicitud Eliminación'), 'class'=> 'column_search', 'data-column' => 7, 'checked' => false]) ?>
                    </div>
                </div>  

                <?= $this->Form->end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-clear-filter" >Limpiar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_customers = null;
    var customer_selected = null;

    $(document).ready(function () {

        $('#table-customers').removeClass('display');

    	table_customers = $('#table-customers').DataTable({
    	    "order": [[ 0, 'asc' ]],
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
    	    "autoWidth": true,
    	    "scrollY": '450px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "deferRender": true,
		    "ajax": {
                "url": "/app/users/customers.json",
                "dataSrc": "accounts"
            },
            "columns": [
                { "data": "name" },
                { "data": "email" },
                { "data": "phone" },
                { "data": "alternative_phone" },
                { "data": "comments" },
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                {
                    "data": "modified",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "request_delete",
                    "render": function ( data, type, row ) {
                        if (type === 'display') {
                            var enabled = '<i class="fa fa-times" aria-hidden="true"></i>';
                            if (data) {
                                enabled = '<i class="fa fa-check" aria-hidden="true"></i>';
                            }
                            data = enabled;
                        }
                        return data;
                    }
                }
            ],
    	    "columnDefs": [
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
    	    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[ 50, 100, -1], [ 50, 100, "Todas"]],
    	});

    	$('#table-customers_filter').closest('div').before($('#btns-tools').contents());

    	$('.btn-hide-column').click(function() {
           $('.modal-hide-columns-customers').modal('show');
        });

        $('.column_search').on( 'keyup', function () {

            table_customers
                .columns( $(this).data('column') )
                .search( this.value )
                .draw();

            checkStatusFilter();
        });

        $('#created-datetimepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        }).on('dp.change', function (ev) {

            table_customers
                .columns( 5 )
                .search( $('#input-created').val() )
                .draw();

            checkStatusFilter();
        });
        
        $('#modified-datetimepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        }).on('dp.change', function (ev) {

            table_customers
                .columns( 6 )
                .search( $('#input-modified').val() )
                .draw();

            checkStatusFilter();
        });

        $('input:checkbox.column_search').change(function() {

            table_customers
                .columns( $(this).data('column') )
                .search( $(this).is(":checked") )
                .draw();

            checkStatusFilter();
        });

        $('select.column_search').change(function() {

            var search = this.value;

            if ($(this).data('column') == 17) {
                search = $(this).find('option:selected').text();
            }

            table_customers
                .columns( $(this).data('column') )
                .search( search )
                .draw();

            checkStatusFilter();
        });

        $('.btn-clear-filter').click(function() {
            $('.column_search').val('');
            table_customers.search( '' ).columns().search( '' ).draw();
            $('.column_search').prop('checked', false);
            checkStatusFilter();
        });

    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-customers').tableExport({tableName: 'Clientes', type:'excel', escape:'false'});
            }
        });
    });

    $('#table-customers tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_customers.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            customer_selected = table_customers.row( this ).data();

            $('.modal-customers').modal('show');
        }
    });

    $('#btn-delete').click(function() {

        var text = "¿Está Seguro que desea eliminar el Cliente?";
        var id = customer_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/users/deleteCustomer/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
                .find('#replacer').submit();
            }
        });
    });

    $('#btn-edit').click(function() {

        var account_id = customer_selected.id;
        var comment = customer_selected.comments;

        $('input#account_id').val(account_id);
        $('textarea#comments').html(comment);
        $('.modal-customers').modal('hide');
        $('#modal-edit-comments').modal('show');
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
