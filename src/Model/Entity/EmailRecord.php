<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EmailRecord Entity
 *
 * @property int $id
 * @property string $status
 * @property string $message
 * @property string $send_from
 * @property string $post_code
 * @property string $customer_name
 * @property string $customer_email
 * @property int $customer_phone
 * @property string $email_contact
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class EmailRecord extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'status' => true,
        'message' => true,
        'send_from' => true,
        'post_code' => true,
        'customer_name' => true,
        'customer_email' => true,
        'customer_phone' => true,
        'email_contact' => true,
        'created' => true,
        'modified' => true
    ];
}
