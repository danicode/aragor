<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Post Entity
 *
 * @property int $id
 * @property string $code
 * @property string $title
 * @property string $type_property
 * @property string $zone
 * @property string $number_property
 * @property string $operation
 * @property string $room
 * @property string $bath
 * @property string $garage
 * @property float $price
 * @property string $money
 * @property string $status
 * @property bool $enabled
 * @property bool $deleted
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $publication_date
 * @property \Cake\I18n\FrozenTime $confirmation_date
 *
 * @property \App\Model\Entity\Favorite[] $favorites
 * @property \App\Model\Entity\Image[] $images
 */
class Post extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
