<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EmailRecords Model
 *
 * @method \App\Model\Entity\EmailRecord get($primaryKey, $options = [])
 * @method \App\Model\Entity\EmailRecord newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\EmailRecord[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EmailRecord|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EmailRecord patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EmailRecord[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\EmailRecord findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EmailRecordsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('email_records');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->scalar('message')
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        $validator
            ->scalar('send_from')
            ->requirePresence('send_from', 'create')
            ->notEmpty('send_from');

        $validator
            ->scalar('post_code')
            ->allowEmpty('post_code');

        $validator
            ->scalar('customer_name')
            ->requirePresence('customer_name', 'create')
            ->notEmpty('customer_name');

        $validator
            ->scalar('customer_email')
            ->requirePresence('customer_email', 'create')
            ->notEmpty('customer_email');

        $validator
            ->integer('customer_phone')
            ->requirePresence('customer_phone', 'create')
            ->notEmpty('customer_phone');

        $validator
            ->scalar('email_contact')
            ->allowEmpty('email_contact');

        return $validator;
    }
}
