<?php
use Migrations\AbstractSeed;

/**
 * ActionsSystemRoles seed.
 */
class ActionsSystemRolesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '758',
                'role_id' => '1',
                'action_system_id' => '1',
            ],
            [
                'id' => '759',
                'role_id' => '1',
                'action_system_id' => '2',
            ],
            [
                'id' => '760',
                'role_id' => '1',
                'action_system_id' => '3',
            ],
            [
                'id' => '761',
                'role_id' => '1',
                'action_system_id' => '4',
            ],
            [
                'id' => '762',
                'role_id' => '1',
                'action_system_id' => '5',
            ],
            [
                'id' => '763',
                'role_id' => '1',
                'action_system_id' => '10',
            ],
            [
                'id' => '764',
                'role_id' => '1',
                'action_system_id' => '11',
            ],
            [
                'id' => '765',
                'role_id' => '1',
                'action_system_id' => '12',
            ],
            [
                'id' => '766',
                'role_id' => '1',
                'action_system_id' => '13',
            ],
            [
                'id' => '767',
                'role_id' => '1',
                'action_system_id' => '14',
            ],
            [
                'id' => '768',
                'role_id' => '1',
                'action_system_id' => '15',
            ],
            [
                'id' => '769',
                'role_id' => '1',
                'action_system_id' => '16',
            ],
            [
                'id' => '770',
                'role_id' => '1',
                'action_system_id' => '17',
            ],
            [
                'id' => '771',
                'role_id' => '1',
                'action_system_id' => '18',
            ],
            [
                'id' => '772',
                'role_id' => '1',
                'action_system_id' => '19',
            ],
            [
                'id' => '773',
                'role_id' => '1',
                'action_system_id' => '20',
            ],
            [
                'id' => '774',
                'role_id' => '1',
                'action_system_id' => '21',
            ],
            [
                'id' => '775',
                'role_id' => '1',
                'action_system_id' => '22',
            ],
            [
                'id' => '776',
                'role_id' => '1',
                'action_system_id' => '23',
            ],
            [
                'id' => '777',
                'role_id' => '1',
                'action_system_id' => '24',
            ],
            [
                'id' => '778',
                'role_id' => '1',
                'action_system_id' => '25',
            ],
            [
                'id' => '779',
                'role_id' => '1',
                'action_system_id' => '26',
            ],
            [
                'id' => '780',
                'role_id' => '1',
                'action_system_id' => '27',
            ],
            [
                'id' => '781',
                'role_id' => '1',
                'action_system_id' => '28',
            ],
            [
                'id' => '782',
                'role_id' => '1',
                'action_system_id' => '29',
            ],
            [
                'id' => '783',
                'role_id' => '1',
                'action_system_id' => '30',
            ],
            [
                'id' => '784',
                'role_id' => '1',
                'action_system_id' => '31',
            ],
            [
                'id' => '785',
                'role_id' => '1',
                'action_system_id' => '32',
            ],
            [
                'id' => '786',
                'role_id' => '1',
                'action_system_id' => '33',
            ],
            [
                'id' => '787',
                'role_id' => '1',
                'action_system_id' => '34',
            ],
            [
                'id' => '788',
                'role_id' => '1',
                'action_system_id' => '35',
            ],
            [
                'id' => '789',
                'role_id' => '1',
                'action_system_id' => '36',
            ],
            [
                'id' => '790',
                'role_id' => '1',
                'action_system_id' => '39',
            ],
            [
                'id' => '791',
                'role_id' => '1',
                'action_system_id' => '40',
            ],
            [
                'id' => '792',
                'role_id' => '1',
                'action_system_id' => '41',
            ],
            [
                'id' => '793',
                'role_id' => '1',
                'action_system_id' => '42',
            ],
            [
                'id' => '794',
                'role_id' => '1',
                'action_system_id' => '43',
            ],
            [
                'id' => '795',
                'role_id' => '1',
                'action_system_id' => '44',
            ],
            [
                'id' => '796',
                'role_id' => '1',
                'action_system_id' => '45',
            ],
            [
                'id' => '797',
                'role_id' => '1',
                'action_system_id' => '46',
            ],
            [
                'id' => '798',
                'role_id' => '1',
                'action_system_id' => '47',
            ],
            [
                'id' => '799',
                'role_id' => '1',
                'action_system_id' => '48',
            ],
            [
                'id' => '800',
                'role_id' => '1',
                'action_system_id' => '49',
            ],
            [
                'id' => '801',
                'role_id' => '1',
                'action_system_id' => '50',
            ],
            [
                'id' => '802',
                'role_id' => '1',
                'action_system_id' => '51',
            ],
            [
                'id' => '803',
                'role_id' => '1',
                'action_system_id' => '52',
            ],
            [
                'id' => '804',
                'role_id' => '1',
                'action_system_id' => '53',
            ],
            [
                'id' => '805',
                'role_id' => '1',
                'action_system_id' => '54',
            ],
            [
                'id' => '806',
                'role_id' => '1',
                'action_system_id' => '55',
            ],
            [
                'id' => '807',
                'role_id' => '1',
                'action_system_id' => '56',
            ],
            [
                'id' => '808',
                'role_id' => '1',
                'action_system_id' => '57',
            ],
            [
                'id' => '809',
                'role_id' => '1',
                'action_system_id' => '58',
            ],
            [
                'id' => '810',
                'role_id' => '1',
                'action_system_id' => '59',
            ],
            [
                'id' => '811',
                'role_id' => '1',
                'action_system_id' => '60',
            ],
            [
                'id' => '812',
                'role_id' => '1',
                'action_system_id' => '61',
            ],
            [
                'id' => '813',
                'role_id' => '1',
                'action_system_id' => '62',
            ],
            [
                'id' => '814',
                'role_id' => '1',
                'action_system_id' => '63',
            ],
            [
                'id' => '815',
                'role_id' => '1',
                'action_system_id' => '64',
            ],
            [
                'id' => '816',
                'role_id' => '1',
                'action_system_id' => '70',
            ],
            [
                'id' => '817',
                'role_id' => '1',
                'action_system_id' => '71',
            ],
            [
                'id' => '818',
                'role_id' => '1',
                'action_system_id' => '72',
            ],
            [
                'id' => '819',
                'role_id' => '1',
                'action_system_id' => '73',
            ],
            [
                'id' => '820',
                'role_id' => '1',
                'action_system_id' => '74',
            ],
            [
                'id' => '821',
                'role_id' => '1',
                'action_system_id' => '75',
            ],
            [
                'id' => '822',
                'role_id' => '1',
                'action_system_id' => '76',
            ],
            [
                'id' => '823',
                'role_id' => '1',
                'action_system_id' => '81',
            ],
            [
                'id' => '824',
                'role_id' => '1',
                'action_system_id' => '82',
            ],
            [
                'id' => '825',
                'role_id' => '1',
                'action_system_id' => '83',
            ],
            [
                'id' => '826',
                'role_id' => '1',
                'action_system_id' => '84',
            ],
            [
                'id' => '827',
                'role_id' => '1',
                'action_system_id' => '85',
            ],
            [
                'id' => '828',
                'role_id' => '1',
                'action_system_id' => '86',
            ],
            [
                'id' => '829',
                'role_id' => '1',
                'action_system_id' => '87',
            ],
            [
                'id' => '830',
                'role_id' => '1',
                'action_system_id' => '88',
            ],
            [
                'id' => '831',
                'role_id' => '1',
                'action_system_id' => '89',
            ],
            [
                'id' => '832',
                'role_id' => '1',
                'action_system_id' => '90',
            ],
            [
                'id' => '833',
                'role_id' => '1',
                'action_system_id' => '91',
            ],
            [
                'id' => '834',
                'role_id' => '1',
                'action_system_id' => '92',
            ],
            [
                'id' => '835',
                'role_id' => '1',
                'action_system_id' => '93',
            ],
            [
                'id' => '836',
                'role_id' => '1',
                'action_system_id' => '94',
            ],
            [
                'id' => '837',
                'role_id' => '1',
                'action_system_id' => '95',
            ],
            [
                'id' => '838',
                'role_id' => '1',
                'action_system_id' => '96',
            ],
            [
                'id' => '839',
                'role_id' => '1',
                'action_system_id' => '97',
            ],
            [
                'id' => '840',
                'role_id' => '1',
                'action_system_id' => '98',
            ],
            [
                'id' => '841',
                'role_id' => '1',
                'action_system_id' => '99',
            ],
            [
                'id' => '842',
                'role_id' => '1',
                'action_system_id' => '100',
            ],
            [
                'id' => '843',
                'role_id' => '1',
                'action_system_id' => '101',
            ],
            [
                'id' => '844',
                'role_id' => '1',
                'action_system_id' => '102',
            ],
            [
                'id' => '845',
                'role_id' => '1',
                'action_system_id' => '103',
            ],
            [
                'id' => '846',
                'role_id' => '1',
                'action_system_id' => '104',
            ],
            [
                'id' => '847',
                'role_id' => '1',
                'action_system_id' => '105',
            ],
            [
                'id' => '848',
                'role_id' => '1',
                'action_system_id' => '106',
            ],
            [
                'id' => '849',
                'role_id' => '1',
                'action_system_id' => '107',
            ],
            [
                'id' => '850',
                'role_id' => '1',
                'action_system_id' => '108',
            ],
            [
                'id' => '851',
                'role_id' => '1',
                'action_system_id' => '109',
            ],
            [
                'id' => '852',
                'role_id' => '1',
                'action_system_id' => '110',
            ],
            [
                'id' => '853',
                'role_id' => '1',
                'action_system_id' => '111',
            ],
            [
                'id' => '854',
                'role_id' => '1',
                'action_system_id' => '112',
            ],
            [
                'id' => '855',
                'role_id' => '1',
                'action_system_id' => '113',
            ],
            [
                'id' => '856',
                'role_id' => '1',
                'action_system_id' => '114',
            ],
            [
                'id' => '857',
                'role_id' => '1',
                'action_system_id' => '115',
            ],
            [
                'id' => '858',
                'role_id' => '1',
                'action_system_id' => '116',
            ],
            [
                'id' => '859',
                'role_id' => '1',
                'action_system_id' => '117',
            ],
            [
                'id' => '860',
                'role_id' => '1',
                'action_system_id' => '118',
            ],
            [
                'id' => '861',
                'role_id' => '1',
                'action_system_id' => '119',
            ],
            [
                'id' => '862',
                'role_id' => '1',
                'action_system_id' => '122',
            ],
            [
                'id' => '863',
                'role_id' => '1',
                'action_system_id' => '123',
            ],
            [
                'id' => '864',
                'role_id' => '1',
                'action_system_id' => '124',
            ],
            [
                'id' => '865',
                'role_id' => '1',
                'action_system_id' => '125',
            ],
            [
                'id' => '866',
                'role_id' => '1',
                'action_system_id' => '126',
            ],
            [
                'id' => '867',
                'role_id' => '1',
                'action_system_id' => '127',
            ],
            [
                'id' => '868',
                'role_id' => '1',
                'action_system_id' => '128',
            ],
            [
                'id' => '869',
                'role_id' => '1',
                'action_system_id' => '129',
            ],
            [
                'id' => '870',
                'role_id' => '1',
                'action_system_id' => '130',
            ],
            [
                'id' => '871',
                'role_id' => '1',
                'action_system_id' => '131',
            ],
            [
                'id' => '872',
                'role_id' => '1',
                'action_system_id' => '132',
            ],
            [
                'id' => '873',
                'role_id' => '1',
                'action_system_id' => '133',
            ],
            [
                'id' => '874',
                'role_id' => '1',
                'action_system_id' => '134',
            ],
            [
                'id' => '875',
                'role_id' => '1',
                'action_system_id' => '135',
            ],
            [
                'id' => '876',
                'role_id' => '1',
                'action_system_id' => '137',
            ],
            [
                'id' => '877',
                'role_id' => '1',
                'action_system_id' => '138',
            ],
            [
                'id' => '878',
                'role_id' => '1',
                'action_system_id' => '139',
            ],
            [
                'id' => '879',
                'role_id' => '1',
                'action_system_id' => '141',
            ],
            [
                'id' => '880',
                'role_id' => '1',
                'action_system_id' => '142',
            ],
            [
                'id' => '881',
                'role_id' => '1',
                'action_system_id' => '143',
            ],
            [
                'id' => '882',
                'role_id' => '1',
                'action_system_id' => '144',
            ],
            [
                'id' => '883',
                'role_id' => '1',
                'action_system_id' => '145',
            ],
            [
                'id' => '1052',
                'role_id' => '1',
                'action_system_id' => '146',
            ],
            [
                'id' => '1054',
                'role_id' => '1',
                'action_system_id' => '147',
            ],
            [
                'id' => '1057',
                'role_id' => '1',
                'action_system_id' => '148',
            ],
            [
                'id' => '1187',
                'role_id' => '1',
                'action_system_id' => '149',
            ],
            [
                'id' => '1188',
                'role_id' => '1',
                'action_system_id' => '150',
            ],
            [
                'id' => '1189',
                'role_id' => '1',
                'action_system_id' => '151',
            ],
            [
                'id' => '1190',
                'role_id' => '1',
                'action_system_id' => '152',
            ],
            [
                'id' => '1191',
                'role_id' => '1',
                'action_system_id' => '153',
            ],
            [
                'id' => '1192',
                'role_id' => '1',
                'action_system_id' => '154',
            ],
            [
                'id' => '1193',
                'role_id' => '1',
                'action_system_id' => '155',
            ],
            [
                'id' => '1194',
                'role_id' => '1',
                'action_system_id' => '156',
            ],
            [
                'id' => '1195',
                'role_id' => '1',
                'action_system_id' => '157',
            ],
            [
                'id' => '1196',
                'role_id' => '1',
                'action_system_id' => '158',
            ],
            [
                'id' => '1197',
                'role_id' => '1',
                'action_system_id' => '159',
            ],
            [
                'id' => '1198',
                'role_id' => '1',
                'action_system_id' => '160',
            ],
            [
                'id' => '1199',
                'role_id' => '1',
                'action_system_id' => '161',
            ],
            [
                'id' => '1200',
                'role_id' => '1',
                'action_system_id' => '162',
            ],
            [
                'id' => '1201',
                'role_id' => '1',
                'action_system_id' => '163',
            ],
            [
                'id' => '1202',
                'role_id' => '1',
                'action_system_id' => '164',
            ],
            [
                'id' => '1203',
                'role_id' => '1',
                'action_system_id' => '165',
            ],
            [
                'id' => '1204',
                'role_id' => '1',
                'action_system_id' => '166',
            ],
            [
                'id' => '1205',
                'role_id' => '1',
                'action_system_id' => '167',
            ],
            [
                'id' => '1206',
                'role_id' => '1',
                'action_system_id' => '168',
            ],
            [
                'id' => '1207',
                'role_id' => '1',
                'action_system_id' => '169',
            ],
            [
                'id' => '1208',
                'role_id' => '1',
                'action_system_id' => '170',
            ],
            [
                'id' => '1209',
                'role_id' => '1',
                'action_system_id' => '171',
            ],
            [
                'id' => '1210',
                'role_id' => '1',
                'action_system_id' => '172',
            ],
        ];

        $table = $this->table('actions_system_roles');
        $table->insert($data)->save();
    }
}
