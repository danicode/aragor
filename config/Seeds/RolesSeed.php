<?php
use Migrations\AbstractSeed;

/**
 * Roles seed.
 */
class RolesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'sistema',
                'created' => '2017-10-11 00:00:00',
                'modified' => '2017-10-23 15:28:26',
                'enabled' => '1',
                'deleted' => '0',
            ],
        ];

        $table = $this->table('roles');
        $table->insert($data)->save();
    }
}
