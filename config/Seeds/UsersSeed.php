<?php
use Migrations\AbstractSeed;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        /*$2y$10$l2jc7nD2rxTe41oae/gzWO.rURx4TyqKtk3QaLstaymIz2tWNSoEK*/
        
        $data = [
            [
                'id' => '1',
                'name' => 'sistema',
                'username' => 'sistema',
                'password' => '$2y$10$l2jc7nD2rxTe41oae/gzWO.rURx4TyqKtk3QaLstaymIz2tWNSoEK',
                'phone' => '',
                'enabled' => '1',
                'created' => '2016-09-11 15:34:09',
                'modified' => '2017-09-27 07:45:39',
                'deleted' => '0',
                'description' => '',
            ],
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
