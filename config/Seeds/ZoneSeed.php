<?php
use Migrations\AbstractSeed;

/**
 * Zone seed.
 */
class ZoneSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Area A',
                'cp' => '9999',
                'enabled' => '1',
                'deleted' => '0',
            ],
        ];

        $table = $this->table('zone');
        $table->insert($data)->save();
    }
}
