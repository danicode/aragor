<?php
use Migrations\AbstractSeed;

/**
 * TemplateDemoData seed.
 */
class TemplateDemoDataSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        
        #CONTROLLERS
        $data = [
            [
                'id' => '1',
                'local_address' => '190.210.176.1',
                'dns_server' => '8.8.8.8',
                'deleted' => '0',
                'queue_default' => 'wireless-default',
            ],
            [
                'id' => '2',
                'local_address' => '190.210.176.1',
                'dns_server' => '8.8.8.8',
                'deleted' => '0',
                'queue_default' => 'default-small',
            ],
        ];
        $table = $this->table('t_controllers');
        $table->insert($data)->save();
        
        
        #PPP SERVERS
        $data = [
            [
                'id' => '1',
                'interface' => 'ether1',
                'one_session_per_host' => 'yes',
                'authentication' => 'chap,mschap1,mschap2',
                'service_name' => 'ISPBrain-PPPoEServer',
                'disabled' => 'no',
                'controller_id' => '1',
                'api_id' => NULL,
                'deleted' => '0',
            ],
            [
                'id' => '2',
                'interface' => 'ether1',
                'one_session_per_host' => 'yes',
                'authentication' => 'chap,mschap1,mschap2',
                'service_name' => 'ISPBrain-PPPoEServer',
                'disabled' => 'no',
                'controller_id' => '2',
                'api_id' => NULL,
                'deleted' => '0',
            ],
        ];
        $table = $this->table('t_ppp_servers');
        $table->insert($data)->save();
        
        
        #PPP PROFILES
        $data = [
            [
                'id' => '1',
                'name' => 'profile-3mb',
                'local_address' => '190.210.176.1',
                'dns_server' => '8.8.8.8',
                'rate_limit_string' => '10k/20k 30k/40k 50k/60k 70/80 1 10k/20k',
                'queue_type' => 'wireless-default',
                'down' => '20',
                'up' => '10',
                'down_burst' => '40',
                'up_burst' => '30',
                'down_threshold' => '60',
                'up_threshold' => '50',
                'down_time' => '80',
                'up_time' => '70',
                'priority' => '1',
                'down_at_limit' => '20',
                'up_at_limit' => '10',
                'controller_id' => '1',
                'deleted' => '0',
                'api_id' => NULL,
            ],
            [
                'id' => '2',
                'name' => 'profile-6mb',
                'local_address' => '190.210.176.1',
                'dns_server' => '8.8.8.8',
                'rate_limit_string' => '10k/20k 30k/40k 50k/60k 70/80 2 10k/20k',
                'queue_type' => 'wireless-default',
                'down' => '20',
                'up' => '10',
                'down_burst' => '40',
                'up_burst' => '30',
                'down_threshold' => '60',
                'up_threshold' => '50',
                'down_time' => '80',
                'up_time' => '70',
                'priority' => '2',
                'down_at_limit' => '20',
                'up_at_limit' => '10',
                'controller_id' => '1',
                'deleted' => '0',
                'api_id' => NULL,
            ],
            [
                'id' => '3',
                'name' => 'profile-3mb',
                'local_address' => '190.210.176.1',
                'dns_server' => '8.8.8.8',
                'rate_limit_string' => '10k/20k 30k/40k 50k/60k 70/80 1 10k/20k',
                'queue_type' => 'default-small',
                'down' => '20',
                'up' => '10',
                'down_burst' => '40',
                'up_burst' => '30',
                'down_threshold' => '60',
                'up_threshold' => '50',
                'down_time' => '80',
                'up_time' => '70',
                'priority' => '1',
                'down_at_limit' => '20',
                'up_at_limit' => '10',
                'controller_id' => '2',
                'deleted' => '0',
                'api_id' => NULL,
            ],
            [
                'id' => '4',
                'name' => 'profile-6mb',
                'local_address' => '190.210.176.1',
                'dns_server' => '8.8.8.8',
                'rate_limit_string' => '10k/20k 30k/40k 50k/60k 70/80 2 10k/20k',
                'queue_type' => 'default-small',
                'down' => '20',
                'up' => '10',
                'down_burst' => '40',
                'up_burst' => '30',
                'down_threshold' => '60',
                'up_threshold' => '50',
                'down_time' => '80',
                'up_time' => '70',
                'priority' => '2',
                'down_at_limit' => '20',
                'up_at_limit' => '10',
                'controller_id' => '2',
                'deleted' => '0',
                'api_id' => NULL,
            ],
        ];
        $table = $this->table('t_profiles');
        $table->insert($data)->save();
        
        
        #POOLS
        $data = [
            [
                'id' => '1',
                'name' => 'pool-3mb',
                'addresses' => '10.20.30.0/24',
                'min_host' => '10.20.30.1',
                'max_host' => '10.20.30.254',
                'next_pool_id' => NULL,
                'controller_id' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '2',
                'name' => 'pool-6mb',
                'addresses' => '10.20.40.0/24',
                'min_host' => '10.20.40.1',
                'max_host' => '10.20.40.254',
                'next_pool_id' => NULL,
                'controller_id' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '3',
                'name' => 'pool-3mb',
                'addresses' => '10.20.50.0/24',
                'min_host' => '10.20.50.1',
                'max_host' => '10.20.50.254',
                'next_pool_id' => NULL,
                'controller_id' => '2',
                'deleted' => '0',
            ],
            [
                'id' => '4',
                'name' => 'pool-6mb',
                'addresses' => '10.20.60.0/24',
                'min_host' => '10.20.60.1',
                'max_host' => '10.20.60.254',
                'next_pool_id' => NULL,
                'controller_id' => '2',
                'deleted' => '0',
            ],
        ];
        $table = $this->table('t_pools');
        $table->insert($data)->save();
        
        #PLANS
        $data = [
            [
                'id' => '1',
                'controller_id' => '1',
                'profile_id' => '1',
                'pool_id' => '1',
                'service_id' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '2',
                'controller_id' => '1',
                'profile_id' => '2',
                'pool_id' => '2',
                'service_id' => '2',
                'deleted' => '0',
            ],
            [
                'id' => '3',
                'controller_id' => '2',
                'profile_id' => '3',
                'pool_id' => '3',
                'service_id' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '4',
                'controller_id' => '2',
                'profile_id' => '4',
                'pool_id' => '4',
                'service_id' => '2',
                'deleted' => '0',
            ],
        ];
        $table = $this->table('t_plans');
        $table->insert($data)->save();
        
        
    }
}
