<?php
use Migrations\AbstractSeed;

/**
 * Clases seed.
 */
class ClasesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'clase' => 'Accounts',
                'display_name' => 'Cuentas',
            ],
            [
                'id' => '2',
                'clase' => 'ActionLog',
                'display_name' => 'Registro de Acciones',
            ],
            [
                'id' => '3',
                'clase' => 'Articles',
                'display_name' => 'AritculSos',
            ],
            [
                'id' => '4',
                'clase' => 'ArticlesStores',
                'display_name' => 'Articulos Depositos',
            ],
            [
                'id' => '5',
                'clase' => 'CashEntities',
                'display_name' => 'Cajas',
            ],
            [
                'id' => '6',
                'clase' => 'Connections',
                'display_name' => 'Conexiones',
            ],
            [
                'id' => '7',
                'clase' => 'Controllers',
                'display_name' => 'Controladores',
            ],
            [
                'id' => '8',
                'clase' => 'CreditNotes',
                'display_name' => 'Nota de Credito',
            ],
            [
                'id' => '9',
                'clase' => 'DebitNotes',
                'display_name' => 'Nota de Debito',
            ],
            [
                'id' => '10',
                'clase' => 'Customers',
                'display_name' => 'Clientes',
            ],
            [
                'id' => '11',
                'clase' => 'Debts',
                'display_name' => 'Deudas',
            ],
            [
                'id' => '12',
                'clase' => 'DiaryBook',
                'display_name' => '(Contable)',
            ],
            [
                'id' => '13',
                'clase' => 'ErrorLog',
                'display_name' => 'Registro de Errores',
            ],
            [
                'id' => '14',
                'clase' => 'Home',
                'display_name' => 'Inicio',
            ],
            [
                'id' => '15',
                'clase' => 'Instalations',
                'display_name' => 'Instalaciones',
            ],
            [
                'id' => '16',
                'clase' => 'Invoices',
                'display_name' => 'Facturas',
            ],
            [
                'id' => '17',
                'clase' => 'Packages',
                'display_name' => 'Paquetes',
            ],
            [
                'id' => '18',
                'clase' => 'PaymentMethods',
                'display_name' => 'Forma de Pagos',
            ],
            [
                'id' => '19',
                'clase' => 'Payments',
                'display_name' => 'Pagos',
            ],
            [
                'id' => '20',
                'clase' => 'Products',
                'display_name' => 'Productos',
            ],
            [
                'id' => '21',
                'clase' => 'RoadMap',
                'display_name' => 'Hoja de Ruta',
            ],
            [
                'id' => '22',
                'clase' => 'Roles',
                'display_name' => 'Privilegios',
            ],
            [
                'id' => '23',
                'clase' => 'Seating',
                'display_name' => 'Asientos',
            ],
            [
                'id' => '24',
                'clase' => 'Services',
                'display_name' => 'Servicos',
            ],
            [
                'id' => '25',
                'clase' => 'Stores',
                'display_name' => 'Depositos',
            ],
            [
                'id' => '26',
                'clase' => 'Users',
                'display_name' => 'Usuarios',
            ],
            [
                'id' => '27',
                'clase' => 'Zone',
                'display_name' => 'Zona',
            ],
            [
                'id' => '28',
                'clase' => 'Settings',
                'display_name' => 'Configuraciones',
            ],
            [
                'id' => '29',
                'clase' => 'MessageTemplates',
                'display_name' => 'Templates de AViso',
            ],
            [
                'id' => '30',
                'clase' => 'ConnectionsHasMessageTemplates',
                'display_name' => 'Avisos Enviados',
            ],
            [
                'id' => '31',
                'clase' => 'TemplateResources',
                'display_name' => 'Recursos Avisos',
            ],
            [
                'id' => '32',
                'clase' => 'Importers',
                'display_name' => 'Importador',
            ],
            [
                'id' => '33',
                'clase' => 'Tickets',
                'display_name' => 'Tickets',
            ],
            [
                'id' => '34',
                'clase' => 'TicketsRecords',
                'display_name' => 'Registro de Tickets',
            ],
            [
                'id' => '35',
                'clase' => 'CustomLogs',
                'display_name' => 'Log de Error',
            ],
            [
                'id' => '36',
                'clase' => 'IntOutCashsEntities',
                'display_name' => 'Partes de Cajas',
            ],
        ];

        $table = $this->table('clases');
        $table->insert($data)->save();
    }
}
