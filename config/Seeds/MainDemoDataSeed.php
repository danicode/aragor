<?php
use Migrations\AbstractSeed;

/**
 * Accounts seed.
 */
class MainDemoDataSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
       
        #CONTROLLERS
        $data = [
            [
                'id' => '1',
                'name' => 'N5',
                'template' => 'template-a',
                'description' => '',
                'lat' => NULL,
                'lng' => NULL,
                'ip' => '190.210.176.55',
                'port' => '2010',
                'username' => 'admin',
                'password' => '',
                'enabled' => '1',
                'created' => '2017-09-04 11:53:54',
                'modified' => '2017-09-04 11:55:12',
                'trademark' => 'mikrotik',
                'message_method' => 'met_a',
                'deleted' => '0',
            ],
            [
                'id' => '2',
                'name' => 'N6',
                'template' => 'template-a',
                'description' => '',
                'lat' => NULL,
                'lng' => NULL,
                'ip' => '190.210.176.55',
                'port' => '2011',
                'username' => 'admin',
                'password' => '',
                'enabled' => '1',
                'created' => '2017-09-04 11:54:25',
                'modified' => '2017-09-04 12:01:35',
                'trademark' => 'mikrotik',
                'message_method' => 'met_a',
                'deleted' => '0',
            ],
        ];
        $table = $this->table('controllers');
        $table->insert($data)->save();
        
         $data = [
            [
                'code' => '120000001',
                'name' => '00001 - 12345678 - CLIENTE DE PRUEBA',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
        ];

        $table = $this->table('accounts');
        $table->insert($data)->save();
        
        
        #CUSTOMERS
        $data = [
            [
                'code' => '1',
                'status' => 'CP',
                'name' => 'CLIENTE DE PRUEBA',
                'debt_month' => '0.00',
                'address' => 'Calle 123',
                'responsible' => '5',
                'doc_type' => '96',
                'ident' => '12345678',
                'phone' => '456789456',
                'phone_alt' => '',
                'asked_router' => '0',
                'availability' => 'Cualquiera',
                'email' => NULL,
                'comments' => '',
                'is_presupuesto' => '1',
                'daydue' => '10',
                'created' => '2017-09-04 12:05:13',
                'modified' => '2017-09-04 12:05:13',
                'deleted' => '0',
                'clave_portal' => 'yk270t',
                'seller' => 'vendedor',
                'plan_ask' => '1',
                'zone_id' => '1',
                'user_id' => '1',
                'account_code' => '120000001',
                'old' => NULL,
            ],
        ];
        $table = $this->table('customers');
        $table->insert($data)->save();
        
        
        #SERVICES
        $data = [
            [
                'id' => '1',
                'name' => 'P3M',
                'description' => '',
                'price' => '500.00',
                'created' => '2017-09-04 11:58:19',
                'modified' => '2017-09-04 11:58:19',
                'enabled' => '1',
                'isFree' => '0',
                'account_code' => '311000002',
                'deleted' => '0',
            ],
            [
                'id' => '2',
                'name' => 'P6M',
                'description' => '',
                'price' => '1000.00',
                'created' => '2017-09-04 11:58:29',
                'modified' => '2017-09-04 11:58:29',
                'enabled' => '1',
                'isFree' => '0',
                'account_code' => '311000003',
                'deleted' => '0',
            ],
        ];
        $table = $this->table('services');
        $table->insert($data)->save();
        
    }
}
