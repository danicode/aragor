<?php
use Migrations\AbstractSeed;

/**
 * PaymentMethods seed.
 */
class PaymentMethodsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Contado',
                'description' => '',
                'created' => '2017-09-04 14:36:32',
                'enabled' => '1',
            ],
        ];

        $table = $this->table('payment_methods');
        $table->insert($data)->save();
    }
}
