#!/bin/bash
# -*- ENCODING: UTF-8 -*-

echo "SET PROYECT PRODUCTION"

#verifico si el sistema esta en modo debug

file="../src/Template/Error/_d_error400.ctp"

if [ -f "$file" ]
then
    echo "el sistema ya esta en produccion."
else

    #cambio a false debug en el famawork
    sed -i '12s/true/false/' ../config/app.php 

    #resguardo los templates de debug
    mv ../src/Template/Error/error400.ctp ../src/Template/Error/_d_error400.ctp
    mv ../src/Template/Error/error500.ctp ../src/Template/Error/_d_error500.ctp
    
    #dejo disponible los template de produccion
    mv ../src/Template/Error/_p_error400.ctp ../src/Template/Error/error400.ctp
    mv ../src/Template/Error/_p_error500.ctp ../src/Template/Error/error500.ctp

fi

exit