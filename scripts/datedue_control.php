<?php

require_once 'routeros/PEAR2_Net_RouterOS-1.0.0b5.phar';
use PEAR2\Net\RouterOS;
use PEAR2\Net\RouterOS\SocketException;
use PEAR2\Net\RouterOS\DataFlowException;


function log_file($text){
    echo $text;
    $flog = fopen("datedue_control_log.txt","a");
    fwrite($flog, date('Y-m-d H:i:s') . ' -- ' . $text); 
    fclose($flog);
}

log_file("************* datedue_control start ! ************** \r\n"); 

function connectDatabase(){
    $link = mysql_connect('localhost', 'jalt', 'clave') or die('No se pudo conectar: ' . mysql_error());
    log_file("database Connected successfully \r\n"); 
    mysql_select_db('ispbraindb') or die('No se pudo seleccionar la base de datos');
    
    return $link;
        
} 


//conexion con la base
$link = connectDatabase();

//obtengo la configuracion del sistema
$paraments = getRow('paraments', 1);



if(!$paraments['avisos_auto']){
    $log =  "El control de aviso automatico esta desactivado.\r\n";
    log_file($log); 
    closeConnectionDatabase($link);
    return false;
}

if(!$paraments['integration_enable']){
    $log =  "La conexion con el router esta desactivado.\r\n";
    log_file($log); 
    closeConnectionDatabase($link);
    return false;
}

//creo los regitro de avisos de cada cliente moroso con sus respectivas conexiones
$messages = getMessages();

if(!$messages){
    $log =  "No hay cambios.\r\n";
    log_file($log); 
    //updateRulesProxy();
    closeConnectionDatabase($link);
    return true;
}

foreach($messages as $message){
    
    if($message['type'] == 'corte'){
        
        $message_datebase = getRow('messages', $message['ip'], 'ip');
       
       //si exite registro y es de tipo venc debo cambia el tipo corte
        if($message_datebase && $message_datebase['type'] == 'venc'){
           
            if(!updateRow($link, 'messages', $message, $message_datebase['id'])){
               $log =  "error al intentar actualizar el registro del aviso de la base. \r\n";
               log_file($log); 
               return false;
            }
            
            if(!updateRow($link, 'connections', ['enabled' => 0], $message['connection']['id'])){
               $log =  "error al intentar actualizar el registro del aviso de la base. \r\n";
               log_file($log); 
               return false;
            }
            
            if(!removeProxyAccess($message)){
                $log =  "error al intentar remover el proxy access de venc el mikrotik. \r\n";
                log_file($log); 
                return false;
            }
            if(!removeIpFirewall($message, 'Venc')){
                $log =  "error al intentar remover el address list de venc el mikrotik. \r\n";
                log_file($log); 
                return false;
            }
            
            if(!addIpFirewall($message, 'Cortes')){
               $log =  "error al intentar agregar el el ip firewall Cortes en el mikrotik. \r\n";
               log_file($log); 
               return false;
            }
            
            if(!addProxyAccess($message)){
                $log =  "error al intentar agregar el proxy access de corte el mikrotik. \r\n";
                log_file($log); 
                return false;
            }
            
            
            
        }
       
       
        //si exite registro no exite en la base debo agregar en la base, el ip firewall
        if(!$message_datebase){
           
            if(!insertRow($link, 'messages', $message)){
               $log =  "error al intentar agregar el registro del aviso en la base. \r\n";
               log_file($log); 
               return false;
            }
           
            if(!addIpFirewall($message, 'Cortes')){
               $log =  "error al intentar agregar el el ip firewall Cortes en el mikrotik. \r\n";
               log_file($log); 
               return false;
            }
            
            if(!addProxyAccess($message)){
                $log =  "error al intentar agregar el proxy access el mikrotik. \r\n";
                log_file($log); 
                return false;
            }
           
            if(!updateRow($link, 'connections', ['enabled' => 0], $message['connection']['id'])){
               $log =  "error al intentar actualizar el registro del aviso de la base. \r\n";
               log_file($log); 
               return false;
            }
           
           
       }
    }else if($message['type'] == 'venc'){
        
       $message_datebase = getRow('messages', $message['ip'], 'ip');
       
       //si el registro SI exite en al base pero no es de tipo venc debo agregar , y el ip firewall tb (debe ser tipo INFO)
       if($message_datebase && $message_datebase['type'] != 'venc'){
           
           if(!insertRow($link, 'messages', $message)){
               $log =  "error al intentar agregar el registro del aviso en la base. \r\n";
               log_file($log); 
               return false;
           }
           
           if(!addIpFirewall($message, 'Venc')){
               $log =  "error al intentar agregar el el ip firewall Venc en el mikrotik. \r\n";
               log_file($log); 
               return false;
           }
           
           if(!addProxyAccess($message)){
                $log =  "error al intentar agregar el proxy access el mikrotik. \r\n";
                log_file($log); 
                return false;
            }
           
       }
       
       //si el registro NO exite en la base debo agregar en la base y el ip firewall tb
       if(!$message_datebase){
           
           if(!insertRow($link, 'messages', $message)){
               $log =  "error al intentar agregar el registro del aviso en la base. \r\n";
               log_file($log); 
               return false;
           }
           
           if(!addIpFirewall($message, 'Venc')){
               $log =  "error al intentar agregar el el ip firewall  Venc en el mikrotik. \r\n";
               log_file($log); 
               return false;
           }
           
            if(!addProxyAccess($message)){
                $log =  "error al intentar agregar el proxy access el mikrotik. \r\n";
                log_file($log); 
                return false;
            }
       }
    }
    
    $log =  "Customer code: ".$message['customer_code'] ." - Aviso tipo: " . $message['type'] ." \r\n" ;
    log_file($log); 
    
    if(!log_database($link,$message)){
        return false;
    }
}

//actualizo los archivos
//updateRulesProxy();

//shell_exec('/etc/init.d/squid3 reload');


//cierro la conexion con la base
closeConnectionDatabase($link);

return true;


/**
FUNCTIONS
*/


function log_database($link, $message){
    
    
    $log_data = [
        'action' => 'Aviso - tipo: ' . $message['type'],
        'created' => date('Y-m-d H:i:s'),
        'menu' => 'Scripts/Control de Morosos',
        'customer_code' =>  intval($message['customer_code'])
        ];
    
    if(!insertRow($link, 'logs' , $log_data)){
         $log =  "error al intentar registrar el log. \r\n";
         log_file($log); 
         return false;
    }
    
    return true;
    
}

function getRow($table, $value, $col = 'id'){
    
    if(gettype($value) == 'string'){
        $value = "'".$value."'";
    }
    
    $query = "select * from $table where $col = $value" ;
    
    $log =  "getRow: $query \r\n"; 
    //log_file($log); 
    
    $result_query = mysql_query($query) or die('Consulta fallida: ' . mysql_error() . "\r\n");
    
    if(!$result_query){
        $log =  "Sin resultado en tabla $table columna $col valor $value \r\n";
        //log_file($log); 
        return false;
    }
 
    $row = mysql_fetch_assoc($result_query);

    if(!$row){
        $log =  "Sin resultado en tabla $table columna $col valor $value\r\n";
        //log_file($log); 
        return false;
    }
    
    return $row;
}

function getAll($table){
    
    $query = "select * from $table where deleted = 0";
    $result_query = mysql_query($query) or die('Consulta fallida: ' . mysql_error(). "\r\n");
    
    if(!$result_query){
        $log =  "Sin resultado en tabla $table \r\n";
        log_file($log); 
        return false;
    }
    
    $rows = [];
    
    while ($row = mysql_fetch_assoc($result_query)) {
        $rows[] = $row;
    }

    if(count($rows) == 0){
        $log =  "Sin resultado en tabla $table \r\n";
        log_file($log); 
        return false;
    }
    
    return $rows;
}

function connectRouterMikrotik($node_id, $timeout = 5){
     
    $node_conf = getRow('nodes', $node_id); 

    if(!$node_conf) return false;
    
    $client = null;
    
    try{
        
      $client = new RouterOS\Client(
        $node_conf['ip'],
        $node_conf['username'],
        $node_conf['password'],
        $node_conf['port'],
        false, $timeout);

        
    }catch(SocketException $e){
        
        if($e->getCode() == SocketException::CODE_CONNECTION_FAIL){
            $log =  'No se pudo conectar al Router. Nodo: '. $node_conf['name'] . "\r\n";
            log_file($log); 
            return false;
        }else{
            var_dump($e);
        }
    }catch(DataFlowException $e){
        
        if($e->getCode() == DataFlowException::CODE_INVALID_CREDENTIALS ){
            $log =  'Credenciales incorrectas. Nodo: '. $node_conf['name'] . "\r\n";
            log_file($log); 
            return false;
        }else{
            var_dump($e);
        }
    }
    
    return $client;
}

// function addSecret(){
    
//     $client = connectRouterMikrotik(1);

//     if(!$client) return false;
    
//     $speeds = getAll('speeds');
    
//     if(!$speeds) return false;
    
    
//     $util = new RouterOS\Util($client);
//     $util->setMenu('/ppp secret');
    
//     $secret = [];
//     $secret['name'] = 'pepe5';
//     $secret['password'] = '1234';
//     $secret['remote-address'] = '10.200.45.11';
//     $secret['profile'] = $speeds[0]['name'];
//     $secret['service'] = 'pppoe';
//     $secret['comment'] = 'comentario';
    
//     $util->add($secret);
    
//     $count = $util->count(COUNT_NORMAL, RouterOS\Query::where('remote-address', $secret['remote-address']));
 
//     if($count > 0){
//         return true;
//     }
    
//     return false;
// }

function isDebtor($connection, $debtors){
    
    foreach($debtors as $debtor){
        //$log =  "conn code: " . $connection['customer_code'] . "debtor code: " . $debtor['customer_code'] . "\r\n";
        
        if($connection['customer_code'] == $debtor['code']){
            return $connection;
        }
    }
    return false;
}


/**
 * obntiene todfos los clientes cons us respectivos movimeintos contables
 * verifica fecha si las fechas de vencieintos son menores al fecha actual
 * si tiene deudas vencidas calcula la sumatoria
 * retorna una lista con lso clienets y el valor de sus deudas vencidas
*/
function getCustomersOverdueDdebt(){
    
    $query_debts = "select customers.code, debts.subtotal, debts.duedate 
    from customers 
    inner join debts on customers.code = debts.customer_code 
    where customers.deleted = 0 and debts.deleted = 0 and debts.doc_id IS NULL" ;
    
    $customers_debts = mysql_query($query_debts) or die('Consulta fallida: ' . mysql_error());
    
    $query_bills = "select customers.code, docs.duedate, docs.total
    from customers 
    inner join docs on customers.code = docs.customer_code
    where customers.deleted = 0 and docs.deleted = 0 
    and (docs.type_comp = 'FX' 
    or docs.type_comp = 'FA'  
    or docs.type_comp = 'FB'  
    or docs.type_comp = 'FC')" ;
        
    $customers_bills = mysql_query($query_bills) or die('Consulta fallida: ' . mysql_error());
    
    $query_creditNote = "select customers.code, docs.total
    from customers 
    inner join docs on customers.code = docs.customer_code
    where customers.deleted = 0 and docs.deleted = 0  
    and docs.type_comp = 'CX'" ;
        
    $customers_credits = mysql_query($query_creditNote) or die('Consulta fallida: ' . mysql_error());
    
    $query_payments = "select customers.code, payments.created, payments.import
    from customers 
    inner join payments on customers.code = payments.customer_code
    where customers.deleted = 0 and payments.deleted = 0" ;
    
    $customers_payments = mysql_query($query_payments) or die('Consulta fallida: ' . mysql_error());
    
    $customers_overdue_debt = [];
    $today = date("Y-m-d");   
    
    $debt_due = 0;

    while ($customer_debts = mysql_fetch_array($customers_debts, MYSQL_ASSOC)) {
        
        $customers_overdue_debt[$customer_debts['code']]['code'] = $customer_debts['code'];
     
        if(date("Y-m-d", strtotime($customer_debts['duedate'])) < $today ){ //si la fecha de hoy es mayor al vencimiento de la deuda
            $customers_overdue_debt[$customer_debts['code']]['debt']  += $customer_debts['subtotal'];
        }
    }
    
    while ($customer_bills = mysql_fetch_array($customers_bills, MYSQL_ASSOC)) {
        
        $customers_overdue_debt[$customer_bills['code']]['code'] = $customer_bills['code'];
     
        if(date("Y-m-d", strtotime($customer_bills['duedate'])) < $today ){ //si la fecha de hoy es mayor al vencimiento de la deuda
            $customers_overdue_debt[$customer_bills['code']]['debt'] += $customer_bills['total'];
        }
    }
    
    while ($customer_payments = mysql_fetch_array($customers_payments, MYSQL_ASSOC)) {
        
        $customers_overdue_debt[$customer_payments['code']]['code'] = $customer_payments['code'];
     
        if(date("Y-m-d", strtotime($customer_payments['created'])) < $today ){ //si la fecha de hoy es mayor al vencimiento de la deuda
            $customers_overdue_debt[$customer_payments['code']]['debt'] -= $customer_payments['import'];
        }
    }
    
    while ($customer_credits = mysql_fetch_array($customers_credits, MYSQL_ASSOC)) {
        
        $customers_overdue_debt[$customer_credits['code']]['code'] = $customer_credits['code'];
        $customers_overdue_debt[$customer_credits['code']]['debt'] -= $customer_credits['total'];
    }
    
    //var_dump($customers_overdue_debt);
    
    //recorro ma mostrar las deudas vencida de cada cliente
    foreach ($customers_overdue_debt as $code => $cOD) {
        $cOD['debt'] = round($cOD['debt'], 2);
        $log =  'customer_code: ' . $code . ' total: ' . $cOD['debt'] . "\r\n";
        log_file($log); 
    }
    return $customers_overdue_debt; 
    
}

/**
 * recorre los coienst y sus deudas vencidas y filtra solo a los clientes que 
 * deben un valor mayor al configurado en el sistema
 * retorna la lista filtrada
 */
function getDebtors(){
    
    //obtengo de la base todos los clientes con sus respectivos movimientos contables (deudas y pagos)
    $customers_overdue_debt = getCustomersOverdueDdebt();
    
    //obtengo la configuracion del sistema
    $paraments = getRow('paraments', 1);
    
    $log =  "Deudores: \r\n";
    log_file($log); 

    $debtors = [];
    
    foreach ($customers_overdue_debt as $customer_code=> $cOD) {
        
        $cOD['debt'] = abs(round($cOD['debt'], 2));
        
        if($paraments['debt_cut'] < $cOD['debt']){
            $debtors[] = $cOD;
             $log =  "customer_code: $customer_code debt_cut: " . $paraments['debt_cut'] . " < debt: " . $cOD['debt'] . "\r\n";
             log_file($log); 
        }
    }
    
    return $debtors;
}

/**
 * recorre la lista de cliente deudores y obtien todas sus conexiones habilitadas
 * y que router pertenece cada conexion
*/
function getMessages(){
    
    //obtengo la configuracion del sistema
    $paraments = getRow('paraments', 1);
    
    //obtengo los deudores
    $debtors = getDebtors();
    
    //obtengo solo el dia actual del corriente mes
    $today = date("d");   
    
    $messages = null;

    //obtengo conexiones no eliminadas y habilitadas y sus respectivos nodos 
    $query = "select connections.id, connections.customer_code, connections.ip, nodes.id as node_id from connections 
                inner join networks on connections.network_id = networks.id
                inner join nodes on networks.node_id = nodes.id
                where connections.deleted = 0 and connections.enabled = 1";
    
    $customers_connections = mysql_query($query) or die('Consulta fallida: ' . mysql_error());
    
    /**
     * Calculo la deuda vencida de cada cliente y resguardo en un array.
    */
    while ($connection = mysql_fetch_array($customers_connections, MYSQL_ASSOC)) {
      
        $message = [];
      
        //verifico si la conexion le pertenece a un deudor
        if(isDebtor($connection, $debtors)){
            
            $message['connection_id'] = $connection['id'];
            $message['connection'] = $connection;
            $message['ip'] = $connection['ip'];
            $message['created'] = date('Y-m-d H:i:s');
            $message['deleted'] = 0;
            $message['view'] = 0;
            $message['customer_code'] = intval($connection['customer_code']);
            
            //$log =  "today: $today  conecction_locker_date ". $paraments['conecction_locker_date'] . "\r\n";
            
            //si la fecha actual es mayor a la de corte 
            if($today >= $paraments['conecction_locker_date']){
                $message['type'] = 'corte';
                $log =  "hay que cortar a customer_code: " .$connection['customer_code'] . " ip: " . $connection['ip'] . "\r\n";
                log_file($log); 
            }else{
                $message['type'] = 'venc';
                $log =  "hay que avisar a customer_code: " .$connection['customer_code'] . " ip: " . $connection['ip'] . "\r\n";
                log_file($log); 
            }
            
            $messages[] = $message;
        }
    }
     
    return  $messages;  
        
}

function addIpFirewall($message, $type){
    
    //obtengo la configuracion del sistema
    $paraments = getRow('paraments', 1);
        
    if(!$paraments['integration_enable']){
        $log =  "La integración con el Router esta desabilitada.\r\n";
        log_file($log); 
        return false;
        
    }
    
    $client = connectRouterMikrotik($message['connection']['node_id']);
    if(!$client) return false;
    
    $util = new RouterOS\Util($client);
    $util->setMenu('/ip firewall address-list');
    
    $query = RouterOS\Query::where('address', $message['connection']['ip'])->andWhere('list', $type);

    //si no existe
    if($util->count(COUNT_NORMAL, $query) == 0){
        
        $customer = getRow('customers',$message['customer_code'], 'code');
        
        if(!$customer){
            $log =  "No se pudo encontrar el cliente. \r\n"; 
            log_file($log); 
            return false;
        }
        
        $data = [
             'list' => $type,
             'address' => $message['connection']['ip'],
             'comment' => $customer['code'] .' - '.$customer['last_name'] .' '.$customer['first_name']
             ];
             
        $util->add($data);
        
        if($util->count(COUNT_NORMAL, $query) == 0){
            $log =  "Error al Intentar agregar el bloqueo al Router. \r\n"; 
            log_file($log); 
            return false;
        }
    }
    
    return true;
}

function addProxyAccess($message){
    
     //obtengo la configuracion del sistema
    $paraments = getRow('paraments', 1);
        
    if(!$paraments['integration_enable']){
        $log =  "La integración con el Router esta desabilitada.\r\n";
        log_file($log); 
        return false;
    }
    
    $client = connectRouterMikrotik($message['connection']['node_id']);
    if(!$client) return false;
    
    $util = new RouterOS\Util($client);
    $util->setMenu('/ip proxy access');
    
    $type = 'VENC';
    $port = ':81/venc.html';
    if($message['type'] == 'corte'){
         $port = ':81/corte.html';
         $type = 'CORTE';
    }
    
    $query = RouterOS\Query::where('src-address', $message['connection']['ip'])
                            ->andWhere('redirect-to', $paraments['ip_server'] . $port);
                            
    //var_dump($query);

    //si no existe
    if($util->count(COUNT_NORMAL, $query) == 0){
        
        $customer = getRow('customers',$message['customer_code'], 'code');
        
        if(!$customer){
            $log =  "No se pudo encontrar el cliente. \r\n"; 
            log_file($log); 
            return false;
        }
        
        $data = [
             'src-address' => $message['connection']['ip'],
             'dst-port' => 80,
             'action' => 'deny',
             'redirect-to' => $paraments['ip_server'] . $port,
             'comment' => $customer['code'] .' - '.$customer['last_name'] .' '.$customer['first_name'] . ' - ' . $type
             ];
             
        $util->add($data);
        
        if($util->count(COUNT_NORMAL, $query) == 0){
            $log =  "Error al Intentar agregar el proxy access al Router. \r\n"; 
            log_file($log); 
            return false;
        }
    }
    
    return true;
    
}

/**
 * Solo se usa para eliominar el proxy access de  tupo vencimiento por que 
 * pasa cuando tiene que avisar de corte y tien un aviso de vencimineto
*/
function removeProxyAccess($message){
    
    //obtengo la configuracion del sistema
    $paraments = getRow('paraments', 1);
        
    if(!$paraments['integration_enable']){
        $log =  "La integración con el Router esta desabilitada.\r\n";
        log_file($log); 
        return false;
    }
    
    $client = connectRouterMikrotik($message['connection']['node_id']);
    if(!$client) return false;
    
    $util = new RouterOS\Util($client);
    $util->setMenu('/ip proxy access');
    

    $port = ':81/venc.html';
    
    $query = RouterOS\Query::where('src-address', $message['connection']['ip'])
                            ->andWhere('redirect-to', $paraments['ip_server'] . $port);

    if($util->count(COUNT_NORMAL, $query) > 0){
      
        $util->remove($query);
        
        if($util->count(COUNT_NORMAL, $query) > 0){
            $log =  "Erro al remover el proxy access del mikrotik.\r\n";
            log_file($log); 
            return false;
        }
    }
    
    return true;
    
}

function removeIpFirewall($message, $type){
    
    //obtengo la configuracion del sistema
    $paraments = getRow('paraments', 1);
        
    if(!$paraments['integration_enable']){
        $log =  "La integración con el Router esta desabilitada.\r\n";
        log_file($log); 
        return false;
    }
    
    $client = connectRouterMikrotik($message['connection']['node_id']);
    if(!$client) return false;
    
    $util = new RouterOS\Util($client);
    $util->setMenu('/ip firewall address-list');

    $query = RouterOS\Query::where('address', $message['connection']['ip'])->andWhere('list', $type);

    if($util->count(COUNT_NORMAL, $query) > 0){
      
        $util->remove($query);
        
        if($util->count(COUNT_NORMAL, $query) > 0){
            $log =  "Erro al remover el ip firewall $type del mikrotik.\r\n";
            log_file($log); 
            return false;
        }
    }
    
    return true;
}

function insertRow($link, $table, $data){
    
    $sql = "INSERT INTO $table " ;
  
    $columns = '';
    $values = '';
    $i = 0;
    foreach($data as $col => $val){
         
         if($col == 'connection') continue;
         
         if($i > 0){
            $columns .= ','; 
            $values .= ','; 
         }
         $columns .= "`".$col."`";
         if(gettype($val) == 'string'){
             $values .= "'".$val."'";
         }else{
             $values .= $val;
         }
         $i++;
    }
    
    $sql .= "($columns) VALUES ($values)";

    $log =  "insertRow: sql: $sql \r\n";
    //log_file($log); 

    $retval = mysql_query( $sql, $link );
    
    if(! $retval ) {
        $log =  'Could not enter data: ' . mysql_error() . "\r\n";
        log_file($log); 
        return false;
    }
    
    return $retval;
}

function removeRow($link, $table, $val, $col = 'id' ){
    
    $sql = "DELETE FROM $table WHERE $col = ";
    
    if(gettype($val) == 'string'){
        $sql .= "'$val'";
    }else{
        $sql .= "$val";
    }
    
    $log =  "removeRow: sql: $sql \r\n";
   // log_file($log); 
    
    $retval = mysql_query( $sql, $link );
    
    if(! $retval ) {
        $log =  'Could not enter data: ' . mysql_error() . "\r\n";
        log_file($log); 
        return false;
    }
    
    return true;
} 

function updateRow($link, $table, $data, $id){
    
    $sql = "UPDATE $table SET " ;
  
    $columns = '';
    $values = '';
    $i = 0;
    foreach($data as $col => $val){
         
         if($col == 'connection') continue;
         
         if($i > 0){
            $sql .= ','; 
         }
         
         if(gettype($val) == 'string'){
             $val = "'".$val."'";
         }
         
         $sql .= $col .' = '. $val;
         $i++;
    }
    
    $sql .= " WHERE id = " . $id;

    $log =  "updateRow: sql: $sql \r\n";
    //log_file($log); 

    $retval = mysql_query( $sql, $link );
    
    if(! $retval ) {
        $log =  'Could not enter data: ' . mysql_error() . "\r\n";
        log_file($log); 
        return false;
    }
    
    return $retval;
    
}


function closeConnectionDatabase($link){
    mysql_close($link);
}

   

?>

