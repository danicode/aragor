#!/bin/bash
# -*- ENCODING: UTF-8 -*-

echo "SET PROYECT DEBUG"

#verifico si el sistema esta en produccion

file="../src/Template/Error/_p_error400.ctp"

if [ -f "$file" ]
then
    echo "el sistema ya esta en modo debug."
else

    #cambio a true debug en el famawork
    sed -i '12s/false/true/' ../config/app.php 

    #resguardo los templates de produccion
	mv ../src/Template/Error/error400.ctp ../src/Template/Error/_p_error400.ctp
    mv ../src/Template/Error/error500.ctp ../src/Template/Error/_p_error500.ctp
    
    #dejo disponible los template de debug
    mv ../src/Template/Error/_d_error400.ctp ../src/Template/Error/error400.ctp
    mv ../src/Template/Error/_d_error500.ctp ../src/Template/Error/error500.ctp
	
fi

exit