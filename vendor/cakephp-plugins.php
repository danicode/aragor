<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'Ajax' => $baseDir . '/vendor/dereuromark/cakephp-ajax/',
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'BootstrapUI' => $baseDir . '/vendor/friendsofcake/bootstrap-ui/',
        'Captcha' => $baseDir . '/vendor/ivanamat/cakephp3-captcha/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'LoggingPack' => $baseDir . '/plugins/LoggingPack/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'SocialShare' => $baseDir . '/vendor/drmonkeyninja/cakephp-social-share/'
    ]
];