<?php
$type = 'TrueType';
$name = 'SmallTypeWriting';
$desc = array('Ascent'=>876,'Descent'=>-200,'CapHeight'=>876,'Flags'=>33,'FontBBox'=>'[-61 -402 616 876]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>1000);
$up = -100;
$ut = 50;
$cw = array(
	chr(0)=>1000,chr(1)=>1000,chr(2)=>1000,chr(3)=>1000,chr(4)=>1000,chr(5)=>1000,chr(6)=>1000,chr(7)=>1000,chr(8)=>1000,chr(9)=>1000,chr(10)=>1000,chr(11)=>1000,chr(12)=>1000,chr(13)=>1000,chr(14)=>1000,chr(15)=>1000,chr(16)=>1000,chr(17)=>1000,chr(18)=>1000,chr(19)=>1000,chr(20)=>1000,chr(21)=>1000,
	chr(22)=>1000,chr(23)=>1000,chr(24)=>1000,chr(25)=>1000,chr(26)=>1000,chr(27)=>1000,chr(28)=>1000,chr(29)=>1000,chr(30)=>1000,chr(31)=>1000,' '=>555,'!'=>555,'"'=>555,'#'=>555,'$'=>555,'%'=>555,'&'=>555,'\''=>555,'('=>555,')'=>555,'*'=>555,'+'=>555,
	','=>555,'-'=>555,'.'=>555,'/'=>555,'0'=>555,'1'=>555,'2'=>555,'3'=>555,'4'=>555,'5'=>555,'6'=>555,'7'=>555,'8'=>555,'9'=>555,':'=>555,';'=>555,'<'=>555,'='=>555,'>'=>555,'?'=>555,'@'=>555,'A'=>555,
	'B'=>555,'C'=>555,'D'=>555,'E'=>555,'F'=>555,'G'=>555,'H'=>555,'I'=>555,'J'=>555,'K'=>555,'L'=>555,'M'=>555,'N'=>555,'O'=>555,'P'=>555,'Q'=>555,'R'=>555,'S'=>555,'T'=>555,'U'=>555,'V'=>555,'W'=>555,
	'X'=>555,'Y'=>555,'Z'=>555,'['=>555,'\\'=>555,']'=>555,'^'=>555,'_'=>555,'`'=>555,'a'=>555,'b'=>555,'c'=>555,'d'=>555,'e'=>555,'f'=>555,'g'=>555,'h'=>555,'i'=>555,'j'=>555,'k'=>555,'l'=>555,'m'=>555,
	'n'=>555,'o'=>555,'p'=>555,'q'=>555,'r'=>555,'s'=>555,'t'=>555,'u'=>555,'v'=>555,'w'=>555,'x'=>555,'y'=>555,'z'=>555,'{'=>555,'|'=>555,'}'=>555,'~'=>555,chr(127)=>1000,chr(128)=>1000,chr(129)=>1000,chr(130)=>555,chr(131)=>555,
	chr(132)=>555,chr(133)=>555,chr(134)=>555,chr(135)=>555,chr(136)=>555,chr(137)=>555,chr(138)=>555,chr(139)=>555,chr(140)=>555,chr(141)=>1000,chr(142)=>1000,chr(143)=>1000,chr(144)=>1000,chr(145)=>555,chr(146)=>555,chr(147)=>555,chr(148)=>555,chr(149)=>555,chr(150)=>555,chr(151)=>555,chr(152)=>555,chr(153)=>555,
	chr(154)=>555,chr(155)=>555,chr(156)=>555,chr(157)=>1000,chr(158)=>1000,chr(159)=>555,chr(160)=>1000,chr(161)=>555,chr(162)=>555,chr(163)=>555,chr(164)=>555,chr(165)=>555,chr(166)=>555,chr(167)=>555,chr(168)=>555,chr(169)=>555,chr(170)=>555,chr(171)=>555,chr(172)=>555,chr(173)=>1000,chr(174)=>555,chr(175)=>555,
	chr(176)=>555,chr(177)=>555,chr(178)=>555,chr(179)=>555,chr(180)=>555,chr(181)=>555,chr(182)=>555,chr(183)=>555,chr(184)=>555,chr(185)=>555,chr(186)=>555,chr(187)=>555,chr(188)=>555,chr(189)=>555,chr(190)=>555,chr(191)=>555,chr(192)=>555,chr(193)=>555,chr(194)=>555,chr(195)=>555,chr(196)=>555,chr(197)=>555,
	chr(198)=>555,chr(199)=>555,chr(200)=>555,chr(201)=>555,chr(202)=>555,chr(203)=>555,chr(204)=>555,chr(205)=>555,chr(206)=>555,chr(207)=>555,chr(208)=>555,chr(209)=>555,chr(210)=>555,chr(211)=>555,chr(212)=>555,chr(213)=>555,chr(214)=>555,chr(215)=>555,chr(216)=>555,chr(217)=>555,chr(218)=>555,chr(219)=>555,
	chr(220)=>555,chr(221)=>555,chr(222)=>555,chr(223)=>555,chr(224)=>555,chr(225)=>555,chr(226)=>555,chr(227)=>555,chr(228)=>555,chr(229)=>555,chr(230)=>555,chr(231)=>555,chr(232)=>555,chr(233)=>555,chr(234)=>555,chr(235)=>555,chr(236)=>555,chr(237)=>555,chr(238)=>555,chr(239)=>555,chr(240)=>555,chr(241)=>555,
	chr(242)=>555,chr(243)=>555,chr(244)=>555,chr(245)=>555,chr(246)=>555,chr(247)=>555,chr(248)=>555,chr(249)=>555,chr(250)=>555,chr(251)=>555,chr(252)=>555,chr(253)=>555,chr(254)=>555,chr(255)=>555);
$enc = 'cp1252';
$uv = array(0=>array(0,128),128=>8364,130=>8218,131=>402,132=>8222,133=>8230,134=>array(8224,2),136=>710,137=>8240,138=>352,139=>8249,140=>338,142=>381,145=>array(8216,2),147=>array(8220,2),149=>8226,150=>array(8211,2),152=>732,153=>8482,154=>353,155=>8250,156=>339,158=>382,159=>376,160=>array(160,96));
$file = 'SmallTypeWriting.z';
$originalsize = 45612;
$subsetted = true;
?>
