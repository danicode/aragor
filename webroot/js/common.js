
  
    function pad (str, max) {
      str = str.toString();
      return str.length < max ? pad("0" + str, max) : str;
    }
      
      /** 
       * notificaciones 
       * type: warning, error, information, success
       * text: html
      */
    function generateNoty(type, text, closeWith = ['click']) {
      
        var animationIn  = '';
        var animationOut  = '';
        
        switch(type){
          case 'error':
            animationIn  = 'shake';
            animationOut  = 'fadeOut';
            break;
          case 'warning':
            animationIn  = 'shake';
            animationOut  = 'fadeOut';
            break;
          case 'success':
            animationIn  = 'bounceInRight';
            animationOut  = 'bounceOutLeft';
            break;
          case 'information': 
            animationIn  = 'bounceInRight';
            animationOut  = 'bounceOutLeft';
            break;
        }
        
  
          var n = new Noty({
              text        : text,
              type        : type,
              dismissQueue: true,
              layout      : 'topCenter', /*top, topLeft, topCenter, topRight, center, centerLeft, centerRight, bottom, bottomLeft, bottomCenter, bottomRight*/
              closeWith   : closeWith, /*['click', 'button', 'hover', 'backdrop']*/
              theme       : 'relax',
              maxVisible  : 5,
              animation   : {
                  open  : 'animated '+animationIn,
                  close : 'animated '+animationOut,
                  easing: 'swing',
                  speed : 500
              }
          });
          
          n.show();
          
          return n;
      }

    // tooltip del sistema
    $( function() {
        $( document ).tooltip({
            // hide: { effect: "explode", duration: 1000 },
            // show: { effect: "blind", duration: 800 }
        });
    });
    
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
  
  
    $(document).ready(function(){
      
      // $('body').css('overflow', 'auto');
      	
    });
    
  
   
    
    function checkStatusFilter(){
        
        var apply = false;
        
        $.each($('input.column_search'), function(){
            if($(this).val().length > 0){
                apply = true;
            }
        });
        
        if(apply){
            $('.btn-filter-column').addClass('filter-apply');
        }else{
            $('.btn-filter-column').removeClass('filter-apply');
        }
        
    }
    
    function createModalHideColumn(table, modal){
        
        $.each(table.columns()[0], function(i, val){
                  
            var column = table.column(i);
            var ckecked = column.visible() ? 'checked' : '';
            var name = $(column.header()).html();
       
            var checkbox = "";
            checkbox  = "<div class='col-3'>";
            checkbox += "   <label class='form-check-label'>";
            checkbox += "       <input type='checkbox' class='form-check-input toggle-hide-column' "+ckecked+" data-column='"+i+"' > "+name;            
            checkbox += "   </label>";
            checkbox += "</div>";
                    
            $(modal +' .checkbox-container').append(checkbox);
        });
        
         $('.toggle-hide-column').on( 'click', function (e) {
       
            var column =  table.column( $(this).data('column'));
            column.visible(  $(this).is(":checked") );
            table.columns.adjust().draw();
    
        });
        
        
        
    }
    
    
        
    function calulateIvaAndNeto(total, aliquot_per, quantity){
        
        var neto = total / ( aliquot_per + 1);
        
        neto = neto.toFixed(2);
        
        var iva = total - neto;
        
        var iva2 =  neto * aliquot_per;
        
        iva = iva.toFixed(2);
        iva2 = iva2.toFixed(2);
        
        
        // console.log('iva: ' + iva);
        
        // console.log('iva2: ' + iva2);
        
        if(iva < iva2){
            iva = iva2;
        }
        
        var price = neto / quantity;
        
        price = price.toFixed(2);
        
        var response = {
            quantity: quantity,
            price: parseFloat(price),
            sum_price: parseFloat(neto),
            sum_tax: parseFloat(iva),
            total: total
        }
        
        return response;
    }
    
    
    function number_format(number, showSymbol){

        if(showSymbol){
            return new Intl.NumberFormat("es-AR", {style: "currency", currency: "ARS", minimumFractionDigits: 2, maximumFractionDigits: 2}).format(number);
        }
        
        return new Intl.NumberFormat("es-AR",  {minimumFractionDigits: 2, maximumFractionDigits: 2}).format(number);
    }
    
        	
  
 