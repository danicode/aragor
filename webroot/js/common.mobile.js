
  
    function pad (str, max) {
      str = str.toString();
      return str.length < max ? pad("0" + str, max) : str;
    }
      
      /** 
       * notificaciones 
       * type: warning, error, information, success
       * text: html
      */
    function generateNoty(type, text, closeWith = ['click']) {
      
        var animationIn  = '';
        var animationOut  = '';
        
        switch(type){
          case 'error':
            animationIn  = 'shake';
            animationOut  = 'fadeOut';
            break;
          case 'warning':
            animationIn  = 'shake';
            animationOut  = 'fadeOut';
            break;
          case 'success':
            animationIn  = 'bounceInRight';
            animationOut  = 'bounceOutLeft';
            break;
          case 'information': 
            animationIn  = 'bounceInRight';
            animationOut  = 'bounceOutLeft';
            break;
        }
        
  
          var n = new Noty({
              text        : text,
              type        : type,
              dismissQueue: true,
              layout      : 'topCenter', /*top, topLeft, topCenter, topRight, center, centerLeft, centerRight, bottom, bottomLeft, bottomCenter, bottomRight*/
              closeWith   : closeWith, /*['click', 'button', 'hover', 'backdrop']*/
              theme       : 'relax',
              maxVisible  : 5,
              animation   : {
                  open  : 'animated '+animationIn,
                  close : 'animated '+animationOut,
                  easing: 'swing',
                  speed : 500
              }
          });
          
          n.show();
          
          return n;
      }

    // tooltip del sistema
    $( function() {
        $( document ).tooltip({
            // hide: { effect: "explode", duration: 1000 },
            // show: { effect: "blind", duration: 800 }
        });
    });
    
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
  
  
    $(document).ready(function(){
      
      $('body').css('overflow', 'auto');
      
      $('.modal').on('shown.bs.modal', function() {
        //Make sure the modal and backdrop are siblings (changes the DOM)
        $(this).before($('.modal-backdrop'));
        //Make sure the z-index is higher than the backdrop
        $(this).css("z-index", parseInt($('.modal-backdrop').css('z-index')) + 1);
      });
      	
    });
    
  
   
    
    function checkStatusFilter(){
        
        var apply = false;
        
        $.each($('input.column_search'), function(){
            if($(this).val().length > 0){
                apply = true;
            }
        });
        
        if(apply){
            $('.btn-filter-column').addClass('filter-apply');
        }else{
            $('.btn-filter-column').removeClass('filter-apply');
        }
        
    }
    
    function createModalHideColumn(table, modal){
        
        $.each(table.columns()[0], function(i, val){
                  
            var column = table.column(i);
            var ckecked = column.visible() ? 'checked' : '';
            var name = $(column.header()).html();
       
            var checkbox = "";
            checkbox  = "<div class='col-3'>";
            checkbox += "   <label class='form-check-label'>";
            checkbox += "       <input type='checkbox' class='form-check-input toggle-hide-column' "+ckecked+" data-column='"+i+"' > "+name;            
            checkbox += "   </label>";
            checkbox += "</div>";
                    
            $(modal +' .checkbox-container').append(checkbox);
        });
        
         $('.toggle-hide-column').on( 'click', function (e) {
       
            var column =  table.column( $(this).data('column'));
            column.visible(  $(this).is(":checked") );
            table.columns.adjust().draw();
    
        });
        
        
        
    }
    	
  
 