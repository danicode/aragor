#comando utiles

#para configuarra el poryecto en estado de production
cd ispbrain/scripts
./set_production.sh

#para configuarra el poryecto en estado de testing
cd ispbrain/scripts
./set_debug.sh

#creo las base de datos
mysql -u jalt -p -e "create database ispbraindb"
mysql -u jalt -p -e "create database ispbraindb_template_a"

#muestro estado de migracion
ispbrain/bin/cake migrations status 

#actualizo cambios de la base de datos
ispbrain/bin/cake migrations migrate 

#muestro estado de migracion
ispbrain/bin/cake migrations status -s migrations_template_a -c template-a

#actualizo cambios de la base de datos
ispbrain/bin/cake migrations migrate -s migrations_template_a -c template-a

#inserto data inicial
ispbrain/bin/cake migrations seed --seed UsersSeed 

ispbrain/bin/cake migrations seed --seed ClasesSeed 
ispbrain/bin/cake migrations seed --seed ActionsSystemSeed 
ispbrain/bin/cake migrations seed --seed RolesSeed 
ispbrain/bin/cake migrations seed --seed RolesUsersSeed 
ispbrain/bin/cake migrations seed --seed ActionsSystemRolesSeed

ispbrain/bin/cake migrations seed --seed AccountsSeed 
ispbrain/bin/cake migrations seed --seed ZoneSeed 
ispbrain/bin/cake migrations seed --seed PaymentMethodsSeed

#inserto data inicial para demo 

ispbrain/bin/cake migrations seed --seed MainDemoDataSeed
ispbrain/bin/cake migrations seed --seed TemplateDemoDataSeed --connection template-a

